<?php
namespace frontend\components;

use Yii;
use yii\base\Object;
use yii\helpers\Html;
use yii\web\Cookie;

class Affiliate extends Object
{
    const PIXEL_POSTBACK = 'CB';
    const PIXEL_ADDILIATE = 'am';

    const UPDATE_FOR_LOGGED = false;

    /**
     * @return array
     */
    public static function getAffiliateUrls()
    {
        return [
            self::PIXEL_POSTBACK => 'http://track.clickbooth.com/l/con?',
            self::PIXEL_ADDILIATE => 'http://www.addiliate.com/report.html?',
        ];
    }

    /**
     * Affiliate constructor.
     */
    public function __construct()
    {
        parent::__construct();

        $this->checkAffiliate();
    }

    /**
     * return null
     */
    public function checkAffiliate()
    {
        $get = Yii::$app->request->get();
        $cookies = Yii::$app->response->cookies;

        if(!empty($get['affid'])) {
            // ?affid=CB&subid={subid1}&clickid={clickid}
            // ?affid=CB&subid=123&clickid=456

            $affiliate_id = $get['affid'];
            $sub_id = !empty($get['subid']) ? $get['subid'] : null;

            $affiliate = [
                'affid' => $affiliate_id,
                'subid' => $sub_id,
                'clickid' => !empty($get['clickid']) ? $get['clickid'] : null,
            ];

            $cookies->add(new Cookie([
                'name' => 'affiliate',
                'value' => $affiliate,
            ]));

            if (self::UPDATE_FOR_LOGGED && !Yii::$app->user->isGuest) {
                Yii::$app->user->identity->updateUserAffiliate($affiliate_id, $sub_id, true);
            }
        }
    }

    /**
     * @return null|string
     */
    public function checkPayment()
    {
        $get = Yii::$app->request->get();

        if(empty($get['paypal']) || $get['paypal'] != 'success') {
            return null;
        }

        $affiliate = $this->getAffiliate();

        if (!$affiliate || empty($affiliate['affiliate_url'])) {
            return null;
        }

        $affiliate_id = $affiliate['affiliate_id'];
        $click_id = $affiliate['click_id'];
        $sub_id = $affiliate['sub_id'];
        $affiliate_url = $affiliate['affiliate_url'];

        switch($affiliate_id) {
            case self::PIXEL_POSTBACK:
                $affiliate_url .= http_build_query([
                    'oid' => 32025,
                    'clickid' => $sub_id,
                    'cbtid' => 111,
                ]);
                break;
            case self::PIXEL_ADDILIATE:
                $affiliate_url .= http_build_query([
                    'clickid' => $sub_id,
                ]);
                break;
        }

        if (self::UPDATE_FOR_LOGGED && !Yii::$app->user->isGuest) {
            Yii::$app->user->identity->updateUserAffiliate($affiliate_id, $sub_id, true);
        }

        return Html::img($affiliate_url, ['width' => 1, 'height' => 1]);
    }

    /**
     * @return array|null
     */
    public function getAffiliate()
    {
        $cookies = Yii::$app->request->cookies;

        if(empty($cookies->get('affiliate'))) {
            return null;
        }

        $affiliate = $cookies->get('affiliate')->value;

        if(!empty($affiliate['affid'])) {

            return [
                'affiliate_id' => $affiliate['affid'],
                'click_id' => !empty($affiliate['clickid']) ? $affiliate['clickid'] : null,
                'sub_id' => !empty($affiliate['subid']) ? $affiliate['subid'] : null,
                'affiliate_url' => !empty(self::getAffiliateUrls()[$affiliate['affid']]) ? self::getAffiliateUrls()[$affiliate['affid']] : null,
            ];
        }

        return null;
    }
}