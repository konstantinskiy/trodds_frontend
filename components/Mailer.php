<?php

namespace frontend\components;

use Yii;
use yii\base\Object;

class Mailer extends Object
{
    private $adminEmail = 'admin@trodds.com';
    private $adminName = 'Admin';

    const SUBJECT_SIGNUP = 0;
    const SUBJECT_PASSWORD_RESET = 1;

    /**
     * @return array
     */
    private static function getSubjects()
    {
        return [
            self::SUBJECT_SIGNUP => 'Signup confirmation',
            self::SUBJECT_PASSWORD_RESET => 'Password reset',
        ];
    }

    /**
     * Mailer constructor.
     * @param null $config
     */
    public function __construct($config = null)
    {
        $adminEmail = Yii::$app->params['supportEmail'];
        $adminName = Yii::$app->name . ' Robot';

        if (!empty($adminEmail)) {
            $this->adminEmail = $adminEmail;
        }
        if (!empty($adminName)) {
            $this->adminName = $adminName;
        }

        parent::__construct($config);
    }

    /**
     * @param $user
     * @return bool
     */
    public function sendSignupEmail($user)
    {
        return Yii::$app->mailer->compose(['html' => 'signup-html', 'text' => 'signup-text'], ['user' => $user])
            ->setTo($user->email)
            ->setFrom([$this->adminEmail => $this->adminName])
            ->setSubject($this->getSubject(self::SUBJECT_SIGNUP))
            ->send();
    }

    /**
     * @param $user
     * @return bool
     */
    public function sendPasswordEmail($user)
    {
        return Yii::$app->mailer->compose(['html' => 'passwordResetToken-html', 'text' => 'passwordResetToken-text'], ['user' => $user])
            ->setFrom([$this->adminEmail => $this->adminName])
            ->setTo($user->email)
            //->setTo([$user->email => $user->first_name . ' ' . $user->last_name])
            ->setSubject(self::getSubject(self::SUBJECT_PASSWORD_RESET))
            ->send();
    }

    /**
     * @param $subjectCode
     * @return string
     */
    private function getSubject($subjectCode)
    {
        return self::getSubjects()[$subjectCode] . ' for ' . Yii::$app->name;
    }
}