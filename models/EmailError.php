<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace frontend\models;

use Yii;
use yii\base\Model;
use common\models\User;
/**
 * Description of EmailError
 *
 * @author doctus
 */
class EmailError extends Model{
    //put your code here
    public $email;
    public $userid;
    
    public function rules()
    {
        return [
            ['email', 'email'],
            ['userid', 'required'],
        ];
    }
    
    public function changeEmail() {
        
        $password = Yii::$app->security->generateRandomString(8);
        
        $user = User::find()->where(['id' => $this->userid])->one();
        
        //remove from mailchimp
//        try {
//            Yii::$app->mailchimp->post('/lists/ecb38f79c1/members', [
//                'status' => 'cleaned',
//                'email_address' => $this->email,
//            ]);
//
//        } catch(\Exception  $e) {
//            // print_r($e);
//            // die();
//        }
        
        
        
        $user->setPassword($password);
        $user->confirm_code = User::generateConfirmCode();
        
        try {
            Yii::$app->mailchimp->delete('/lists/ecb38f79c1/members/'.md5(strtolower($user->email)));
            $result = Yii::$app->mailchimp->post('/lists/ecb38f79c1/members', [
                'status' => 'subscribed',
                'email_address' => $this->email,
                'merge_fields' => [
                    'FNAME' => $user->first_name,
                    'PASS' => $password,
                    'CONF_CODE' => $user->confirm_code,
                ]
            ]);
            $user->mailchimp_id = $result->id;
        } catch(\Exception  $e) {
            // print_r($e);
            // die();
        }
        
        $user->email = $this->email;
        $user->save();
        return  $user ? $user : null;
    }
    
}
