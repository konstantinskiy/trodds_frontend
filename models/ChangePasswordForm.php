<?php 
namespace frontend\models;

use Yii;
use yii\base\Model;
use common\models\User;

class ChangePasswordForm extends Model {
    public $oldpass;
    public $newpass;
    public $repeatnewpass;
    
    public function rules(){
        return [
            [['oldpass','newpass','repeatnewpass'], 'required'],
            [['oldpass'], 'findPasswords'],
            [['repeatnewpass'], 'compare', 'compareAttribute' => 'newpass'],
        ];
    }
    
    public function findPasswords($attribute, $params){
        $user = Yii::$app->user->identity;

        // $oldPassword = Yii::$app->security->generatePasswordHash($this->oldpass);

        // // print_r(Yii::$app->user->identity->password_hash);
        // print_r($oldPassword);

        if(!$user->validatePassword($this->oldpass))
            $this->addError($attribute, 'Old password is incorrect');
    }
    
    public function attributeLabels(){
        return [
            'oldpass'=>'Old Password',
            'newpass'=>'New Password',
            'repeatnewpass'=>'Repeat New Password',
        ];
    }
}