<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace frontend\models;

use Yii;
use yii\base\Model;
use common\models\User;
/**
 * Description of Confirm
 *
 * @author doctus
 */
class Confirm extends Model{
    //put your code here
    public $code;
    
    public function rules()
    {
        return [
            ['code', 'required'],
        ];
    }
 
    public function confirm($code = null) {
        if($code != null){
            $this->code = $code;
        }
        if($user = User::confirm($this->code)){
            return Yii::$app->getUser()->login($user);
        }
        return false;
    }
}
