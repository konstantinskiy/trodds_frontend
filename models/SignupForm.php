<?php
namespace frontend\models;

use Yii;
use yii\base\Model;
use yii\base\Security;
use common\models\User;
use GeoIp2\Database\Reader;
use frontend\components\Mailer;

/**
 * Signup form
 */
class SignupForm extends Model
{
    public $first_name;
    public $email;
    public $password;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['first_name', 'trim'],
            ['first_name', 'required'],
            // ['first_name', 'unique', 'targetClass' => '\common\models\User', 'message' => 'This username has already been taken.'],
            ['first_name', 'string', 'min' => 2, 'max' => 20],

            ['email', 'trim'],
            ['email', 'required'],
            ['email', 'email'],
            ['email', 'string', 'max' => 255],
            ['email', 'unique', 'targetClass' => '\common\models\User', 'message' => 'This email address has already been taken.'],

            ['password', 'required'],
            ['password', 'string', 'min' => 6],
        ];
    }

    /**
     * Signs user up.
     *
     * @return User|null the saved model or null if saving fails
     */
    public function signup()
    {
        if (!$this->validate()) {
            return null;
        }
        
        $user = new User();
        $user->first_name = $this->first_name;
        $user->email = $this->email;
        $user->status = User::STATUS_ACTIVE;
        // Generating random password
        //$this->password = Yii::$app->security->generateRandomString(8);
        $user->setPassword($this->password);
        $user->generateAuthKey();
        $user->generateAccessToken();
        // Generate random confirmation code
        $user->confirm_code = User::generateConfirmCode();
        // set confirmation status
        $user->role = User::ROLE_UNCOFIRMED;
        // Getting IP
        $user->ip_address = isset($_SERVER['HTTP_X_FORWARDED_FOR']) ? $_SERVER['HTTP_X_FORWARDED_FOR'] : Yii::$app->request->getUserIP();

        // Getting country
        try {
            $reader = new Reader(dirname(__FILE__).'/../../common/GeoLite2-Country.mmdb');
            $record = $reader->country($user->ip_address);
            $user->country_code = $record->country->isoCode;
        } catch(\Exception  $e) {

        }

        $user->save();

        // Subscribe to mailchimp
        try {
            $result = Yii::$app->mailchimp->post('/lists/ecb38f79c1/members', [
                'status' => 'subscribed',
                'email_address' => $this->email,
                'merge_fields' => [
                    'FNAME' => $this->first_name,
                    'PASS' => $this->password,
                    'CONF_CODE' => $user->confirm_code,
                    'USER_ID' => $user->id,
                ]
            ]);

            $user->mailchimp_id = $result->id;
            //$mailer = new Mailer();
            //$mailer->sendSignupEmail($user);
        } catch(\Exception  $e) {
//             print_r($e);
            // die();
        }
        
        
        return  $user ? $user : null;
    }
}
