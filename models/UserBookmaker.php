<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace frontend\models;

use Yii;
use yii\base\Model;
use common\models\UserBookmakers;
use common\models\Bookmaker;
use yii\helpers\ArrayHelper;

/**
 * Description of UserBookmaker
 *
 * @author doctus
 */
class UserBookmaker extends Model{

    public $list = [];
    
    public function updateList($userId)
    {
        $savedList = UserBookmakers::find()->where(['user_id' => $userId])->all();

        foreach($savedList as $item){
            if(empty($this->list[$item->bookmakers_id])){
                UserBookmakers::deleteAll(['user_id' => $userId, 'bookmakers_id' => $item->bookmakers_id]);
            }
        }

        $savedList = ArrayHelper::index($savedList, 'bookmakers_id');

        foreach ($this->list as $key => $item) {
            $key = (int) $key;
            if (!empty($key) && empty($savedList[$key])) {
                UserBookmakers::addBookmakersToUser($userId, $key);
            }
        }
    }
}
