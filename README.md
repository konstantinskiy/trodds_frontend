# TRODDS.COM

* **www_root:** frontend/web

* **Database settings:**

    models/UpdateTrial.php

    models/UpdateUsers.php

* **cookieValidationKey:**

    config/main-local.php

* **Should be writable by php/web server**

    frontend/runtime

    frontend/web/assets

#### Required versions:

* **PHP:** 7.1

#### Directory structure for reference:
```
dev.trodds.local
├── backend  https://bitbucket.org/sbodds7team/backend.git
├── common   https://bitbucket.org/sbodds7team/common.git
├── engine   https://bitbucket.org/sbodds7team/engine.git
└── frontend https://bitbucket.org/sbodds7team/frontend.git
```
#### main-local.php for reference:

```php
<?php

$config = [
    'components' => [
        'request' => [
            // !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
            'cookieValidationKey' => '',
        ],
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            'viewPath' => '@common/mail',
            // send all mails to a file by default. You have to set
            // 'useFileTransport' to false and configure a transport
            // for the mailer to send real emails.
            'useFileTransport' => true,
        ],
        'authClientCollection' => [
            'clients' => [
                'google' => [
                    'clientId' => '...',
                    'clientSecret' => '...',
                ],
                'facebook' => [
                    'clientId' => '...',
                    'clientSecret' => '...',
                ],
                'twitter' => [
                    'consumerKey' => '...',
                    'consumerSecret' => '...',
                ],
            ],
        ],
    ],
];

if (!YII_ENV_TEST && YII_DEBUG) {
    // configuration adjustments for 'dev' environment
    $config['bootstrap'][] = 'debug';
    $config['modules']['debug'] = [
        'class' => 'yii\debug\Module',
    ];

    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = [
        'class' => 'yii\gii\Module',
    ];
}

return $config;
```

#### params-local.php for reference:

```php
<?php
return [
    'adminEmail' => 'support@trodds.com',
    'engineUrl' => 'https://www.trodds.com/engine/parser/loadFrontend.php?id=',
    'paypal' => ['sandbox' => false, 'hosted_button_id' => 'code'],
];
```

#### index.php for reference:

```php
// for development environment
defined('YII_DEBUG') or define('YII_DEBUG', true);
defined('YII_ENV') or define('YII_ENV', 'dev');

// for production environment
defined('YII_DEBUG') or define('YII_DEBUG', false);
defined('YII_ENV') or define('YII_ENV', 'prod');

// ...
```

#### UpdateTrial.php for reference:

```php
<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

//set count days of trial period
$day_count = 2;

if($link = mysqli_connect('dbhost', '', '', 'trodds_engine')){
    $time_over = time() - $day_count * 24 * 3600;
    mysqli_query($link, "UPDATE user SET role=10 WHERE confirm_date<".$time_over." AND role=40");
    mysqli_close($link);
}
```

#### UpdateUsers.php for reference:

```php
<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
if($link = mysqli_connect('dbhost', '', '', 'trodds_engine')){
    $today = time();
    $days = intval(date('t')) * 24 * 3600;
    mysqli_query($link, "UPDATE user SET role=10, payed_day=0 WHERE role=20 AND subscribed=0 AND payed_day<=".$today);
    mysqli_query($link, "UPDATE user SET payed_day=payed_day+".$days." WHERE role=20 AND subscribed=1 AND payed_day<=".$today);
    mysqli_close($link);
}
```

#### Nginx configuration for reference:

```text
server {

  listen 80;

  server_name www.dev.trodds.local;

  root /var/www/dev.trodds.local/frontend/web;

  location / {
    try_files $uri /index.php =404;
    fastcgi_pass php-sock; # PHP-FPM socket or IP:PORT
    fastcgi_param SCRIPT_FILENAME $document_root/index.php;
    include fastcgi_params;
    limit_except GET POST {deny all;}
  }

  location ~ \.php$ {
    try_files $fastcgi_script_name =404;
    fastcgi_split_path_info ^((?U).+\.php)(/?.+)$;
    fastcgi_pass php-sock; # PHP-FPM socket or IP:PORT
    fastcgi_param SCRIPT_FILENAME $document_root$fastcgi_script_name;
    include fastcgi_params;
    limit_except GET POST {deny all;}
  }

}
```