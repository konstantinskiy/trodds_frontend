<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use yii\helpers\Url;
use common\models\User;

/* @var $this yii\web\View */

$countries_list = [
    'AF' => 'Afghanistan',
    'AX' => 'Aland Islands',
    'AL' => 'Albania',
    'DZ' => 'Algeria',
    'AS' => 'American Samoa',
    'AD' => 'Andorra',
    'AO' => 'Angola',
    'AI' => 'Anguilla',
    'AQ' => 'Antarctica',
    'AG' => 'Antigua and Barbuda',
    'AR' => 'Argentina',
    'AM' => 'Armenia',
    'AW' => 'Aruba',
    'AU' => 'Australia',
    'AT' => 'Austria',
    'AZ' => 'Azerbaijan',
    'BS' => 'Bahamas',
    'BH' => 'Bahrain',
    'BD' => 'Bangladesh',
    'BB' => 'Barbados',
    'BY' => 'Belarus',
    'BE' => 'Belgium',
    'BZ' => 'Belize',
    'BJ' => 'Benin',
    'BM' => 'Bermuda',
    'BT' => 'Bhutan',
    'BO' => 'Bolivia',
    'BQ' => 'Bonaire, Saint Eustatius and Saba',
    'BA' => 'Bosnia and Herzegovina',
    'BW' => 'Botswana',
    'BV' => 'Bouvet Island',
    'BR' => 'Brazil',
    'IO' => 'British Indian Ocean Territory',
    'VG' => 'British Virgin Islands',
    'BN' => 'Brunei',
    'BG' => 'Bulgaria',
    'BF' => 'Burkina Faso',
    'BI' => 'Burundi',
    'KH' => 'Cambodia',
    'CM' => 'Cameroon',
    'CA' => 'Canada',
    'CV' => 'Cape Verde',
    'KY' => 'Cayman Islands',
    'CF' => 'Central African Republic',
    'TD' => 'Chad',
    'CL' => 'Chile',
    'CN' => 'China',
    'CX' => 'Christmas Island',
    'CC' => 'Cocos Islands',
    'CO' => 'Colombia',
    'KM' => 'Comoros',
    'CK' => 'Cook Islands',
    'CR' => 'Costa Rica',
    'HR' => 'Croatia',
    'CU' => 'Cuba',
    'CW' => 'Curacao',
    'CY' => 'Cyprus',
    'CZ' => 'Czech Republic',
    'CD' => 'Democratic Republic of the Congo',
    'DK' => 'Denmark',
    'DJ' => 'Djibouti',
    'DM' => 'Dominica',
    'DO' => 'Dominican Republic',
    'TL' => 'East Timor',
    'EC' => 'Ecuador',
    'EG' => 'Egypt',
    'SV' => 'El Salvador',
    'GQ' => 'Equatorial Guinea',
    'ER' => 'Eritrea',
    'EE' => 'Estonia',
    'ET' => 'Ethiopia',
    'FK' => 'Falkland Islands',
    'FO' => 'Faroe Islands',
    'FJ' => 'Fiji',
    'FI' => 'Finland',
    'FR' => 'France',
    'GF' => 'French Guiana',
    'PF' => 'French Polynesia',
    'TF' => 'French Southern Territories',
    'GA' => 'Gabon',
    'GM' => 'Gambia',
    'GE' => 'Georgia',
    'DE' => 'Germany',
    'GH' => 'Ghana',
    'GI' => 'Gibraltar',
    'GR' => 'Greece',
    'GL' => 'Greenland',
    'GD' => 'Grenada',
    'GP' => 'Guadeloupe',
    'GU' => 'Guam',
    'GT' => 'Guatemala',
    'GG' => 'Guernsey',
    'GN' => 'Guinea',
    'GW' => 'Guinea-Bissau',
    'GY' => 'Guyana',
    'HT' => 'Haiti',
    'HM' => 'Heard Island and McDonald Islands',
    'HN' => 'Honduras',
    'HK' => 'Hong Kong',
    'HU' => 'Hungary',
    'IS' => 'Iceland',
    'IN' => 'India',
    'ID' => 'Indonesia',
    'IR' => 'Iran',
    'IQ' => 'Iraq',
    'IE' => 'Ireland',
    'IM' => 'Isle of Man',
    'IL' => 'Israel',
    'IT' => 'Italy',
    'CI' => 'Ivory Coast',
    'JM' => 'Jamaica',
    'JP' => 'Japan',
    'JE' => 'Jersey',
    'JO' => 'Jordan',
    'KZ' => 'Kazakhstan',
    'KE' => 'Kenya',
    'KI' => 'Kiribati',
    'XK' => 'Kosovo',
    'KW' => 'Kuwait',
    'KG' => 'Kyrgyzstan',
    'LA' => 'Laos',
    'LV' => 'Latvia',
    'LB' => 'Lebanon',
    'LS' => 'Lesotho',
    'LR' => 'Liberia',
    'LY' => 'Libya',
    'LI' => 'Liechtenstein',
    'LT' => 'Lithuania',
    'LU' => 'Luxembourg',
    'MO' => 'Macao',
    'MK' => 'Macedonia',
    'MG' => 'Madagascar',
    'MW' => 'Malawi',
    'MY' => 'Malaysia',
    'MV' => 'Maldives',
    'ML' => 'Mali',
    'MT' => 'Malta',
    'MH' => 'Marshall Islands',
    'MQ' => 'Martinique',
    'MR' => 'Mauritania',
    'MU' => 'Mauritius',
    'YT' => 'Mayotte',
    'MX' => 'Mexico',
    'FM' => 'Micronesia',
    'MD' => 'Moldova',
    'MC' => 'Monaco',
    'MN' => 'Mongolia',
    'ME' => 'Montenegro',
    'MS' => 'Montserrat',
    'MA' => 'Morocco',
    'MZ' => 'Mozambique',
    'MM' => 'Myanmar',
    'NA' => 'Namibia',
    'NR' => 'Nauru',
    'NP' => 'Nepal',
    'NL' => 'Netherlands',
    'NC' => 'New Caledonia',
    'NZ' => 'New Zealand',
    'NI' => 'Nicaragua',
    'NE' => 'Niger',
    'NG' => 'Nigeria',
    'NU' => 'Niue',
    'NF' => 'Norfolk Island',
    'KP' => 'North Korea',
    'MP' => 'Northern Mariana Islands',
    'NO' => 'Norway',
    'OM' => 'Oman',
    'PK' => 'Pakistan',
    'PW' => 'Palau',
    'PS' => 'Palestinian Territory',
    'PA' => 'Panama',
    'PG' => 'Papua New Guinea',
    'PY' => 'Paraguay',
    'PE' => 'Peru',
    'PH' => 'Philippines',
    'PN' => 'Pitcairn',
    'PL' => 'Poland',
    'PT' => 'Portugal',
    'PR' => 'Puerto Rico',
    'QA' => 'Qatar',
    'CG' => 'Republic of the Congo',
    'RE' => 'Reunion',
    'RO' => 'Romania',
    'RU' => 'Russia',
    'RW' => 'Rwanda',
    'BL' => 'Saint Barthelemy',
    'SH' => 'Saint Helena',
    'KN' => 'Saint Kitts and Nevis',
    'LC' => 'Saint Lucia',
    'MF' => 'Saint Martin',
    'PM' => 'Saint Pierre and Miquelon',
    'VC' => 'Saint Vincent and the Grenadines',
    'WS' => 'Samoa',
    'SM' => 'San Marino',
    'ST' => 'Sao Tome and Principe',
    'SA' => 'Saudi Arabia',
    'SN' => 'Senegal',
    'RS' => 'Serbia',
    'SC' => 'Seychelles',
    'SL' => 'Sierra Leone',
    'SG' => 'Singapore',
    'SX' => 'Sint Maarten',
    'SK' => 'Slovakia',
    'SI' => 'Slovenia',
    'SB' => 'Solomon Islands',
    'SO' => 'Somalia',
    'ZA' => 'South Africa',
    'GS' => 'South Georgia and the South Sandwich Islands',
    'KR' => 'South Korea',
    'SS' => 'South Sudan',
    'ES' => 'Spain',
    'LK' => 'Sri Lanka',
    'SD' => 'Sudan',
    'SR' => 'Suriname',
    'SJ' => 'Svalbard and Jan Mayen',
    'SZ' => 'Swaziland',
    'SE' => 'Sweden',
    'CH' => 'Switzerland',
    'SY' => 'Syria',
    'TW' => 'Taiwan',
    'TJ' => 'Tajikistan',
    'TZ' => 'Tanzania',
    'TH' => 'Thailand',
    'TG' => 'Togo',
    'TK' => 'Tokelau',
    'TO' => 'Tonga',
    'TT' => 'Trinidad and Tobago',
    'TN' => 'Tunisia',
    'TR' => 'Turkey',
    'TM' => 'Turkmenistan',
    'TC' => 'Turks and Caicos Islands',
    'TV' => 'Tuvalu',
    'VI' => 'U.S. Virgin Islands',
    'UG' => 'Uganda',
    'UA' => 'Ukraine',
    'AE' => 'United Arab Emirates',
    'GB' => 'United Kingdom',
    'US' => 'United States',
    'UM' => 'United States Minor Outlying Islands',
    'UY' => 'Uruguay',
    'UZ' => 'Uzbekistan',
    'VU' => 'Vanuatu',
    'VA' => 'Vatican',
    'VE' => 'Venezuela',
    'VN' => 'Vietnam',
    'WF' => 'Wallis and Futuna',
    'EH' => 'Western Sahara',
    'YE' => 'Yemen',
    'ZM' => 'Zambia',
    'ZW' => 'Zimbabwe',
];

$this->title = 'Settings';
$this->params['breadcrumbs'][] = $this->title;

$this->params['page_class'] = '_settings_bookies';
?>
<main>
    <div class="main_menu clearfix width_container">
      <div class="container_list">
        <?= $this->render('_menu') ?>
      </div>
    </div>
    <?php $form = ActiveForm::begin([
          'options' => ['class' => 'form_contact_settings']
          //'errorCssClass' => 'has-danger',
          /*'fieldConfig' => [
              'hintOptions' => [
                  'class' => 'form-control-feedback'
              ],
              'errorOptions' => [
                  'class' => 'form-control-feedback'
              ]
          ]*/
          
      ]); ?>
      <p class="form_contact_name">Name</p>
      <?= $form->field($model, 'first_name', ['template' => '{input}{error}'])->textInput(['placeholder' => 'Tester']) ?>
      <p class="form_contact_email">Email<span id="true"></span><span id="false"></span></p>
      <?= $form->field($model, 'email', ['template' => '{input}{error}'])->textInput(['placeholder' => 'email@user.com']) ?>
      <div class="container_coutry">
        <p class="form_contact_account">Country
          <span class="select_arrow"></span>
        </p>
        <?= $form->field($model, 'country_code', ['template' => '{input}{error}'])->widget(Select2::classname(), [
            'data' => $countries_list,
            'language' => 'en',
            'options' => ['placeholder' => 'Select a country ...', 'class' => 'select_country'],
            'pluginOptions' => [
                'allowClear' => false
            ],
        ])->label('Country') ?>
        
      </div>
      <i>
        Notice: Not all bookmakers accept bettors from your country. Please select the country
        of your residence and we will provide you the arbs only from the relevant bookies.
      </i>
      <input type="submit" name="" value="Save">
      <!-- <i>
        You receive full information on the current SureBets since you are subscribed to TRodds Premium Services. In order to cancel your subscription and stop renewal charges, please click the button below.
      </i>
      <input type="submit" name="" value="Stop"> -->
    <?php ActiveForm::end(); ?>
    <?php $prefix = !empty(Yii::$app->params['paypal']) && !empty(Yii::$app->params['paypal']['sandbox']) ? 'sandbox.' : ''; ?>
    <?php $hosted_button_id = !empty(Yii::$app->params['paypal']) && !empty(Yii::$app->params['paypal']['hosted_button_id']) ? Yii::$app->params['paypal']['hosted_button_id'] : ''; ?>
    <?php if(!Yii::$app->user->identity->isSubscriber): ?>
        <div class="home_limited">
          <h2>Limited account warning!</h2>
          <p>
            In order to enjoy from the SureBet Calculator, to see the full details about arbitrage situations and to take the advantage of our full featured
            <span>Premium Package</span> please upgrade your account just for <span>€9.99/month.</span>
          </p>
          <a href="/site/deposit">Upgrade</a>
        </div>
    <?php elseif(Yii::$app->user->identity->isSubscriber): ?>
        <?php if(Yii::$app->user->identity->isTrial): ?>
            <?php
            if(time() - Yii::$app->user->identity->confirm_date >= 2 * 3600 * 24){
                User::clearPayedPeriod();
                $this->context->refresh();
            }
            ?>
                <p class="card-text">To continue enjoying your Premium account privileges, SureBet calculator, as well as see full details about SureBet situations, take advantage of our full featured Premium Package by upgrading your account for just <b>€9.99/month</b></p>
                <form action="https://www.<?= $prefix ?>paypal.com/cgi-bin/webscr" method="post" target="_top">
                    <input type="hidden" name="cmd" value="_s-xclick">
                    <input type="hidden" name="hosted_button_id" value="<?= $hosted_button_id ?>">
                    <input type="hidden" name="custom" value="<?= Yii::$app->user->id ?>">
                    <input type="submit" class="btn btn-primary" name="submit" value="Upgrade">
                    <?= Html::a('Start tutotrial', yii\helpers\Url::to(['settings/account', 'tutotrial' => true]), ['class' => 'btn btn-primary']) ?>
                </form>
        <?php elseif(Yii::$app->user->identity->role == 20 && Yii::$app->user->identity->subscribed == 0): ?>

            <?php
                $today = time();
                if($today >= Yii::$app->user->identity->payed_day){
                    User::clearPayedPeriod();
                    $this->context->refresh();
                }else{
                    $remained = Yii::$app->user->identity->payed_day - $today;
                    $sec_remained = $remained % 60;
                    $min_remained = (($remained - $sec_remained) / 60) % 60;
                    $hours = (($remained - $sec_remained - 60 * $min_remained) / 3600) % 24;
                    $days = (($remained - $sec_remained - 60 * $min_remained - 3600 * $hours) / (3600 * 24));
                }
                $left_str = 'Left ';
                if($days > 1){
                    $left_str .= $days.' Days ';
                }elseif ($days == 1) {
                    $left_str .= $days.' Day ';
                }
                $left_str .= $hours.':';
                $left_str .= ($min_remained < 10 && $min_remained > 0)?'0':'';
                $left_str .= $min_remained.'.';
            ?>

            <?php $prefix = !empty(Yii::$app->params['paypal']) && !empty(Yii::$app->params['paypal']['sandbox']) ? 'sandbox.' : ''; ?>
            <?php $hosted_button_id = !empty(Yii::$app->params['paypal']) && !empty(Yii::$app->params['paypal']['hosted_button_id']) ? Yii::$app->params['paypal']['hosted_button_id'] : ''; ?>
            <form action="https://www.<?= $prefix ?>paypal.com/cgi-bin/webscr" method="post" target="_top">
                <input type="hidden" name="cmd" value="_s-xclick">
                <input type="hidden" name="hosted_button_id" value="<?= $hosted_button_id ?>">
                <input type="hidden" name="custom" value="<?= Yii::$app->user->id ?>">
                 <i class="alarm_button">
                  <span>Unsubscribed Account! </br> <?=$left_str?>.</span> In order to continue you Premium Package subscription and to enjoy from the SureBet Calculator and the full details about arbitrage situations please upgrade your account just
                  <span>for €9.99/month.</span>
                  <a href="/site/deposit" class="upgrade-acc-btn">Upgrade</a>
                </i>
            </form>
        <?php else: ?>
            <div class="form_contact_settings">
                <i>
                    You receive full information on the current SureBets since you are subscribed to TRodds Premium Services. In order to cancel your subscription and stop renewal charges, please click the button below.
                </i>
                <a class="upgrade-acc-btn" href="https://www.<?= $prefix ?>paypal.com/cgi-bin/webscr?cmd=_subscr-find&alias=NGEK9WD6H59U4&custom=<?= Yii::$app->user->id ?>">Stop</a>
            </div>
        <?php endif; ?>
    <?php endif; ?>
  </main>