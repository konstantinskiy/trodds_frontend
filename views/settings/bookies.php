<?php

/* @var $this yii\web\View */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\helpers\Url;
use common\models\Bookmaker;
use common\models\UserBookmakers;

$this->title = 'Change Password';
$this->params['breadcrumbs'][] = $this->title;
$this->params['page_class'] = '_settings_bookies';
?>
  <main>
    <div class="main_menu clearfix width_container">
      <div class="container_list">
        <?= $this->render('_menu') ?>
      </div>
    </div>
    <div class="settings_checked">
        <?php
            $bookmakers = Bookmaker::find()->all();
            $form = ActiveForm::begin();
        ?>
            <div class="settings_checkbox row">
                <?foreach($bookmakers as $bookmaker):?>
                    <p class="col-md-3 col-sm-6 col-xs-12">
                      <input id="bookmaker_id_<?=$bookmaker->id?>" name="<?=$bookmaker->id?>" type="checkbox" placeholder="<?=$bookmaker->name?>" <?if(UserBookmakers::isConnect(Yii::$app->user->identity->id, $bookmaker->id)):?> checked <?endif;?>>
                      <label for="bookmaker_id_<?=$bookmaker->id?>"><?=$bookmaker->name?></label>
                    </p>
                <?endforeach;?>
            </div>

            <div class="bookies_checkbox_submit row">
                <div class="col-xs-12">
                    <?php echo Html::submitButton('Save changes', ['class' => 'bookies_checkbox_submit_btn', 'name' => 'login-button']); ?>
                </div>
            </div>
            
        <?php ActiveForm::end(); ?>
    </div>
  </main>