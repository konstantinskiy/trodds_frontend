<?php
use yii\widgets\Menu;
use yii\helpers\Url;

$menuItems = [];


$menuItems[] = ['label' => 'My account', 'url' => ['settings/account']] ;
$menuItems[] = ['label' => 'Change password', 'url' => ['settings/changepassword']];
$menuItems[] = ['label' => 'My bookies', 'url' => ['settings/bookies']];
// $menuItems[] = ['label' => 'Getting started', 'url' => ['site/start']];
// $menuItems[] = ['label' => 'Contact us', 'url' => ['site/contact']];

?>

<?php echo Menu::widget([
        'options' => [
            'class' => 'main_menu_list'
        ],
        'itemOptions' => [
            'class' => 'main_menu_item'
        ],
        'linkTemplate' => '<a class="main_menu_link" href="{url}">{label}</a>',
        'items' => $menuItems,
    ]);
?>