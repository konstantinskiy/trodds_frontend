<?php

/* @var $this yii\web\View */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\helpers\Url;

$this->title = 'Change Password';
$this->params['breadcrumbs'][] = $this->title;
$this->params['page_class'] = '_settings_bookies';
?>
  <main>
    <div class="main_menu clearfix width_container">
      <div class="container_list">
        <?= $this->render('_menu') ?>
      </div>
    </div>
    <div class="main_sing_up">
      <?php $form = ActiveForm::begin([
            'id'=>'changepassword-form',
            'options' => [
                'class' => 'form_changes_pass'
            ],
            'errorCssClass' => 'has-danger',
            /*'fieldConfig' => [
                'hintOptions' => [
                    'class' => 'form-control-feedback'
                ],
                'errorOptions' => [
                    'class' => 'form-control-feedback'
                ]
            ]*/
        ]); ?>
        <label class="form_contact_changes_password" for="username">Old password</label>
        <?= $form->field($model,'oldpass',['template' => '{input}<i>{error}</i>'])->passwordInput(['placeholder' => 'Old Password'])->label(false) ?>
        
        
        <label class="form_contact_changes_password" for="username">New password</label>
        <?= $form->field($model,'newpass',['template' => '{input}<i>{error}</i>'])->passwordInput(['placeholder' => 'New Password'])->label(false) ?>
        
    
        <label class="form_contact_changes_password" for="username">Repeat new password</label>
        <?= $form->field($model,'repeatnewpass', ['template' => '{input}<i>{error}</i>'])->passwordInput(['placeholder' => 'Repeat New Password'])->label(false) ?>
        <input type="submit" name="" value="Change">
        
      <?php ActiveForm::end(); ?>
    </div>
  </main>