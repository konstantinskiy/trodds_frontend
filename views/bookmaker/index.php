<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Bookies';
$this->params['breadcrumbs'][] = $this->title;
$this->params['page_class'] = '_bookies';
$this->params['head_title'] = '_bookies';
?>
<main>

<?$bookmakers_array = array_chunk($dataProvider->models, 2);?>
    <?foreach($bookmakers_array as $chunk):?>
      <?$bookmakers_chunk = array_chunk($chunk, 2);?>
      <div class="section_bookies">   
        <?php $counter = 1; ?> 
        <?foreach($chunk as $model):?>
          <div class="bookies_<?php if($counter % 2 == 0):?>winner<?else:?>pinnacle<?endif?>">
            <div class="bookies_img">
              <a href="<?= Url::to(['site/redirectbookmaker']) ?>?bookmaker_id=<?= $model->id ?>" target="_blank">
              <?= Html::img('data:image/png;base64,' . base64_encode($model->banner_image)) ?>
              </a>
            </div>
            <div class="description_bookies">
              <a href="<?= Url::to(['site/redirectbookmaker']) ?>?bookmaker_id=<?= $model->id ?>" target="_blank">Open Account</a>
              <?= $model->body ?>
            </div>
          </div>
          <?php $counter++; ?>  
        <?endforeach;?>  
      </div>    
    
    <?endforeach;?>
</main>