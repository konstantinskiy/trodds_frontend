<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use yii\widgets\Menu;
use frontend\assets\AppAsset;
use common\widgets\Alert;
use frontend\components\Affiliate;
use yii\helpers\Url;
use frontend\widgets\ConfirmEmailChecker;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>

<?php
$homeUrl = Yii::$app->controller->id == 'clickbank' ? ['clickbank/index'] : Url::home();
?>

<div class="wrapper">
    <nav class="navbar navbar-full navbar-dark">
        <div class="container">
            <?= (!($this->context->action->id == 'thankyou' || $this->context->action->id == 'confirm'))?Html::a('<img src="' . Url::to('@web/img/logo.svg') . '">', $homeUrl, ['class' => 'navbar-brand']):Html::a('<img src="' . Url::to('@web/img/logo.svg') . '">')  ?>
            <div class="clearfix hidden-sm-up"></div>
            <?php
                $menuItems = [];
                if (Yii::$app->user->isGuest) {
                    if(!($this->context->action->id == 'thankyou' || $this->context->action->id == 'confirm')){
                    if (Yii::$app->controller->id == 'clickbank') {
                        $menuItems = [
                            ['label' => 'Getting started', 'url' => ['clickbank/start']],
                            ['label' => 'Bookies', 'url' => ['bookmaker/index']],
                            ['label' => 'Contact us', 'url' => ['clickbank/contact']],
                        ];
                    } else {
                        $menuItems = [
                            ['label' => 'Getting started', 'url' => ['site/start']],
                            ['label' => 'Bookies', 'url' => ['bookmaker/index']],
                            ['label' => 'Contact us', 'url' => ['site/contact']],
                        ];
                    }
                    // $menuItems[] = ['label' => 'Signup', 'url' => ['/site/signup']];
                    if (Yii::$app->controller->id != 'clickbank') {
                        $menuItems[] = ['label' => 'Signup', 'url' => ['/site/signup'], 'template' => '<a class="btn btn-outline-secondary" href="{url}">{label}</a>'];
                        $menuItems[] = ['label' => 'Login', 'url' => ['/site/login'], 'template' => '<a class="btn btn-outline-secondary" href="{url}">{label}</a>'];
                    }
                    }
                } else {
                    $menuItems = [
//                        ['label' => (Yii::$app->user->identity->role == 40)?'Trial mode':''],
                        ['label' => 'Home', 'url' => ['subscription/index']],
                        ['label' => 'Bookies', 'url' => ['bookmaker/index']],
                        [
                            'label' => 'Settings',
                            'url' => ['settings/account'], 
                            'submenuTemplate' => "\n<ul class=\"hidden-xl-down\">\n{items}\n</ul>\n",
                            'items' => [
                                ['label' => 'Account', 'url' => ['settings/account']],
                                ['label' => 'Change password', 'url' => ['settings/changepassword']],
                                ['label' => 'My Bookies', 'url' => ['settings/bookies']],
                            ]],
                        [
                            'label' => 'Help',
                            'url' => ['help/start'],
                            'submenuTemplate' => "\n<ul class=\"hidden-xl-down\">\n{items}\n</ul>\n",
                            'items' => [
                                ['label' => 'Getting started', 'url' => ['help/start']],
                                ['label' => 'About TRodds', 'url' => ['help/about']],
                                ['label' => 'Terms, privacy Disclaimer', 'url' => ['help/terms']],
                                ['label' => 'Contact us', 'url' => ['help/contact']],
                            ],
                        ],
                    ];
                    $menuItems[] = ['label' => 'Logout (' . Yii::$app->user->identity->first_name . ')', 'url' => ['/site/logout']];
                }
            ?>
            <?php echo Menu::widget([
                    'options' => [
                        'class' => 'nav navbar-nav float-sm-right'
                    ],
                    'itemOptions' => [
                        'class' => 'nav-item'
                    ],
                    'activateParents' => true,
                    'linkTemplate' => '<a class="nav-link" href="{url}">{label}</a>',
                    'items' => $menuItems,
                ]);
            ?>
        </div>
    </nav>
    <?= ConfirmEmailChecker::widget() ?>
    <?php //= Alert::widget() ?>
    <?= $content ?>
    <?php if(Yii::$app->controller->id == 'site') { ?>
        <section class="section section-dark text-xs-center">
            <div class="container">
                <h2>Some of the bookmakers that we scan</h2>
                <div class="mt-2 bookmakers-logos">
                    <img style="height: 52px; width: 200px;" class="img-fluid" src="<?= Url::to('@web/img/William_Hill.svg') ?>">
                    <img style="height: 55px; width: 200px;" class="img-fluid" src="<?= Url::to('@web/img/Bet-at-home-Logo.svg') ?>">
                    <img style="height: 52px; width: 200px;" class="img-fluid" src="<?= Url::to('@web/img/banner-pinnacle.svg') ?>">
                    <img style="height: 79px; width: 200px;" class="img-fluid" src="<?= Url::to('@web/img/mybet.svg') ?>">
                    <img style="height: 45px; width: 200px;" class="img-fluid" src="<?= Url::to('@web/img/Ladbrokes.svg') ?>">
                </div>
            </div>
        </section>
    <?php } ?>
</div>

<footer class="footer">
    <div class="container">
        <div class="row">
            <div class="col-lg-5">
                <p class="float-lg-left text-xs-center">All rights reseved to Top Rated Odds &copy; 2007-<?= date('Y') ?></p>
                <div class="clearfix"></div>
            </div>
            <div class="col-lg-2">
                <?php
                $url = Url::base(true);
                $host = str_replace(['http://', 'https://'], '', $url);
                $text = 'Welcome to a new age of Sports Arbitrage Betting ' . $host . '. Earn a profit no matter how the games ends';
                $this->registerMetaTag(['property' => 'og:url', 'content' => $url]);
                $this->registerMetaTag(['property' => 'og:image', 'content' => $url . '/img/sports-banner.jpg']);
                $this->registerMetaTag(['property' => 'og:title', 'content' => $host]);
                $this->registerMetaTag(['property' => 'og:description', 'content' => $text]);
                $this->registerMetaTag(['property' => 'fb:app_id', 'content' => '144588562752230']);
                ?>
                <ul class="list-inline list-social h3 text-xs-center">
                    <li class="list-inline-item list-item-facebook">
                        <a href="#" onClick="window.open('https://www.facebook.com/sharer.php?u=<?= $url ?>','sharer','toolbar=0,status=0,width=548,height=325'); event.preventDefault();">
                            <i class="fa fa-facebook-official"></i>
                        </a>
                    </li>
                    <li class="list-inline-item list-item-twitter">
                        <a href="#" onClick="window.open('https://twitter.com/share?url=<?= $url ?>&text=<?= $text ?>','sharer','toolbar=0,status=0,width=548,height=325'); event.preventDefault();">
                            <i class="fa fa-twitter"></i>
                        </a>
                    </li>
                    <li class="list-inline-item list-item-youtube">
                        <a href="https://www.youtube.com/channel/UCuOWusGnUrlBEvx7IWEUPfg" target="_blank">
                            <i class="fa fa-youtube-play"></i>
                        </a>
                    </li>
                </ul>
                <div class="clearfix"></div>
            </div>
            <div class="col-lg-5">

                <?php
                    $footerMenuItems = [
                        ['label' => 'Terms & Conditions', 'url' => ['help/terms']],
                        ['label' => 'Privacy Policy', 'url' => ['help/terms']],
                        ['label' => 'Contact Us', 'url' => ['site/contact']],
                    ];
                ?>
                <?php echo Menu::widget([
                        'options' => [
                            'class' => 'list-inline text-xs-center float-lg-right'
                        ],
                        'itemOptions' => [
                            'class' => 'list-inline-item'
                        ],
                        'items' => $footerMenuItems,
                    ]);
                ?>
                <div class="clearfix"></div>
            </div>
        </div>
    </div>
    <div class="footer-disclaimer text-xs-center">
        <div class="container">
            ClickBank is the retailer of products on this site. CLICKBANK® is a registered trademark of Click Sales, Inc., a Delaware
            corporation located at 917 S. Lusk Street, Suite 200, Boise Idaho, 83706, USA and used by permission. ClickBank's
            role as retailer does not constitute an endorsement, approval or review of these products or any claim, statement
            or opinion used in promotion of these products.
        </div>
    </div>
</footer>

<?php $this->endBody() ?>

<script>
$(document).ready(function () {
    var surebets;

    function getOdds(offset, update) {

        var min = $('#above').val();
        var max = $('#below').val();
        var interval = $('input[name="interval"]:checked').val();        
        var data = {};

        if(offset) {
            data.offset = offset;
        }

        if(min) {
            data.min = min;
        }

        if(max) {
            data.max = max;
        }

        if(interval) {
            data.interval = interval;
        }

        $.get('<?= Url::to(['subscription/odds']) ?>', data, function (data) {
            var source = $("#table-template").html();
            var template = Handlebars.compile(source);
            var html = template(data);
            $('#tableMain').html(html);

            surebets = data.items;
            showBetDetails(surebets[0], 0);

            if (update != undefined && update !== false) {
                updatePagination(data.count);
            }
        }, 'json');
    }

    function updatePagination(total) {
        $('.table-pagination').twbsPagination({
            totalPages: Math.ceil(total / 20),
            visiblePages: 4,
            first: false,
            prev: false,
            next: false,
            last: false,
            onPageClick: function (event, page) {
                var offset = (page - 1) * 20;
                getOdds(offset, false);
            }
        });
    }

    function showBetDetails(bet, id) {
        <?php if(!Yii::$app->user->isGuest && Yii::$app->user->identity->isSubscriber): ?>
            var source = $("#details-template").html();
            var template = Handlebars.compile(source);
            bet.id = id;
            var html = template(bet);
            $('#details').html(html);
            engageCalc(bet);
        <?php endif; ?>
    }

    getOdds(0, true);

    $('#tableMain').on('click', '.js-surebet', function () {
        var id = $(this).data('id');
        showBetDetails(surebets[id], id);

        <?php if(Yii::$app->user->isGuest): ?>
            $('#modal-register').modal('show')
        <?php endif; ?>

        <?php if(!Yii::$app->user->isGuest && !Yii::$app->user->identity->isSubscriber): ?>
            $('#modal-upgrade').modal('show')
        <?php endif; ?>
        
    })

    $('#tableMain').on('click', 'td', function () {
        var href = $(this).find('.b_img').parent('a').attr('href');
        if (href != undefined) {
            window.open(href);
            $(this).parent('tr').click();
            return false;
        }
    })

    $('#above, #belowe').on('change keyup', function() {
        getOdds(0, true);
    })

    $('.dates .btn').on('click', function() {
        setTimeout(function() {
            getOdds(0, true);
        }, 500);
    });

    // $('input[name="interval"]').on('change', function() {
    //     getOdds(0, true);
    // })

    $('.sticky').Stickyfill();

    function engageCalc(bet) {

        var home_win = parseFloat(bet.home_win);
        var draw = parseFloat(bet.draw);
        var away_win = parseFloat(bet.away_win);
        var bet = parseFloat($('#bet').val());

        var reverse_home_win;
        var reverse_draw;
        var reverse_away_win;
        var ret;
        var profit;

        var tmp;

        // Calculate basic data here
        if (isNaN(home_win) || home_win == 0) {
            reverse_home_win = 0;
        } else {
            reverse_home_win = 1 / home_win;
        }
        if (isNaN(draw) || draw == 0) {
            reverse_draw = 0;
        } else {
            reverse_draw = 1 / draw;
        }
        if (isNaN(away_win) || away_win == 0) {
            reverse_away_win = 0;
        } else {
            reverse_away_win = 1 / away_win;
        }

        ret = reverse_home_win + reverse_draw + reverse_away_win;

        if (ret != 0) {
            profit = 1 / ret;
        } else {
            profit = 0;
        }

        // Main table calculation
        if (isNaN(home_win) || home_win == 0) {
            $("#home_stake").html("");
            $("#home_risk").html("");
            $("#home_profit").html("");
        } else {
            tmp = Math.round(reverse_home_win * profit * 10000) / 100;
            $("#home_stake").html(tmp + '%');
            if (isNaN(bet) || bet == 0) {
                $("#home_risk").html("");
                $("#home_profit").html("");
            } else {
                tmp = Math.round(reverse_home_win * profit * bet * 100) / 100;
                $("#home_risk").html('$' + tmp);
                tmp = Math.round(reverse_home_win * profit * bet * home_win * 100) / 100;
                $("#home_profit").html('$' + tmp);
            }
        }
        if (isNaN(draw) || draw == 0) {
            $("#draw_stake").html("");
            $("#draw_risk").html("");
            $("#draw_profit").html("");
        } else {
            tmp = Math.round(reverse_draw * profit * 10000) / 100;
            $("#draw_stake").html(tmp + '%');
            if (isNaN(bet) || bet == 0) {
                $("#draw_risk").html("");
                $("#draw_profit").html("");
            } else {
                tmp = Math.round(reverse_draw * profit * bet * 100) / 100;
                $("#draw_risk").html('$' + tmp);
                tmp = Math.round(reverse_draw * profit * bet * draw * 100) / 100;
                $("#draw_profit").html('$' + tmp);
            }
        }
        if (isNaN(away_win) || away_win == 0) {
            $("#away_stake").html("");
            $("#away_risk").html("");
            $("#away_profit").html("");
        } else {
            tmp = Math.round(reverse_away_win * profit * 10000) / 100;
            $("#away_stake").html(tmp + '%');
            if (isNaN(bet) || bet == 0) {
                $("#away_risk").html("");
                $("#away_profit").html("");
            } else {
                tmp = Math.round(reverse_away_win * profit * bet * 100) / 100;
                $("#away_risk").html('$' + tmp);
                tmp = Math.round(reverse_away_win * profit * bet * away_win * 100) / 100;
                $("#away_profit").html('$' + tmp);
            }
        }
    }

    $('#details').on('keyup', '#bet', function() {
        var id = $(this).data('id');
        engageCalc(surebets[id]);
    });

    var update_bet_speed = 30000;
    var update_bet_list = true;

    function updateBetList() {
        var activePage = $('ul.pagination .page-item.active .page-link');

        if (activePage.length == 0) {
            update_bet_list = false;
            return false;
        }

        var page = activePage.html();
        var offset = (parseInt(page, 10) - 1) * 20;

        getOdds(offset, false);

        return true;
    }

    setTimeout(function runUpdateBets() {
        if (update_bet_list) {
            updateBetList();
            setTimeout(runUpdateBets, update_bet_speed);
        }
    }, update_bet_speed);

});
</script>
<script id="table-template" type="text/x-handlebars-template">
    <table class="betSlip">
        <tbody>
            {{#each items}}
            <tr class="headSport">
                <td colspan="6">{{sport_name}}, {{competition_name}}</td>
            </tr>
            <tr class="headBet">
                <td{{#if draw_bookmakers}} colspan="2"{{else}} colspan="3"{{/if}}>{{bet_type}}</td>
                <td>{{team1}}</td>
                {{#if draw_bookmakers}}
                <td>{{team_draw}}</td>
                {{/if}}
                <td>{{team2}}</td>
                <td>Profit</td>
            </tr>
            <tr class="body js-surebet" data-id="{{@index}}">
                <td>{{event_date}}<br>{{event_time}}</td>
                <!--<td class="name">{{team1}} - <br>{{team2}}</td>-->
                <td{{#if draw_bookmakers}}{{else}} colspan="2"{{/if}} class="name">{{event_name}}</td>
                <td>
                    <span class="odd">{{home_win}}</span>
                    <?php if(!Yii::$app->user->isGuest && Yii::$app->user->identity->isSubscriber): ?>
                    <br>
                    <a href="<?= Url::to(['site/redirectbookmaker']) ?>?bookmaker_id={{home_bookmakers.0.id}}&bet_id={{bet_id}}&bet_type=home" target="_blank">
                        <img src="//trodds.com/engine/modules/image.php?id={{home_bookmakers.0.id}}" class="b_img">
                    </a>
                    <?php endif; ?>
                </td>
                {{#if draw_bookmakers}}
                <td>
                    <span class="odd">{{draw}}</span>
                    <?php if(!Yii::$app->user->isGuest && Yii::$app->user->identity->isSubscriber): ?>
                    <br>
                    <a href="<?= Url::to(['site/redirectbookmaker']) ?>?bookmaker_id={{draw_bookmakers.0.id}}&bet_id={{bet_id}}&bet_type=draw" target="_blank">
                        <img src="//trodds.com/engine/modules/image.php?id={{draw_bookmakers.0.id}}" class="b_img">
                    </a>
                    <?php endif; ?>
                </td>
                {{/if}}
                <td>
                    <span class="odd">{{away_win}}</span>
                    <?php if(!Yii::$app->user->isGuest && Yii::$app->user->identity->isSubscriber): ?>
                    <br>
                    <a href="<?= Url::to(['site/redirectbookmaker']) ?>?bookmaker_id={{away_bookmakers.0.id}}&bet_id={{bet_id}}&bet_type=away" target="_blank">
                        <img src="//trodds.com/engine/modules/image.php?id={{away_bookmakers.0.id}}" class="b_img">
                    </a>
                    <?php endif; ?>
                </td>
                <td class="profit">{{profit}}%</td>
            </tr>
            {{/each}}
        </tbody>
    </table>
</script>
<script id="details-template" type="text/x-handlebars-template">
    <div class="card bets">
        <div class="card-block">
            <h4 class="card-title text-xs-center mb-0">Beting slip</h4>
        </div>
        <div class="table-odds" id="divBettingSlip">
            <table class="table betSlip invisible_table mb-0">
                <thead>
                    <tr class="headBet">
                        <td class="text-xs-left">Teams</td>
                        <td class="text-center">Odds</td>
                        <td class="text-center" colspan="2">Bookmaker</td>
                    </tr>
                </thead>
                <tbody>
                    <tr class="body">
                        <td class="text-xs-left">1: <span>{{team1}}</span>:</td>
                        <td>
                            <span class="text-right">{{home_win}}</span>
                        </td>
                        <td>
                            <img src="//trodds.com/engine/modules/image.php?id={{home_bookmakers.0.id}}">
                        </td>
                        <td>
                            <a href="<?= Url::to(['site/redirectbookmaker']) ?>?bookmaker_id={{home_bookmakers.0.id}}&bet_id={{bet_id}}&bet_type=home" class="btn btn-sm btn-success" target="_blank">Go to site</a>
                        </td>
                    </tr>
                    {{#if draw_bookmakers}}
                    <tr class="body">
                        <td class="text-xs-left">3:
                            <span>{{team_draw}}</span>:</td>
                        <td>
                            <span class="text-right">{{draw}}</span>
                        </td>
                        <td>
                            <img src="//trodds.com/engine/modules/image.php?id={{draw_bookmakers.0.id}}">
                        </td>
                        <td>
                            <a href="<?= Url::to(['site/redirectbookmaker']) ?>?bookmaker_id={{draw_bookmakers.0.id}}&bet_id={{bet_id}}&bet_type=draw" class="btn btn-sm btn-success" target="_blank">Go to site</a>
                        </td>
                    </tr>
                    {{/if}}
                    <tr class="body">
                        <td class="text-xs-left">2:
                            <span id="second_team_name">{{team2}}</span>:</td>
                        <td>
                            <span class="text-right">{{away_win}}</span>
                        </td>
                        <td>
                            <img src="//trodds.com/engine/modules/image.php?id={{away_bookmakers.0.id}}">
                        </td>
                        <td>
                            <a href="<?= Url::to(['site/redirectbookmaker']) ?>?bookmaker_id={{away_bookmakers.0.id}}&bet_id={{bet_id}}&bet_type=away" class="btn btn-sm btn-success" target="_blank">Go to site</a>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
        <div class="card-block">
            <div class="match-info nb-1">
                <h4 class="card-title text-xs-center">Match Info</h4>
                <p>
                    <strong>Competition:</strong>
                    <span>{{competition_name}}</span>
                </p>
                <p>
                    <strong>Event:</strong>
                    <span>{{event_name}}</span>
                </p>
                <p>
                    <strong>Sport:</strong>
                    <span>{{sport_name}}</span>
                </p>
                <p>
                    <strong>Arb type:</strong>
                    <span>{{bet_type}}</span>
                </p>
            </div>
        </div>
        <div class="table-odds">
            <table class="table betSlip mb-1">
                <tbody>
                    <tr class="headBet">
                        <td>Your bet</td>
                        <td>Home win</td>
                        {{#if draw_bookmakers}}
                        <td>Draw</td>
                        {{/if}}
                        <td>Away win</td>
                    </tr>
                    <tr class="body">
                        <td>
                            <div class="input-group input-group-sm">
                                <span class="input-group-addon">$</span>
                                <input class="form-control" type="text" id="bet" size="4" value="100" data-id="{{id}}">
                            </div>
                        </td>
                        <td id="home_calc">{{home_win}}</td>
                        {{#if draw_bookmakers}}
                        <td id="draw_calc">{{draw}}</td>
                        {{/if}}
                        <td id="away_calc">{{away_win}}</td>
                    </tr>
                </tbody>
            </table>
            <table class="table betSlip">
                <tbody>
                    <tr class="headBet">
                        <td></td>
                        <td>Stake</td>
                        <td>Risk</td>
                        <td>Profit</td>
                    </tr>
                    <tr class="body">
                        <td class="head">Home win</td>
                        <td id="home_stake"></td>
                        <td id="home_risk"></td>
                        <td id="home_profit"></td>
                    </tr>
                    {{#if draw_bookmakers}}
                    <tr class="body">
                        <td class="head">Draw</td>
                        <td id="draw_stake"></td>
                        <td id="draw_risk"></td>
                        <td id="draw_profit"></td>
                    </tr>
                    {{/if}}
                    <tr class="body">
                        <td class="head">Away win</td>
                        <td id="away_stake"></td>
                        <td id="away_risk"></td>
                        <td id="away_profit"></td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
</script>

<?php if(Yii::$app->user->isGuest): ?>
    <div class="modal fade" id="modal-register">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title">Gambling Is For Losers!</h4>
                </div>
                <div class="modal-body">
                    <?= $this->render('../site/_signupForm', [
                        'text' => '<p>Imagine if you knew you would win every time? With sports arbitrage, that’s exactly what happens! Don’t waste a single moment and fill in your details.</p>'
                    ]) ?>
                </div>
                <!--<div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary" name="submit">Upgrade</button>
                </div>-->
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
<?php endif; ?>

<?php if(!Yii::$app->user->isGuest && !Yii::$app->user->identity->isSubscriber): ?>
    <div class="modal fade" id="modal-upgrade">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title">Limited account warning!</h4>
            </div>
            <div class="modal-body">
                <p>In order to take enjoy from the SureBet calculator, as well as see the full details about SureBet situation and take advantage of our full featured <b>Premium Package</b> please upgrade your account just for <b>$99/month</b></p>
            </div>
            <?php $prefix = !empty(Yii::$app->params['paypal']) && !empty(Yii::$app->params['paypal']['sandbox']) ? 'sandbox.' : ''; ?>
            <?php $hosted_button_id = !empty(Yii::$app->params['paypal']) && !empty(Yii::$app->params['paypal']['hosted_button_id']) ? Yii::$app->params['paypal']['hosted_button_id'] : ''; ?>
            <form class="modal-footer" action="https://www.<?= $prefix ?>paypal.com/cgi-bin/webscr" method="post" target="_top">
                <input type="hidden" name="cmd" value="_s-xclick">
                <input type="hidden" name="hosted_button_id" value="<?= $hosted_button_id ?>">
                <input type="hidden" name="custom" value="<?= Yii::$app->user->id ?>">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary" name="submit">Upgrade</button>
            </form>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
<?php endif; ?>

<?php $affiliate = new Affiliate(); ?>
<?= $affiliate->checkPayment(); ?>

<?php $get = Yii::$app->request->get(); ?>
<?php if(!empty($get['paypal']) && $get['paypal'] == 'success'): ?>
    <!-- Google Code for trodds Conversion Page -->
    <script type="text/javascript">
        /* <![CDATA[ */
        var google_conversion_id = 859806119;
        var google_conversion_language = "en";
        var google_conversion_format = "3";
        var google_conversion_color = "ffffff";
        var google_conversion_label = "2e_TCIjI_XEQp7P-mQM";
        var google_remarketing_only = false;
        /* ]]> */
    </script>
    <script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
    </script>
    <noscript>
        <div style="display:inline;">
            <img height="1" width="1" style="border-style:none;" alt="" src="//www.googleadservices.com/pagead/conversion/859806119/?label=2e_TCIjI_XEQp7P-mQM&guid=ON&script=0"/>
        </div>
    </noscript>
<?php endif; ?>

<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-88707511-2', 'auto');
  ga('send', 'pageview');

</script>
</body>
</html>
<?php $this->endPage() ?>
