<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use yii\widgets\Menu;
use frontend\assets\AppAsset;
use common\widgets\Alert;
use frontend\components\Affiliate;
use yii\helpers\Url;
use frontend\widgets\ConfirmEmailChecker;
use frontend\widgets\HeaderWidget;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>

<?php
$homeUrl = Yii::$app->controller->id == 'clickbank' ? ['clickbank/index'] : Url::home();
?>

  <header>
    <div class="header_container<?php if(isset($this->params['page_class'])) echo $this->params['page_class'];?>">
      <div class="header_bg<?php if(isset($this->params['bg_class'])) echo $this->params['bg_class'];?> clearfix">
        <div class="header_nav<?if(isset($this->params['nav_class'])) echo $this->params['nav_class']?> container clearfix">
          <div class="header_logo col-12 col-sm-12 col-md-3">
            <a href="<?php echo Url::home(); ?>">
              <svg xmlns="http://www.w3.org/2000/svg" width="110" height="30" viewBox="0 0 372 102"><g fill="#FFF"><path d="M2.147 40.259h11.558v5.317c-1.377.791-2.446 1.802-3.208 3.032-1.26 1.993-2.109 4.351-2.549 7.075l-.439 2.681c-.147.85-.461 1.516-.945 2s-1.092.725-1.824.725c-.762 0-1.384-.264-1.868-.791s-.725-1.215-.725-2.065V40.259zm34.629 0v51.636c0 1.143.22 1.963.659 2.461.234.293.454.477.659.549.205.074.879.258 2.021.55 1.406.323 2.109 1.128 2.109 2.417 0 .762-.381 1.377-1.143 1.846-.557.352-1.494.527-2.813.527h-22.5c-1.29 0-2.263-.197-2.922-.594-.659-.395-.989-.988-.989-1.779 0-1.289.703-2.094 2.109-2.417 1.143-.292 1.816-.476 2.021-.55.205-.072.41-.256.615-.549.439-.498.659-1.318.659-2.461V40.259h19.515zm15.205 0v17.974c0 .85-.242 1.538-.725 2.065s-1.106.791-1.868.791c-.732 0-1.34-.242-1.824-.725s-.798-1.149-.945-2l-.439-2.681c-.439-2.725-1.29-5.083-2.549-7.075-.791-1.23-1.861-2.241-3.208-3.032v-5.317h11.558zM77.357 40.259v51.636c0 1.143.22 1.963.659 2.461.234.293.454.477.659.549.205.074.879.258 2.021.55 1.406.323 2.109 1.128 2.109 2.417 0 .762-.381 1.377-1.143 1.846-.557.352-1.479.527-2.769.527H56.656c-1.055 0-1.868-.197-2.439-.594-.571-.395-.857-.959-.857-1.691 0-1.201.586-1.992 1.758-2.373.967-.322 1.531-.52 1.692-.594.161-.072.33-.241.505-.505.352-.468.527-1.216.527-2.241v-43.99c0-1.025-.176-1.772-.527-2.241-.176-.264-.345-.432-.505-.505-.161-.073-.725-.271-1.692-.593-1.172-.38-1.758-1.171-1.758-2.373 0-.732.286-1.296.857-1.692s1.384-.593 2.439-.593h20.701zm13.492 30.586c2.695.411 4.768.864 6.218 1.362s2.761 1.172 3.933 2.021c1.23.908 2.175 1.992 2.834 3.252s.989 2.593.989 3.999v9.097c0 .938.308 1.406.923 1.406a.91.91 0 0 0 .593-.197c.161-.133.388-.389.681-.77.732-.996 1.479-1.494 2.241-1.494.498 0 .996.264 1.494.791s.747 1.055.747 1.582c0 .908-.433 2.015-1.296 3.318a15.157 15.157 0 0 1-3.23 3.493c-1.202.967-2.505 1.663-3.911 2.087-1.406.425-3.062.638-4.966.638-3.867 0-6.885-1.106-9.053-3.317-2.168-2.213-3.252-5.296-3.252-9.251v-7.383c0-1.641-.015-2.593-.044-2.856-.147-1.172-.557-2.182-1.23-3.032-.85-1.025-2.065-1.626-3.647-1.802v-4.702c1.582-.234 2.709-.644 3.384-1.23.586-.527.989-1.128 1.208-1.802s.33-1.67.33-2.988V51.421c0-1.846-.359-3.223-1.077-4.131-.718-.908-2.015-1.626-3.889-2.153v-4.878h1.143c3.252 0 6.021.117 8.306.352 3.398.323 6.431 1.436 9.097 3.34 3.838 2.725 5.757 6.607 5.757 11.646 0 4.131-1.29 7.515-3.867 10.151-1.084 1.114-2.351 2.021-3.801 2.725s-3.656 1.493-6.615 2.372zM241.987 40.259v59.985h-20.699c-1.055 0-1.868-.197-2.439-.594-.571-.395-.857-.959-.857-1.691 0-1.201.586-1.992 1.758-2.373.967-.322 1.531-.52 1.692-.594.161-.072.33-.241.505-.505.352-.468.527-1.216.527-2.241V48.257c0-1.025-.176-1.772-.527-2.241-.176-.264-.345-.432-.505-.505-.161-.073-.725-.271-1.692-.593-1.172-.38-1.758-1.171-1.758-2.373 0-.732.286-1.296.857-1.692s1.384-.593 2.439-.593h20.699zm8.876 47.417V53.003c0-2.138-.307-3.838-.922-5.098-.381-.82-.851-1.421-1.407-1.802-.557-.38-1.436-.703-2.637-.967v-4.878c1.553 0 2.871.059 3.956.176 3.72.411 7.323 1.993 10.811 4.746 3.456 2.666 6.02 6.138 7.689 10.415 1.729 4.336 2.594 9.141 2.594 14.414 0 5.977-1.1 11.287-3.297 15.93-2.197 4.645-5.346 8.299-9.447 10.965-1.993 1.289-3.846 2.168-5.56 2.637s-3.962.703-6.746.703v-4.746c1.23-.322 2.087-.615 2.571-.879s.9-.674 1.252-1.23c.439-.645.74-1.369.901-2.176.162-.805.242-1.984.242-3.537zM295.271 40.259v59.985h-20.699c-1.055 0-1.867-.197-2.439-.594-.57-.395-.856-.959-.856-1.691 0-1.201.586-1.992 1.758-2.373.967-.322 1.53-.52 1.692-.594.16-.072.329-.241.505-.505.352-.468.527-1.216.527-2.241V48.257c0-1.025-.176-1.772-.527-2.241-.176-.264-.345-.432-.505-.505-.162-.073-.726-.271-1.692-.593-1.172-.38-1.758-1.171-1.758-2.373 0-.732.286-1.296.856-1.692.572-.396 1.385-.593 2.439-.593h20.699zm8.876 47.417V53.003c0-2.138-.308-3.838-.923-5.098-.381-.82-.85-1.421-1.406-1.802-.557-.38-1.436-.703-2.637-.967v-4.878c1.553 0 2.871.059 3.955.176 3.721.411 7.324 1.993 10.811 4.746 3.457 2.666 6.021 6.138 7.69 10.415 1.729 4.336 2.593 9.141 2.593 14.414 0 5.977-1.099 11.287-3.296 15.93-2.197 4.645-5.347 8.299-9.448 10.965-1.992 1.289-3.846 2.168-5.559 2.637-1.715.469-3.963.703-6.746.703v-4.746c1.23-.322 2.088-.615 2.57-.879.484-.264.901-.674 1.253-1.23a5.874 5.874 0 0 0 .901-2.176c.161-.805.242-1.984.242-3.537zM346.564 39.072v5.054c-1.816.499-3.201 1.268-4.152 2.307-.953 1.04-1.429 2.322-1.429 3.845 0 1.465.396 2.747 1.187 3.845s2.08 2.146 3.867 3.142c1.23.674 4.014 1.963 8.35 3.867 2.637 1.143 4.68 2.175 6.131 3.098 1.449.923 2.818 2.073 4.108 3.45 3.456 3.633 5.186 7.954 5.186 12.964 0 3.779-.938 7.236-2.813 10.371-1.875 3.135-4.454 5.596-7.734 7.383-1.758.938-3.685 1.678-5.778 2.219-2.096.543-4.051.813-5.867.813v-5.01c2.549-.615 4.387-1.472 5.516-2.571 1.127-1.098 1.691-2.585 1.691-4.46 0-1.962-.879-3.691-2.637-5.186-1.172-.967-3.398-2.168-6.68-3.604-3.604-1.582-6.072-2.71-7.404-3.384a26.93 26.93 0 0 1-3.846-2.373c-5.508-3.926-8.262-9.155-8.262-15.688 0-3.34.688-6.423 2.065-9.25s3.296-5.104 5.757-6.833c3.428-2.402 7.676-3.735 12.744-3.999zm-2.285 57.349v5.01c-2.402 0-5.054-.688-7.954-2.065-1.084-.498-1.831-.82-2.241-.967s-.807-.22-1.187-.22c-.587 0-1.026.095-1.318.286-.293.19-.763.666-1.406 1.428-.527.645-1.099.967-1.714.967-1.641 0-2.461-1.26-2.461-3.779V81.084c0-.82.234-1.516.703-2.088.469-.57 1.055-.871 1.758-.9.645-.029 1.149.154 1.516.549.367.396.682 1.092.945 2.088a28.939 28.939 0 0 0 2.922 7.075c1.275 2.197 2.762 4.058 4.461 5.581 1.728 1.582 3.72 2.592 5.976 3.032zm5.757-52.295v-4.834c1.758.117 3.28.345 4.57.681 1.289.337 3.12 1.004 5.493 2 .586.235 1.069.352 1.45.352.556 0 .967-.19 1.23-.571l.967-1.274c.468-.644 1.04-.967 1.714-.967.791 0 1.369.286 1.736.857.365.571.549 1.472.549 2.703v14.502c0 .85-.242 1.545-.725 2.087-.484.542-1.106.813-1.868.813-.587 0-1.128-.19-1.626-.571a2.65 2.65 0 0 1-.637-.857c-.162-.336-.315-.901-.462-1.692-.323-1.435-.879-2.915-1.67-4.438a20.448 20.448 0 0 0-2.769-4.087c-2.107-2.375-4.759-3.942-7.952-4.704z"/></g><g fill-rule="evenodd" clip-rule="evenodd"><circle fill="#FFF" cx="161.795" cy="51.185" r="49"/><path fill="#F00" d="M112.618 48.211c.219-1.682.377-3.373.664-5.043 2.186-12.727 8.371-23.106 18.574-30.992 9.745-7.531 20.88-10.834 33.153-10.068 11.077.691 20.797 4.814 29.201 12.062.492.425.559.744.248 1.32-1.414 2.627-3.318 4.795-5.759 6.457-1.441-1.118-2.796-2.279-4.256-3.286-24.233-16.719-57.276-2.572-61.811 26.513-3.254 20.882 10.552 40.619 31.187 44.812 21.998 4.471 43.323-10.141 47.057-32.256a38.927 38.927 0 0 0-.168-13.966c-.109-.566.028-.778.539-.978 2.561-1.001 5.205-1.392 7.945-1.231.631.037.895.25 1.005.907 2.251 13.393-.386 25.735-8.152 36.868-6.858 9.832-16.185 16.293-27.772 19.396-25.858 6.93-52.963-8.703-59.873-34.578-.808-3.023-1.104-6.183-1.638-9.28-.046-.269-.096-.536-.145-.804l.001-5.853z"/><path fill="#FFF" stroke="#000" stroke-width="3" stroke-miterlimit="10" d="M235.695 22.458c-.736.385-1.483.752-2.209 1.157-6.414 3.592-12.821 7.196-19.246 10.77-.347.192-.853.254-1.246.179-6.117-1.171-11.93-.35-17.383 2.679-7.548 4.191-15.065 8.435-22.607 12.635-.527.293-.719.605-.733 1.222-.136 5.523-4.618 10.027-10.083 10.179-5.763.162-10.436-4.043-10.885-9.795-.421-5.391 3.578-10.333 9.012-11.083 2.516-.348 4.888.139 7.041 1.508.418.266.715.251 1.126.021 7.646-4.3 15.328-8.535 22.94-12.894 5.271-3.02 8.898-7.494 11.035-13.185.144-.381.428-.808.766-.999 7.032-3.972 14.081-7.913 21.127-11.858.116-.065.244-.111.493-.223l-3.93 15.294 3.627 1.04 11.155 3.188v.165z"/><path fill="#F00" d="M191.348 48.258c1.471 10.389-4.067 24.18-17.464 30.122-13.904 6.168-29.873 1.025-37.755-12.068-7.597-12.621-4.546-29.203 7.121-38.482 11.754-9.349 27.16-7.683 36.161-.603-1.572.886-3.112 1.754-4.653 2.619-1.408.791-2.828 1.563-4.224 2.376-.446.26-.82.3-1.319.111-12.282-4.63-25.149 2.699-27.419 15.603-1.892 10.758 5.822 21.504 16.646 23.188 11.355 1.766 21.605-5.719 23.401-17.07.045-.288.226-.664.458-.797 2.927-1.672 5.873-3.307 8.816-4.947.067-.036.153-.035.231-.052z"/></g></svg>
          </a>
          </div>
          <div class="nav_menu col-12 col-sm-12 col-md-9">
          <?php
                $menuItems = [];
                if (Yii::$app->user->isGuest) {
                    if(!($this->context->action->id == 'thankyou' || $this->context->action->id == 'confirm')){
                    if (Yii::$app->controller->id == 'clickbank') {
                        $menuItems = [
                            ['label' => 'Getting started', 'url' => ['clickbank/start']],
                            ['label' => 'Bookies', 'url' => ['bookmaker/index']],
                            ['label' => 'Contact us', 'url' => ['clickbank/contact']],
                        ];
                    } else {
                        $menuItems = [
                            ['label' => 'Getting started', 'url' => ['site/start']],
                            ['label' => 'Bookies', 'url' => ['bookmaker/index']],
                            ['label' => 'Contact us', 'url' => ['site/contact']],
                        ];
                    }
                    if (Yii::$app->controller->id != 'clickbank') {
                        $menuItems[] = ['label' => 'Log In', 'url' => ['/site/login'], 'template' => '<a class="nav_menu_list_item_login_main" href="{url}">{label}</a>', 'options' => ['class' => 'noactive']];
                        $menuItems[] = ['label' => 'Sign Up', 'url' => ['/site/signup'], 'template' => '<a class="nav_menu_list_item_sing_up" href="{url}">{label}</a>', 'options' => ['class' => 'noactive']];
                        }
                    }
                } else {
                    $menuItems = [
//                        ['label' => (Yii::$app->user->identity->role == 40)?'Trial mode':''],
                        ['label' => 'Home', 'url' => ['subscription/index']],
                        ['label' => 'Bookies', 'url' => ['bookmaker/index']],
                        [
                            'label' => 'Settings',
                            'url' => ['settings/account'], 
                            /*'submenuTemplate' => "\n<ul class=\"hidden-xl-down\">\n{items}\n</ul>\n",
                            'items' => [
                                ['label' => 'Account', 'url' => ['settings/account']],
                                ['label' => 'Change password', 'url' => ['settings/changepassword']],
                                ['label' => 'My Bookies', 'url' => ['settings/bookies']],
                            ],*/
                        ],
                        [
                            'label' => 'Help',
                            'url' => ['help/start'],
                            /*'submenuTemplate' => "\n<ul class=\"hidden-xl-down\">\n{items}\n</ul>\n",
                            'items' => [
                                ['label' => 'Getting started', 'url' => ['help/start']],
                                ['label' => 'About TRodds', 'url' => ['help/about']],
                                ['label' => 'Terms, privacy Disclaimer', 'url' => ['help/terms']],
                                ['label' => 'Contact us', 'url' => ['help/contact']],
                            ],*/
                        ],
                    ];
                    $menuItems[] = ['label' => 'Log Out (' . Yii::$app->user->identity->first_name . ')', 'url' => ['/site/logout']];
                }
            ?>
            <?php echo Menu::widget([
                    'options' => [
                        'class' => 'nav_menu_list'
                    ],
                    'itemOptions' => [
                        'class' => 'nav_menu_list_item'
                    ],
                    'activateParents' => true,
                    'linkTemplate' => '<a class="nav-link" href="{url}">{label}</a>',
                    'items' => $menuItems,
                ]);
            ?>
          </div>
        </div>
        <div class="mobile_menu">
          <button class="js-menu menu" type="button">
          <span class="bar"></span>
          </button>

          <div class="mobile_login">
          <?if(Yii::$app->user->isGuest):?>
            <span><a href="/site/login">Log In</a></span>
            <span><a href="/site/signup">Sign Up</a></span>
          <?else:?>
              <span class="mobile_login_isauth"><a href="/site/logout">Log Out (<?=Yii::$app->user->identity->first_name?>)</a></span>
          <?endif;?>
          </div>
          <nav id="mobile_menu">
            <button class="js-menu menu" type="button">
            <span class="bar"></span>
            </button>
            <a class="mobile_link_logo" href="/">
            <img src="/img/Trodds.svg" alt="logo_mobile">
            </a>
            <?$menuItemsMobile = [];
                if (Yii::$app->user->isGuest) {
                    if(!($this->context->action->id == 'thankyou' || $this->context->action->id == 'confirm')){
                        if (Yii::$app->controller->id == 'clickbank') {
                            $menuItemsMobile = [
                                ['label' => 'Getting started', 'url' => ['clickbank/start']],
                                ['label' => 'Bookies', 'url' => ['bookmaker/index']],
                                ['label' => 'Contact us', 'url' => ['clickbank/contact']],
                            ];
                        } else {
                            $menuItemsMobile = [
                                ['label' => 'Home', 'url' => ['subscription/index'], 'template' => '<a class="mobile_home" href="{url}">{label}</a>'],
                                ['label' => 'Bookies', 'url' => ['bookmaker/index'], 'template' => '<a class="mobile_Bookies" href="{url}">{label}</a>'],
                                ['label' => 'Help', 'url' => ['help/start'], 'template' => '<a class="mobile_Help" href="{url}">{label}</a>'],
                                ['label' => 'Contact us', 'url' => ['/help/contact'], 'template' => '<a class="mobile_Contact" href="{url}">{label}</a>'],
                                ['label' => 'Terms of Use', 'url' => ['/help/terms'], 'template' => '<a class="mobile_Terms" href="{url}">{label}</a>'],
                                ['label' => 'Privacy Policy', 'url' => ['/help/privacy'], 'template' => '<a class="mobile_Privacy" href="{url}">{label}</a>'],
                            ];
                        }
                        // $menuItems[] = ['label' => 'Signup', 'url' => ['/site/signup']];
                        // if (Yii::$app->controller->id != 'clickbank') {
                        //     $menuItemsMobile[] = ['label' => 'Log In', 'url' => ['/site/login'], 'template' => '<a class="mobile_upgrade" href="{url}">{label}</a>', 'options' => ['class' => 'noactive']];
                        //     $menuItemsMobile[] = ['label' => 'Sign Up', 'url' => ['/site/signup'], 'template' => '<a class="moble_logout" href="{url}">{label}</a>', 'options' => ['class' => 'noactive']];
                        // }
                    }
                } else {
                    $menuItemsMobile = [
                        ['label' => 'Home', 'url' => ['subscription/index'], 'template' => '<a class="mobile_home" href="{url}">{label}</a>'],
                        ['label' => 'Bookies', 'url' => ['bookmaker/index'], 'template' => '<a class="mobile_Bookies" href="{url}">{label}</a>'],
                        ['label' => 'Settings', 'url' => ['settings/account'], 'template' => '<a class="mobile_Settings" href="{url}">{label}</a>'],
                        ['label' => 'Help', 'url' => ['help/start'], 'template' => '<a class="mobile_Help" href="{url}">{label}</a>'],
                        ['label' => 'Contact us', 'url' => ['/help/contact'], 'template' => '<a class="mobile_Contact" href="{url}">{label}</a>'],
                        ['label' => 'Terms of Use', 'url' => ['/help/terms'], 'template' => '<a class="mobile_Terms" href="{url}">{label}</a>'],
                        ['label' => 'Privacy Policy', 'url' => ['/help/privacy'], 'template' => '<a class="mobile_Privacy" href="{url}">{label}</a>'],
                    ];
                }?>
            <?php echo Menu::widget([
                    'items' => $menuItemsMobile,
                ]);
            ?>
            <?php if (Yii::$app->user->isGuest) : ?>
                <a class="mobile_upgrade" href="/site/login">Log In</a>
                <a class="moble_logout" href="/site/signup">Sign Up</a>
            <?php else: ?>
                <a class="mobile_upgrade" href="/site/deposit">Upgrade</a>
                <a class="moble_logout" href="/site/logout">Log Out</a>
            <?php endif; ?>
          </nav>
        </div>
        <?=HeaderWidget::widget();?>
      </div>
      
  </header>

    <?=$content?>

  <footer>
    <div class="footer_copyright">
      All rights reseved to Top Rated Odds © 2013-2017
    </div>
    <div class="footer_social">
    <?php
      $url = Url::base(true);
      $host = str_replace(['http://', 'https://'], '', $url);
      $text = 'Welcome to a new age of Sports Arbitrage Betting ' . $host . '. Earn a profit no matter how the games ends';
      $this->registerMetaTag(['property' => 'og:url', 'content' => $url]);
      $this->registerMetaTag(['property' => 'og:image', 'content' => $url . '/img/sports-banner.jpg']);
      $this->registerMetaTag(['property' => 'og:title', 'content' => $host]);
      $this->registerMetaTag(['property' => 'og:description', 'content' => $text]);
      $this->registerMetaTag(['property' => 'fb:app_id', 'content' => '144588562752230']);
      ?>
      <ul>
        <li>
          <a class="footer_social_facebook" href="#" onClick="window.open('https://www.facebook.com/sharer.php?u=<?= $url ?>','sharer','toolbar=0,status=0,width=548,height=325'); event.preventDefault();">
          </a>
        </li>
        <li>
          <a class="footer_social_twitter" href="#" onClick="window.open('https://twitter.com/share?url=<?= $url ?>&text=<?= $text ?>','sharer','toolbar=0,status=0,width=548,height=325'); event.preventDefault();">
          </a>
        </li>
        <li>
          <a class="footer_social_youtube" href="https://www.youtube.com/channel/UCuOWusGnUrlBEvx7IWEUPfg" target="_blank">
          </a>
        </li>
      </ul>
    </div>
    <div class="footer_menu clearfix">

        <?php
            $footerMenuItems = [
                ['label' => 'Terms of Use', 'url' => ['help/terms']],
                ['label' => 'Privacy Policy', 'url' => ['help/privacy']],
                ['label' => 'Contact Us', 'url' => ['site/contact']],
            ];
        ?>
        <?php echo Menu::widget([
                'options' => [
                    'class' => ''
                ],
                'itemOptions' => [
                    'class' => ''
                ],
                'items' => $footerMenuItems,
            ]);
        ?>
    </div>
  </footer>

<?php $this->endBody() ?>
<script>
$(document).ready(function () {
    var surebets;

    function getOdds(offset, update) {

        var min = $('#above').val();
        var max = $('#below').val();
        var interval = $('input[name="interval"]:checked').val();        
        var data = {};

        if(offset) {
            data.offset = offset;
        }

        if(min) {
            data.min = min;
        }

        if(max) {
            data.max = max;
        }

        if(interval) {
            data.interval = interval;
        }

        $.get('<?= Url::to(['subscription/odds']) ?>', data, function (data) {
            var source = $("#table-template").html();
            var template = Handlebars.compile(source);
            var html = template(data);
            <?if(Yii::$app->user->isGuest):?>
              $('#tableMain_iPhone').html(html);
            <?endif;?>
            
            $('#tableMain').html(html);
            
            
            surebets = data.items;
            showBetDetails(surebets[0], 0);

            if (update != undefined && update !== false) {
                updatePagination(data.count);
            }
        }, 'json');
    }

    function updatePagination(total) {
        $('.table-pagination').twbsPagination({
            totalPages: Math.ceil(total / 20),
            visiblePages: 4,
            first: false,
            prev: false,
            next: false,
            last: false,
            onPageClick: function (event, page) {
                var offset = (page - 1) * 20;
                getOdds(offset, false);
            }
        });
    }

    function showBetDetails(bet, id) {
        <?php if(!Yii::$app->user->isGuest && Yii::$app->user->identity->isSubscriber): ?>
            var source = $("#details-template").html();
            var template = Handlebars.compile(source);
            bet.id = id;
            var html = template(bet);
            $('#details').html(html);
            engageCalc(bet);
        <?php endif; ?>
    }

    getOdds(0, true);

    $('#tableMain').on('click', '.js-surebet', function () {
        var id = $(this).data('id');
        showBetDetails(surebets[id], id);

        <?php if(Yii::$app->user->isGuest): ?>
            $('#modal-register').modal('show')
        <?php endif; ?>

        <?php if(!Yii::$app->user->isGuest && !Yii::$app->user->identity->isSubscriber): ?>
            $('#modal-upgrade').modal('show')
        <?php endif; ?>
        
    })

    $('#tableMain').on('click', 'td', function () {
        var href = $(this).find('.b_img').parent('a').attr('href');
        if (href != undefined) {
            window.open(href);
            $(this).parent('tr').click();
            return false;
        }
    })

    $('#above, #belowe').on('change keyup', function() {
        getOdds(0, true);
    })

    $('.dates .btn').on('click', function() {
        setTimeout(function() {
            getOdds(0, true);
        }, 500);
    });

    // $('input[name="interval"]').on('change', function() {
    //     getOdds(0, true);
    // })

    $('.sticky').Stickyfill();

    function engageCalc(bet) {

        var home_win = parseFloat(bet.home_win);
        var draw = parseFloat(bet.draw);
        var away_win = parseFloat(bet.away_win);
        var bet = parseFloat($('#bet').val());

        var reverse_home_win;
        var reverse_draw;
        var reverse_away_win;
        var ret;
        var profit;

        var tmp;

        // Calculate basic data here
        if (isNaN(home_win) || home_win == 0) {
            reverse_home_win = 0;
        } else {
            reverse_home_win = 1 / home_win;
        }
        if (isNaN(draw) || draw == 0) {
            reverse_draw = 0;
        } else {
            reverse_draw = 1 / draw;
        }
        if (isNaN(away_win) || away_win == 0) {
            reverse_away_win = 0;
        } else {
            reverse_away_win = 1 / away_win;
        }

        ret = reverse_home_win + reverse_draw + reverse_away_win;

        if (ret != 0) {
            profit = 1 / ret;
        } else {
            profit = 0;
        }

        // Main table calculation
        if (isNaN(home_win) || home_win == 0) {
            $("#home_stake").html("");
            $("#home_risk").html("");
            $("#home_profit").html("");
        } else {
            tmp = Math.round(reverse_home_win * profit * 10000) / 100;
            $("#home_stake").html(tmp + '%');
            if (isNaN(bet) || bet == 0) {
                $("#home_risk").html("");
                $("#home_profit").html("");
            } else {
                tmp = Math.round(reverse_home_win * profit * bet * 100) / 100;
                $("#home_risk").html('$' + tmp);
                tmp = Math.round(reverse_home_win * profit * bet * home_win * 100) / 100;
                $("#home_profit").html('$' + tmp);
            }
        }
        if (isNaN(draw) || draw == 0) {
            $("#draw_stake").html("");
            $("#draw_risk").html("");
            $("#draw_profit").html("");
        } else {
            tmp = Math.round(reverse_draw * profit * 10000) / 100;
            $("#draw_stake").html(tmp + '%');
            if (isNaN(bet) || bet == 0) {
                $("#draw_risk").html("");
                $("#draw_profit").html("");
            } else {
                tmp = Math.round(reverse_draw * profit * bet * 100) / 100;
                $("#draw_risk").html('$' + tmp);
                tmp = Math.round(reverse_draw * profit * bet * draw * 100) / 100;
                $("#draw_profit").html('$' + tmp);
            }
        }
        if (isNaN(away_win) || away_win == 0) {
            $("#away_stake").html("");
            $("#away_risk").html("");
            $("#away_profit").html("");
        } else {
            tmp = Math.round(reverse_away_win * profit * 10000) / 100;
            $("#away_stake").html(tmp + '%');
            if (isNaN(bet) || bet == 0) {
                $("#away_risk").html("");
                $("#away_profit").html("");
            } else {
                tmp = Math.round(reverse_away_win * profit * bet * 100) / 100;
                $("#away_risk").html('$' + tmp);
                tmp = Math.round(reverse_away_win * profit * bet * away_win * 100) / 100;
                $("#away_profit").html('$' + tmp);
            }
        }
    }

    $('#details').on('keyup', '#bet', function() {
        var id = $(this).data('id');
        engageCalc(surebets[id]);
    });

    var update_bet_speed = 30000;
    var update_bet_list = true;

    function updateBetList() {
        var activePage = $('ul.pagination .page-item.active .page-link');

        if (activePage.length == 0) {
            update_bet_list = false;
            return false;
        }

        var page = activePage.html();
        var offset = (parseInt(page, 10) - 1) * 20;

        getOdds(offset, false);

        return true;
    }

    setTimeout(function runUpdateBets() {
        if (update_bet_list) {
            updateBetList();
            setTimeout(runUpdateBets, update_bet_speed);
        }
    }, update_bet_speed);

});
</script>
<script id="table-template" type="text/x-handlebars-template">
    {{#each items}}
    <table class="betSlip">
      <caption>{{sport_name}}, {{competition_name}}</caption>
      <tr class="headBet">
          <th{{#if draw_bookmakers}} colspan="2"{{else}} colspan="3"{{/if}}>{{bet_type}}</th>
          <th>{{team1}}</th>
          {{#if draw_bookmakers}}
          <th>{{team_draw}}</th>
          {{/if}}
          <th>{{team2}}</th>
          <th>Profit</th>
      </tr>
      <tr class="body js-surebet" data-id="{{@index}}">
          <td>{{event_date}}<br>{{event_time}}</td>
          <!--<td class="name">{{team1}} - <br>{{team2}}</td>-->
          <td{{#if draw_bookmakers}}{{else}} colspan="2"{{/if}} class="name">{{event_name}}</td>
          <td>
              <span class="odd">{{home_win}}</span>
              <?php if(!Yii::$app->user->isGuest && Yii::$app->user->identity->isSubscriber): ?>
              <br>
              <a href="<?= Url::to(['site/redirectbookmaker']) ?>?bookmaker_id={{home_bookmakers.0.id}}&bet_id={{bet_id}}&bet_type=home" target="_blank">
                  <img src="//trodds.com/engine/modules/image.php?id={{home_bookmakers.0.id}}" class="b_img">
              </a>
              <?php endif; ?>
          </td>
          {{#if draw_bookmakers}}
          <td>
              <span class="odd">{{draw}}</span>
              <?php if(!Yii::$app->user->isGuest && Yii::$app->user->identity->isSubscriber): ?>
              <br>
              <a href="<?= Url::to(['site/redirectbookmaker']) ?>?bookmaker_id={{draw_bookmakers.0.id}}&bet_id={{bet_id}}&bet_type=draw" target="_blank">
                  <img src="//trodds.com/engine/modules/image.php?id={{draw_bookmakers.0.id}}" class="b_img">
              </a>
              <?php endif; ?>
          </td>
          {{/if}}
          <td>
              <span class="odd">{{away_win}}</span>
              <?php if(!Yii::$app->user->isGuest && Yii::$app->user->identity->isSubscriber): ?>
              <br>
              <a href="<?= Url::to(['site/redirectbookmaker']) ?>?bookmaker_id={{away_bookmakers.0.id}}&bet_id={{bet_id}}&bet_type=away" target="_blank">
                  <img src="//trodds.com/engine/modules/image.php?id={{away_bookmakers.0.id}}" class="b_img">
              </a>
              <?php endif; ?>
          </td>
          <td class="profit">{{profit}}%</td>
      </tr>     
    </table>
    {{/each}}
</script>
<script id="details-template" type="text/x-handlebars-template">
        <table class="table betSlip invisible_table mb-0">
            <caption>betting slip</caption>
            <tr class="headBet">
                <th class="text-xs-left">Teams</th>
                <th class="text-center">Odds</th>
                <th class="text-center" colspan="2">Bookmaker</th>
            </tr>
                <tr class="body">
                    <td class="text-xs-left">1: <span>{{team1}}</span>:</td>
                    <td>
                        <span class="text-right">{{home_win}}</span>
                    </td>
                    <td>
                        <img src="//trodds.com/engine/modules/image.php?id={{home_bookmakers.0.id}}">
                    </td>
                    <td>
                        <a href="<?= Url::to(['site/redirectbookmaker']) ?>?bookmaker_id={{home_bookmakers.0.id}}&bet_id={{bet_id}}&bet_type=home" class="btn btn-sm btn-success" target="_blank">Site</a>
                    </td>
                </tr>
                {{#if draw_bookmakers}}
                <tr class="body">
                    <td class="text-xs-left">3:
                        <span>{{team_draw}}</span>:</td>
                    <td>
                        <span class="text-right">{{draw}}</span>
                    </td>
                    <td>
                        <img src="//trodds.com/engine/modules/image.php?id={{draw_bookmakers.0.id}}">
                    </td>
                    <td>
                        <a href="<?= Url::to(['site/redirectbookmaker']) ?>?bookmaker_id={{draw_bookmakers.0.id}}&bet_id={{bet_id}}&bet_type=draw" class="btn btn-sm btn-success" target="_blank">Site</a>
                    </td>
                </tr>
                {{/if}}
                <tr class="body">
                    <td class="text-xs-left">2:
                        <span id="second_team_name">{{team2}}</span>:</td>
                    <td>
                        <span class="text-right">{{away_win}}</span>
                    </td>
                    <td>
                        <img src="//trodds.com/engine/modules/image.php?id={{away_bookmakers.0.id}}">
                    </td>
                    <td>
                        <a href="<?= Url::to(['site/redirectbookmaker']) ?>?bookmaker_id={{away_bookmakers.0.id}}&bet_id={{bet_id}}&bet_type=away" class="btn btn-sm btn-success" target="_blank">Site</a>
                    </td>
                </tr>
        </table>

        <div class="home_user_left_titles">
            <h4>Match Info</h4>
            <p><span>Sport: </span>{{sport_name}}</p>
            <p><span>Competition: </span>{{competition_name}}</p>
            <p><span>Event: </span>{{event_name}}s</p>
            <p><span>Date: </span>{{event_date}}</p>
            <p><span>Bet Type: </span>{{bet_type}}</p>
        </div>
        <div class="home_user_table_two">
              <table>
                <tr>
                  <th> Total Investment</th>
                  <th><input class="form-control" type="text" id="bet" size="4" value="100" data-id="{{id}}"></th>
                </tr>
              </table>
        </div>
        <table>
            <tr>
              <th>Result</th>
              <th>Stakes</th>
              <th>Risk</th>
              <th>Return</th>
            </tr>
            <tr>
              <td>Home win</td>
              <td id="home_stake">22.09</td>
              <td id="home_risk">22.09</td>
              <td id="home_profit" class="dollars_profit">106.91</td>
              <!-- <td id="home_profit" class="dollars_profit" rowspan="3">106.91</td> -->
            </tr>
            {{#if draw_bookmakers}}
            <tr>
              <td>Draw</td>
              <td id="draw_stake">29.53</td>
              <td id="draw_risk">29.53</td>
              <td id="draw_profit" class="dollars_profit">106.91</td>
            </tr>
            {{/if}}
            <tr>
              <td>Away win</td>
              <td id="away_stake">29.53</td>
              <td id="away_risk">48.38</td>
              <td id="away_profit" class="dollars_profit">106.91</td>
            </tr>
        </table>
</script>


<?php if(!Yii::$app->user->isGuest && !Yii::$app->user->identity->isSubscriber && false): ?>
    <div class="modal fade" id="modal-upgrade">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title">Limited account warning!</h4>
            </div>
            <div class="modal-body">
                <p>In order to take enjoy from the SureBet calculator, as well as see the full details about SureBet situation and take advantage of our full featured <b>Premium Package</b> please upgrade your account just for <b>€9.99/month</b></p>
            </div>
            <?php $prefix = !empty(Yii::$app->params['paypal']) && !empty(Yii::$app->params['paypal']['sandbox']) ? 'sandbox.' : ''; ?>
            <?php $hosted_button_id = !empty(Yii::$app->params['paypal']) && !empty(Yii::$app->params['paypal']['hosted_button_id']) ? Yii::$app->params['paypal']['hosted_button_id'] : ''; ?>
            <form class="modal-footer" action="https://www.<?= $prefix ?>paypal.com/cgi-bin/webscr" method="post" target="_top">
                <input type="hidden" name="cmd" value="_s-xclick">
                <input type="hidden" name="hosted_button_id" value="<?= $hosted_button_id ?>">
                <input type="hidden" name="custom" value="<?= Yii::$app->user->id ?>">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary" name="submit">Upgrade</button>
            </form>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
<?php endif; ?>

<?php $affiliate = new Affiliate(); ?>
<?= $affiliate->checkPayment(); ?>

<?php $get = Yii::$app->request->get(); ?>
<?php if(!empty($get['paypal']) && $get['paypal'] == 'success'): ?>
    <!-- Google Code for trodds Conversion Page -->
    <script type="text/javascript">
        /* <![CDATA[ */
        var google_conversion_id = 859806119;
        var google_conversion_language = "en";
        var google_conversion_format = "3";
        var google_conversion_color = "ffffff";
        var google_conversion_label = "2e_TCIjI_XEQp7P-mQM";
        var google_remarketing_only = false;
        /* ]]> */
    </script>
    <script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
    </script>
    <noscript>
        <div style="display:inline;">
            <img height="1" width="1" style="border-style:none;" alt="" src="//www.googleadservices.com/pagead/conversion/859806119/?label=2e_TCIjI_XEQp7P-mQM&guid=ON&script=0"/>
        </div>
    </noscript>
<?php endif; ?>

<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-88707511-2', 'auto');
  ga('send', 'pageview');

</script>
</body>
</html>
<?php $this->endPage() ?>
