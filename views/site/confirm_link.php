<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


use yii\bootstrap\ActiveForm;
use yii\bootstrap\Html;

$this->title = 'Congratulations!';
?>
<div style="max-width: 870px; margin: 0 auto; text-align: center; padding: 30px; padding-bottom: 0px;">
    Congratulations!<br> You have successfully activated your Premium Trial account!<br>
    Simply press the "Proceed" button and you will be immediately redirected to your TRodds Premium Trial account, after that your login details will be sent to you via email.


</div>
<div style="max-width: 400px; margin: 0 auto; text-align: center; padding: 30px;">
<?php
$form = ActiveForm::begin([
        'id' => 'form-confirm',        
    ]);
    echo $form->field($model, 'code')->hiddenInput(['placeholder' => '32-digits code', 'value' => $code])->label(false);
    echo Html::submitButton('Proceed', ['class' => 'btn btn-success', 'name' => 'signup-button']);
    ActiveForm::end();
?>
</div>
<div style="max-width: 720px; margin: 0 auto; text-align: center; padding: 30px; padding-top: 0px;">
The trial account gives you full access to all arbitrages, all bookies, best payment suppliers, our unique Surebet calculator and much more.
</div>