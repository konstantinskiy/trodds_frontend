<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\PasswordResetRequestForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Request password reset';
$this->params['breadcrumbs'][] = $this->title;
$this->params['page_class'] = '_reset';

?>

<main>
    <div class="reset_container">
        <h2>Please fill out your email.</h2>
        <h2>A link to reset password will be sent there.</h2>
        
        <?php 
            $form = ActiveForm::begin(
                [
                    'id' => 'request-password-reset-form',
                    'options' => [
                        'class' => 'form_contact_settings',
                    ],
                ]
            );

            echo $form->field($model, 'email', [
                'template' => '<p class="form_contact_email">Email<span id="true"></span><span id="false"></span></p>{input}',
                ])->textInput(['placeholder' => 'email@user.com']); ?>
            
            <div class="reset-password-submit-row">
                <?= Html::submitButton('Send', ['class' => 'subs_form_button', 'name' => 'reset-button']) ?>
            </div>
        

        <?php ActiveForm::end(); ?>
    </div>
</main>