<?php

/* @var $this yii\web\View */

use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
use frontend\models\SignupForm;

$this->title = 'Thank You!';
$this->params['breadcrumbs'][] = $this->title;

$this->registerJsFile('/engine/js/jquery_cookie.js');
$this->registerJs('
    $.ajax({
        type:"GET",
        url: "/engine/modules/track_ack.php?uid='.Yii::$app->user->id.'"
    });
');

?>

<section class="section section-image section-faded" style="background-image:url('subscription/img/banner2.jpg'); background-position:center center;">
    <div class="container">
        <h2 class="text-xs-center"><?= Html::encode($this->title) ?></h2>
    </div>
</section>
<section class="section">
    <div class="container">
        <div class="h4 text-xs-center">
            Thank you for subscribing!
        </div>
        <div class="text-xs-center" style="max-width: 600px;margin: 0 auto;">
            <div class="text-xs-center" id="example-caption-1">Processing application...</div>
            <div class="pr"><div class="pr-bar"></div></div>
            Check your email box at : <b><?= $email ?></b> your login password is there<br>
            <span class="text-warning">You will be redirected to TRodds System in 5 seconds</span>
        </div>
    </div>
</section>

<script>

</script>
