<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\Modal;
?>

<?php Modal::begin([
    'id' => 'confirm-terms-modal',
    'header' => Html::tag('h3', 'Confirm Login using Networks'),
]); ?>

<?= Html::checkbox('agree-checkbox', false, [
    'id' => 'agree-checkbox',
    'label' => 'I agree to TRodds\'s <a href="' . Url::to(['help/terms']). '" target="_blank">terms and conditions</a> and <a href="' . Url::to(['help/terms']). '" target="_blank">privacy policy</a>.'
]) ?>

<?php Modal::end(); ?>