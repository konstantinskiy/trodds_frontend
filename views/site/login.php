<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \common\models\LoginForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\authclient\widgets\AuthChoice;
use yii\bootstrap\Modal;
use frontend\widgets\BookmakersWidget;

$this->title = 'Log In';
$this->params['breadcrumbs'][] = $this->title;

$this->params['page_class'] = '_login';
?>

<main>
    <div class="social_sing_up">
      <div class="social_sing_list">
        <a class="social_login_list_facebook" href="/auth?authclient=facebook"></a>
        <a class="social_login_list_twitter" href="/auth?authclient=twitter"></a>
        <a class="social_login_list_google" href="/auth?authclient=google"></a>
      </div>
      <p class="social_sing_up_or">Or</p>
    </div>
    <div class="main_login">
      <?php $form = ActiveForm::begin(['id' => 'login-form', 'options' => ['class' => 'form_login']]); ?>
        <p class="form_contact_email">Email<span id="true"></span><span id="false"></span></p>
        <?= $form->field($model, 'email', ['template' => '{input}{error}'])->textInput(['placeholder' => 'email@user.com']) ?>
        <label class="form_contact_password required" for="username">Password</label>
        <?= $form->field($model, 'password', ['template' => '{input}{error}'])->passwordInput(['rel' => 'to-replace', 'class' => 'form-control', 'placeholder' => 'password']) ?>
        <?= $form->field($model, 'rememberMe', ['template' => '{input}{error}'])->checkbox(['class' => 'checkbox_input'])->label(false) ?>
        <i class="login_remember">Remember me</i>
        <label class="checkbox_label" for="subscribeNews">If you forgot your password you can <?= Html::a('reset', ['site/request-password-reset']) ?> it.</label>
        <input type="submit" name="" value="Log In">
      <?php ActiveForm::end(); ?>
    </div>
<?=BookmakersWidget::widget()?>
</main>



<?/*= AuthChoice::widget([
    'baseAuthUrl' => ['site/auth'],
    'popupMode' => false,
])*/ ?>


<?//= $this->render('_confirmModal'); ?>