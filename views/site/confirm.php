<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

use yii\bootstrap\ActiveForm;
use yii\bootstrap\Html;

$this->title = 'Confirmation!';
?>
<div style="max-width: 900px; margin: 0 auto; text-align: center; padding: 30px; padding-bottom: 0px;">
Insert the 32 digits code that you received to your email in the form below and press on the "Confirm" button.
</div>
<div style="max-width: 400px; margin: 0 auto; text-align: center; padding: 30px;">
<?php
$form = ActiveForm::begin([
        'id' => 'form-confirm',        
    ]);
    echo $form->field($model, 'code')->textInput(['placeholder' => '32-digits code'])->label(false);
    echo Html::submitButton('Confirm', ['class' => 'btn btn-success', 'name' => 'signup-button']);
    ActiveForm::end();
?>
</div>
<div style="max-width: 900px; margin: 0 auto; text-align: center; padding: 30px; padding-top: 0px;">
You will be redirected after you press "confirm" to your TRodds Premium Trial account, Where you can enjoy full access to all arbitrages, all bookies, best payment suppliers, our unique Surebet calculator, and much more.
</div>
