<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use frontend\models\SignupForm;
$signupFormModel = new SignupForm();
?>
    <?php if (Yii::$app->user->isGuest): ?>
    <?php $form = ActiveForm::begin([
            'id' => 'form-signup',
            'action' => Yii::$app->controller->id == 'clickbank' ? ['clickbank/signup'] : ['site/signup'],
            'method' => 'post',
            'errorCssClass' => 'has-danger',
            'fieldConfig' => [
                'hintOptions' => [
                    'class' => 'hidden-xs-up'
                ],
                'errorOptions' => [
                    'class' => 'hidden-xs-up',
                    'tag' => 'div',
                ]
            ],
            'options' => [
                'class' => 'subs-form '. (Yii::$app->controller->id == 'clickbank' ? '' : '  pt-2')
            ],
        ]); 
    ?>

        <?php print(isset($text) ? $text : ''); ?>
        
        <div class="subs_form_name_row">
            <div class="subs_form_name_line">
                <?= $form->field($signupFormModel, 'first_name')->textInput(['placeholder' => 'Name', 'class' => 'subs_form_name'])->label(false); ?>
            </div>
            <div class="subs_form_name_line">
                <?= $form->field($signupFormModel, 'email')->textInput()->input('email', ['placeholder' => 'Email', 'class' => 'subs_form_email'])->label(false); ?>
            </div>
            <div class="subs_form_name_line">
                <?= $form->field($signupFormModel, 'password')->textInput()->input('password', ['placeholder' => 'Password', 'class' => 'subs_form_password'])->label(false); ?>
            </div>
        </div>

        <?php if (Yii::$app->controller->id != 'clickbank'): ?>
            <div class="form-group mt-1 subs_form_submit_row">
                <div class="btn-group subscribe-group">
                    <?= Html::submitButton('Sign Up', ['class' => 'subs_form_button', 'name' => 'signup-button']) ?>
                </div>
            </div>
        <?php else: ?>
            <div class="form-group mt-1 subs_form_submit_row">
                <div class="btn-group subscribe-group">
                    <?= Html::submitButton('Subscribe Now!', ['class' => 'subs_form_button', 'name' => 'signup-button']) ?>
                </div>
            </div>
        <?php endif; ?>
        <?php if(Yii::$app->controller->id == 'clickbank'): ?>
        <div class="small text-muted">
            Only for
            <strike>$499/month</strike> $9.99/month as long as you use it.
            <br> You can cancel any time. No problem. Full refund guarantee as well.
        </div>
        <?php endif; ?>
    <?php ActiveForm::end(); ?>
<?php else: ?>

    <?php if (!Yii::$app->user->identity->isSubscriber): ?>
        <div class="card-block">
            <h4 class="card-title">Limited account warning!</h4>
            <p class="card-text">In order to take enjoy from the SureBet calculator, as well as see the full details about SureBet situation and take advantage of our full featured <b>Premium Package</b> please upgrade your account just for <b>$9.99/month</b></p>
            <?php $prefix = !empty(Yii::$app->params['paypal']) && !empty(Yii::$app->params['paypal']['sandbox']) ? 'sandbox.' : ''; ?>
            <?php $hosted_button_id = !empty(Yii::$app->params['paypal']) && !empty(Yii::$app->params['paypal']['hosted_button_id']) ? Yii::$app->params['paypal']['hosted_button_id'] : ''; ?>
            <form action="https://www.<?= $prefix ?>paypal.com/cgi-bin/webscr" method="post" target="_top">
                <input type="hidden" name="cmd" value="_s-xclick">
                <input type="hidden" name="hosted_button_id" value="<?= $hosted_button_id ?>">
                <input type="hidden" name="custom" value="<?= Yii::$app->user->id ?>">
                <input type="submit" class="btn btn-primary" name="submit" value="Upgrade">
            </form>
        </div>
    <?php endif; ?>

<?php endif; ?>