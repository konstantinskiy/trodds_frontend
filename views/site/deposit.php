<?php

/* @var $this yii\web\View */

use yii\helpers\Html;
use frontend\widgets\BookmakersWidget;

$this->title = 'Getting started';
$this->params['breadcrumbs'][] = $this->title;

$this->params['page_class'] = '_main';

?>

<main>
  <div class="deposit_main">
    <h2 class="deposit_title">Limited account warning!</h2>
    <p class="deposit_text">In order to get access to the full information on the current arbitrage situations in the sport betting world and to be able to pick up the hottest arbs, please subscribe to the full featured <span>TRodds Premium Package</span> - now just for €9.99/month.</p>
    <?php $prefix = !empty(Yii::$app->params['paypal']) && !empty(Yii::$app->params['paypal']['sandbox']) ? 'sandbox.' : ''; ?>
    <?php $hosted_button_id = !empty(Yii::$app->params['paypal']) && !empty(Yii::$app->params['paypal']['hosted_button_id']) ? Yii::$app->params['paypal']['hosted_button_id'] : ''; ?>
    <form action="https://www.<?= $prefix ?>paypal.com/cgi-bin/webscr" method="post" target="_top">
        <input type="hidden" name="cmd" value="_s-xclick">
        <input type="hidden" name="hosted_button_id" value="<?= $hosted_button_id ?>">
        <input type="hidden" name="custom" value="<?= Yii::$app->user->id ?>">
        <input type="submit" class="deposit_sebscribe" name="submit" value="Subscribe">
    </form>
    <h2 class="deposit_title">Satisfaction Guarantee</h2>
    <p class="deposit_text">To ensure customer protection our company offers a 15 days money back guarantee. If you are not satisfied with our service, you have the unconditional right to claim and receive a refund within 15 days after the purchase date. A request for a refund
      must be submitted via our Contact Form.</a> We also believe that most of the refunds can be avoided. Please contact our qualified staff and you will get professional assistance in any issue you have been faced with while
      using our services.</p>
  </div>
</main>