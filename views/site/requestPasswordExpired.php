<?php
use yii\helpers\Html;

$this->title = 'Reset link is expired';
?>
<section class="section">
    <div class="container">

        <div class="row">
            <div class="push-lg-3 col-lg-6">
                <h1><?= Html::encode($this->title) ?></h1>

                <p>This password reset link has been expired. If you forgot your password you can <?= Html::a('reset it', ['site/request-password-reset']) ?>.</p>
            </div>
        </div>
    </div>
</section>