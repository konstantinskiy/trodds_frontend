<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

use yii\bootstrap\ActiveForm;
use yii\bootstrap\Html;

$this->title = 'Thank You!';

$this->registerJsFile('/engine/js/jquery_cookie.js');
$this->registerJs('
    $.ajax({
        type:"GET",
        url: "/engine/modules/track_ack.php?uid='.$id.'"
    });
');
?>
<section class="section section-image section-faded" style="background-image:url('subscription/img/banner2.jpg'); background-position:center center;">
    <div class="container">
        <h2 class="text-xs-center">Thank you for signing up!</h2>
    </div>
</section>
<section class="section">
    <div class="container">
        <div class="text-xs-center" style="max-width: 700px;margin: 0 auto;">
            Part of successful premium account trial promotion we decided to prolong it, all you have to receive your free trial is follow instructions that was sent to this email <b>
<input type="text" disabled value="<?= $email ?>" style="text-align: center; margin: 0 auto; border-radius: 5px; border: solid 1px gray; line-height: 30px;"></b><br><br>
The Trial account grant you full access to all arbitrages, all bookies, best payment suppliers, our unique Surebet calculator, and much more. <b>DON'T MISS THIS OPPORTUNITY!</b><br>
<br>
<span style="color: red;">Important</span>: NO credit card or any other payment methods is required for trial activation<br>
<br>
 In case you mistyped your email address fill in the correct email address in the box below<br>and click on the "resend" button
<div class="card-block">
    <div class="row">
    <?php

    $form = ActiveForm::begin([
        'id' => 'form-email-error',
    ]);
    echo $form->field($model, 'email')->textInput(['placeholder' => 'E-mail', 'style' => 'max-width: 300px; margin: 0 auto;'])->label(false);
    echo $form->field($model, 'userid')->hiddenInput(['value' => $id])->label(false);
    echo Html::submitButton('Resend', ['class' => 'btn btn-success', 'name' => 'signup-button']);
    ActiveForm::end();
    ?>
    </div>
</div>
        </div>
    </div>
</section>

