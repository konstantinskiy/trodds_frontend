<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\SignupForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\authclient\widgets\AuthChoice;
use frontend\widgets\BookmakersWidget;

$this->title = 'Sign Up';
$this->params['breadcrumbs'][] = $this->title;

$this->params['page_class'] = '_login';
?>
<main>
    <div class="social_sing_up">
      <div class="social_sing_list">
        <a class="social_sing_list_facebook" href="/auth?authclient=facebook"></a>
        <a class="social_sing_list_twitter" href="/auth?authclient=twitter"></a>
        <a class="social_sing_list_google" href="/auth?authclient=google"></a>
      </div>
      <p class="social_sing_up_or">Or</p>
    </div>
    <div class="main_sing_up">
      <?php $form = ActiveForm::begin([
            'id' => 'form-signup',
            'errorCssClass' => 'has-danger',
            'fieldConfig' => [
                /*'hintOptions' => [
                    'class' => 'form-control-feedback'
                ],
                'errorOptions' => [
                    'class' => 'form-control-feedback'
                ]*/
            ],
            'options' => [
                'class' => 'form_login'
            ],
        ]); ?>
        <p class="form_contact_name">Name</p>
        <?= $form->field($model, 'first_name')->textInput(['placeholder' => 'Name'])->label(false); ?>
        <p class="form_contact_email">Email<span id="true"></span><span id="false"></span></p>
        <?= $form->field($model, 'email')->textInput()->input('email', ['placeholder' => 'email@user.com'])->label(false); ?>
        <label class="form_contact_password required" for="username">Password</label>
        <?= $form->field($model, 'password')->textInput()->input('password', ['placeholder' => 'Password', 'rel' => 'to-replace', 'class' => 'form-control'])->label(false); ?>
        <i>By clicking on "Sign Up" below, you are agreeing to Trodds <a href="/terms">Terms of Use</a> and <a href="/privacy">Privacy Policy.</a></i>
        <input type="submit" name="" value="Sign Up">
      <?php ActiveForm::end(); ?>
    </div>
    <?=BookmakersWidget::widget()?>
</main>

<?/*= AuthChoice::widget([
    'baseAuthUrl' => ['site/auth'],
    'popupMode' => false,
])*/?>
<?//= $this->render('_confirmModal'); ?>