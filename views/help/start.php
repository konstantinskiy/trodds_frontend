<?php

use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */

$this->title = 'Getting started';
$this->params['breadcrumbs'][] = $this->title;
?>

<main>
<div class="container clearfix">
  <div class="main_menu clearfix width_container">
    <div class="container_list">
      <?= $this->render('_menu') ?>
    </div>
  </div>
  <div class="main_info width_container">
    <div class="info_logo">
      <!DOCTYPE svg PUBLIC "-//W3C//DTD SVG 1.1//EN" "http://www.w3.org/Graphics/SVG/1.1/DTD/svg11.dtd">
      <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 60 60" enable-background="new 0 0 60 60" xml:space="preserve">
              <polyline fill="#FF0013" points="16.8,14.8 24.4,21.6 27.1,24 37.9,33.7 58.9,14.8 "/>
              <path fill="#222222" d="M58.9,13.7H16.8c-0.6,0-1.1,0.5-1.1,1.1v30.5c0,0.6,0.5,1.1,1.1,1.1h32.1h0.8h9.3c0.6,0,1.1-0.5,1.1-1.1
                V14.7C60,14.2,59.5,13.7,58.9,13.7z M56.2,15.8L37.9,32.2l-10.1-9c0,0,0,0,0,0l-8.2-7.4H56.2z M17.9,17.1l9,8l0,0l5.4,4.9L17.9,42.9
                V17.1z M49.7,44.2H49H19.6l14.3-12.8l3.3,3c0.2,0.2,0.4,0.3,0.7,0.3s0.5-0.1,0.7-0.3l3.3-3l14.3,12.8H49.7z M57.9,42.9L43.5,30
                l14.4-12.9V42.9z"/>
              <path fill="#222222" d="M3.9,19.5H12c0.6,0,1.1-0.5,1.1-1.1s-0.5-1.1-1.1-1.1H3.9c-0.6,0-1.1,0.5-1.1,1.1S3.3,19.5,3.9,19.5z"/>
              <path fill="#222222" d="M12.7,32.1H5.3c-0.6,0-1.1,0.5-1.1,1.1c0,0.6,0.5,1.1,1.1,1.1h7.5c0.6,0,1.1-0.5,1.1-1.1
                C13.8,32.6,13.3,32.1,12.7,32.1z"/>
              <path fill="#222222" d="M1.1,27.9h8.9c0.6,0,1.1-0.5,1.1-1.1s-0.5-1.1-1.1-1.1H1.1c-0.6,0-1.1,0.5-1.1,1.1S0.5,27.9,1.1,27.9z"/>
              <path fill="#222222" d="M9.9,40.5H9.1C8.5,40.5,8,41,8,41.6c0,0.6,0.5,1.1,1.1,1.1h0.9c0.6,0,1.1-0.5,1.1-1.1
                C11,41,10.5,40.5,9.9,40.5z"/>
              <path fill="#222222" d="M5.6,40.5H1.1C0.5,40.5,0,41,0,41.6c0,0.6,0.5,1.1,1.1,1.1h4.5c0.6,0,1.1-0.5,1.1-1.1
                C6.6,41,6.2,40.5,5.6,40.5z"/>
              </svg>
    </div>
    <div class="info_title">
      <h2>1. Subscribe to TRodds Premium Edition</h2>
    </div>
    <div class="info_text">
      <p>
        А method that turns sports betting into a way of investing your money with regular profits of over 10% per month. It is important to note that this is not gambling or a dodgy “betting” system that only works until you run out of luck or that won’t even
        work at all. Luck has nothing to do with this. After you have placed your bets, you will win every single time!
      </p>
    </div>
    <div class="info_logo info_logo_big_pictures">
      <!DOCTYPE svg PUBLIC "-//W3C//DTD SVG 1.1//EN" "http://www.w3.org/Graphics/SVG/1.1/DTD/svg11.dtd">
      <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 80 60" enable-background="new 0 0 80 60" xml:space="preserve">
              <g>
                <path fill="#FF0000" d="M44.3,6.3c0,2.5-2,4.5-4.5,4.5c-2.5,0-4.5-2-4.5-4.5c0-2.5,2-4.5,4.5-4.5C42.2,1.8,44.3,3.8,44.3,6.3
                  L44.3,6.3z M44.3,6.3"/>
                <path fill="#FF0000" d="M65,41.9c0,2.5-2,4.5-4.5,4.5c-2.5,0-4.5-2-4.5-4.5c0-2.5,2-4.5,4.5-4.5C63,37.4,65,39.4,65,41.9L65,41.9z
                   M65,41.9"/>
                <g>
                  <defs>
                    <rect id="SVGID_1_" x="13.4" y="0.6" width="52.7" height="59"/>
                  </defs>
                  <clipPath id="SVGID_2_">
                    <use xlink:href="#SVGID_1_"  overflow="visible"/>
                  </clipPath>
                  <path clip-path="url(#SVGID_2_)" fill="#FF0000" d="M23.6,41.9c0,2.5-2,4.5-4.5,4.5c-2.5,0-4.5-2-4.5-4.5c0-2.5,2-4.5,4.5-4.5
                    C21.5,37.4,23.6,39.4,23.6,41.9L23.6,41.9z M23.6,41.9"/>
                  <path clip-path="url(#SVGID_2_)" fill="#222222" d="M66.1,18.3c0-3.1-2.5-5.6-5.6-5.6c-1.7,0-3.2,0.7-4.2,1.9L45.1,8.1
                    c0.2-0.6,0.3-1.2,0.3-1.8c0-3.1-2.5-5.6-5.6-5.6c-3.1,0-5.6,2.5-5.6,5.6c0,0.6,0.1,1.2,0.3,1.8l-11.1,6.5c-1-1.2-2.5-1.9-4.2-1.9
                    c-3.1,0-5.6,2.5-5.6,5.6c0,2.7,1.9,4.9,4.4,5.5l0,12.6c-2.5,0.6-4.4,2.8-4.4,5.5c0,3.1,2.5,5.6,5.6,5.6c1.7,0,3.1-0.7,4.2-1.9
                    l11.2,6.5c-0.2,0.6-0.3,1.1-0.3,1.7c0,3.1,2.5,5.6,5.6,5.6c3.1,0,5.6-2.5,5.6-5.6c0-0.6-0.1-1.2-0.3-1.7l11.2-6.5
                    c1,1.1,2.5,1.9,4.2,1.9c3.1,0,5.6-2.5,5.6-5.6c0-2.7-2-5-4.5-5.5V23.8C64.1,23.3,66.1,21,66.1,18.3L66.1,18.3z M60.4,14.8
                    c1.9,0,3.5,1.5,3.5,3.5c0,1.9-1.5,3.5-3.5,3.5c-1.9,0-3.5-1.5-3.5-3.5C57,16.3,58.5,14.8,60.4,14.8L60.4,14.8z M39.7,2.8
                    c1.9,0,3.5,1.5,3.5,3.5c0,0.6-0.1,1.1-0.4,1.6c0,0,0,0-0.1,0.1l0,0.1c-0.6,1-1.7,1.7-3,1.7c-1.9,0-3.5-1.6-3.5-3.5
                    C36.3,4.4,37.8,2.8,39.7,2.8L39.7,2.8z M15.6,18.3c0-1.9,1.5-3.5,3.5-3.5c1.9,0,3.5,1.5,3.5,3.5c0,1.9-1.6,3.5-3.5,3.5
                    C17.1,21.7,15.6,20.2,15.6,18.3L15.6,18.3z M19,45.4c-1.9,0-3.5-1.6-3.5-3.5c0-1.9,1.5-3.5,3.5-3.5c1.9,0,3.5,1.5,3.5,3.5
                    C22.5,43.8,20.9,45.4,19,45.4L19,45.4z M39.7,57.3c-1.9,0-3.5-1.5-3.5-3.5c0-1.9,1.6-3.5,3.5-3.5c1.9,0,3.5,1.6,3.5,3.5
                    C43.2,55.8,41.6,57.3,39.7,57.3L39.7,57.3z M44,50.2c-1-1.2-2.6-2-4.3-2c-1.7,0-3.3,0.8-4.3,2l-11.1-6.4c0.2-0.6,0.3-1.2,0.3-1.9
                    c0-2.8-2-5.1-4.6-5.6l0-12.6c2.6-0.5,4.7-2.8,4.7-5.6c0-0.6-0.1-1.2-0.3-1.8L35.5,10c1,1.2,2.6,1.9,4.3,1.9c1.7,0,3.2-0.7,4.2-1.9
                    l11.1,6.5c-0.2,0.6-0.3,1.2-0.3,1.8c0,2.7,2,5,4.5,5.5v12.6c-2.6,0.5-4.5,2.8-4.5,5.5c0,0.7,0.1,1.3,0.3,1.9L44,50.2z M63.9,41.9
                    c0,1.9-1.5,3.5-3.5,3.5c-1.9,0-3.5-1.6-3.5-3.5c0-1.9,1.6-3.5,3.5-3.5C62.4,38.4,63.9,40,63.9,41.9L63.9,41.9z M63.9,41.9"/>
                </g>
                <path fill="#222222" d="M45,34.4v-3.9c0.7-0.6,1-1.5,1-2.5v-5.2c0-3.2-2.6-5.7-5.7-5.7h-1.1c-3.2,0-5.7,2.6-5.7,5.7V28
                  c0,0.9,0.4,1.8,1,2.5v3.9c-1.2,0.6-4.3,2.3-7.1,4.6c-0.5,0.4-0.5,1.1-0.1,1.5c0.4,0.5,1.1,0.5,1.5,0.1c3.1-2.6,6.5-4.3,7.1-4.6
                  c0.5-0.2,0.7-0.7,0.7-1.2v-5c0-0.4-0.2-0.7-0.5-0.9c-0.3-0.2-0.6-0.6-0.6-1v-5.2c0-2,1.6-3.6,3.6-3.6h1.1c2,0,3.6,1.6,3.6,3.6V28
                  c0,0.4-0.2,0.8-0.6,1c-0.3,0.2-0.5,0.5-0.5,0.9v5c0,0.5,0.3,1,0.7,1.2c0.6,0.3,4,2,7.1,4.6c0.2,0.2,0.4,0.3,0.7,0.3
                  c0.3,0,0.6-0.1,0.8-0.4c0.4-0.5,0.3-1.2-0.1-1.5C49.2,36.7,46.2,35.1,45,34.4L45,34.4z M45,34.4"/>
              </g>
              </svg>
    </div>
    <div class="info_title">
      <h2>2. Choose the right  bookmakers for you</h2>
    </div>
    <div class="info_text info_text_choose">
      <p>
        TRodds has carefully selected the safest and most reliable bookmakers in the market for you. All that’s left tfor you to do is to open an account with them. So, BEFORE you place your first SureBet, you have to choose the right bookmakers for you. Here,
        you will provide thema with your personal details. When the registration is completed, you will receive a username, a password and/or your personal account number. It’s very important to write these down and it has to be in a place where you,
        and only you, have access to it.
      </p>
      <p>Note: You will withdraw your profits using your personal details, so it's very important to provide all the chosen bookmakers your actual personal details.</p>
      <p>Note: Some bookmakers will offer you a promotional bonus as a new member. Remember to check the conditions before receiving the bonus.</p>
    </div>
    <div class="info_logo info_logo_big_pictures">
      <!DOCTYPE svg PUBLIC "-//W3C//DTD SVG 1.1//EN" "http://www.w3.org/Graphics/SVG/1.1/DTD/svg11.dtd">
      <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 80 60" enable-background="new 0 0 80 60" xml:space="preserve">
              <g>
                <g>
                    <g>
                        <g>
                            <defs>
                                <rect id="SVGID_1_" x="10.6" y="2.7" width="58.9" height="54.7"/>
                            </defs>
                            <clipPath id="SVGID_2_">
                                <use xlink:href="#SVGID_1_"  overflow="visible"/>
                            </clipPath>
                            <path clip-path="url(#SVGID_2_)" fill="#FF0000" d="M40.8,25.8c0,5.7-4.6,10.2-10.2,10.2s-10.2-4.6-10.2-10.2
                                c0-5.7,4.6-10.2,10.2-10.2C36.2,15.5,40.8,20.1,40.8,25.8L40.8,25.8z"/>
                        </g>
                    </g>
                    <g>
                        <g>
                            <defs>
                                <rect id="SVGID_3_" x="10.6" y="2.7" width="58.9" height="54.7"/>
                            </defs>
                            <clipPath id="SVGID_4_">
                                <use xlink:href="#SVGID_3_"  overflow="visible"/>
                            </clipPath>
                            <path clip-path="url(#SVGID_4_)" fill="#222222" d="M69.4,45.8C69.4,45.8,69.4,45.7,69.4,45.8c0-0.1,0-0.2,0-0.2
                                c0,0,0-0.1,0-0.1c0,0,0,0,0-0.1s0,0,0-0.1c0,0,0,0,0-0.1s0,0,0-0.1c0,0,0,0-0.1-0.1L69,45c0,0,0,0-0.1,0c0,0,0,0-0.1,0
                                l-13.3-6.6c0,0-0.1,0-0.1,0c-0.2-0.1-5.7-2-10.1,0l-9.8,3.6c0,0,0,0-0.1,0c-1.4,0.6-2.1,2.1-1.8,3.6c0.3,1.5,1.5,2.6,3,2.7h0
                                c0.1,0,0.5,0,5-0.3c1.9-0.1,4-0.3,4.2-0.3c0.6,0,1-0.5,1.1-1c0-0.6-0.4-1.1-1.1-1.1c0,0,0,0-4.2,0.3c-2.1,0.1-4.5,0.3-4.8,0.3
                                c-0.8,0-0.9-0.7-1-0.9c-0.1-0.4,0-0.9,0.6-1.2l9.8-3.6c0,0,0,0,0.1,0c3.4-1.6,8-0.2,8.5,0l5.9,2.9l-3.3,7.7
                                c-1.6-1-3.6-1.2-5.4-0.7l-13.2,4c-1.6,0.5-3.3,0.3-4.8-0.5L13.3,42.4c-0.8-0.4-0.5-1.1-0.5-1.2c0.1-0.2,0.4-0.8,1.2-0.6
                                l16.1,5.4c0.6,0.2,1.2-0.1,1.4-0.7c0.2-0.6-0.1-1.2-0.7-1.4l-16.1-5.4c0,0,0,0,0,0c-1.6-0.4-3.2,0.3-3.8,1.9
                                c-0.6,1.5,0,3.2,1.4,4L33,55.6c1.9,1.1,4.3,1.3,6.4,0.6l13.2-4c1.2-0.3,2.5-0.2,3.6,0.5L63,57c0,0,0,0,0.1,0c0,0,0,0,0,0l0,0h0
                                c0,0,0,0,0.1,0h0c0,0,0,0,0.1,0h0c0,0,0.1,0,0.1,0s0.1,0,0.1,0h0c0,0,0,0,0.1,0h0c0,0,0,0,0.1,0c0,0,0,0,0,0c0,0,0,0,0.1,0l0,0
                                c0,0,0,0,0.1,0l0,0l0,0l0,0c0,0,0,0,0-0.1l0,0l0,0v0c0,0,0,0,0-0.1l4.8-10.2c0,0,0,0,0-0.1s0-0.1,0-0.1c0,0,0-0.1,0-0.1v-0.1
                                C69.4,45.9,69.4,45.9,69.4,45.8L69.4,45.8z M66.9,46.4l-3.8,8.1l-3.9-2.4l3.3-7.8L66.9,46.4z"/>
                        </g>
                    </g>
                </g>
                <path fill="#222222" d="M29.6,26.4h1.9c0.8,0,1.4,0.6,1.4,1.4c0,0.8-0.6,1.4-1.4,1.4h-3.4c-0.4,0-0.7,0.3-0.7,0.7
                    c0,0.4,0.3,0.7,0.7,0.7h1.8v1.3c0,0.4,0.3,0.7,0.7,0.7s0.7-0.3,0.7-0.7v-1.3h0.3c1.5,0,2.8-1.2,2.8-2.8c0-1.5-1.2-2.8-2.8-2.8h-1.9
                    c-0.8,0-1.4-0.6-1.4-1.4c0-0.8,0.6-1.4,1.4-1.4H33c0.4,0,0.7-0.3,0.7-0.7c0-0.4-0.3-0.7-0.7-0.7h-1.7v-1.3c0-0.4-0.3-0.7-0.7-0.7
                    c-0.4,0-0.7,0.3-0.7,0.7v1.3h-0.3c-1.5,0-2.8,1.2-2.8,2.8C26.8,25.2,28.1,26.4,29.6,26.4L29.6,26.4z"/>
                <path fill="#222222" d="M30.6,36.7c6,0,10.9-4.9,10.9-10.9s-4.9-10.9-10.9-10.9s-10.9,4.9-10.9,10.9C19.6,31.8,24.5,36.7,30.6,36.7
                    L30.6,36.7z M30.6,16.2c5.3,0,9.6,4.3,9.6,9.6c0,5.3-4.3,9.6-9.6,9.6c-5.3,0-9.6-4.3-9.6-9.6C21,20.5,25.3,16.2,30.6,16.2
                    L30.6,16.2z"/>
              </g>
              </svg>
    </div>
    <div class="info_title">
      <h2>3. Deposit funds at your bookmakers betting accounts</h2>
    </div>
    <div class="info_text info_text_deposit">
      <p>
        The best SureBet situations stay for no more than a few minutes, so you have to already be set up with all your bookmaker accounts. For that, you need to deposit funds into your betting accounts.
      </p>
      <p>The most common deposit options are credit card, bank transfer, and bank checks. Also, for your convenience in the Standard and Premium editions, we have added some e-wallet options like Skrill, Neteller, and Payoneer. Most bookmakers accept
        them and they are a much safer, easier, and more cost-effective option to handle your deposits and claim back your profits. We advise you to follow the links and open an account before making a deposit with your selected bookmakers.
      </p>
      <p>Note: The fees and the time it takes for the money to arrive to your betting account vary from method to method. Some brokers will carry part of the fees, but you will have to double check that with the specific bookmaker and your bank and/or
        credit card issuer.</p>
    </div>
    <div class="info_logo info_logo_big_pictures">
      <!DOCTYPE svg PUBLIC "-//W3C//DTD SVG 1.1//EN" "http://www.w3.org/Graphics/SVG/1.1/DTD/svg11.dtd">
      <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 80 60" enable-background="new 0 0 80 60" xml:space="preserve">
              <g>
                <path fill="#FF0000" d="M49,50.2l-7.4,7.4l0,0l-7.4-7.4c-1.8-1.8-1.8-4.7,0-6.5c1.8-1.8,4.7-1.8,6.5,0l0.9,0.9l0.9-0.9
                  c1.8-1.8,4.7-1.8,6.5,0C50.8,45.5,50.8,48.4,49,50.2L49,50.2z"/>
                <path fill="#222222" d="M33.2,23.7v7.7c-1.8,0.9-7.4,3.9-12.6,8.2c-1.2,1-2,2.5-2,4.2v6.2c0,0.6,0.5,1.1,1.1,1.1s1.1-0.5,1.1-1.1
                  v-6.2c0-1,0.4-1.9,1.2-2.5c5.5-4.5,11.4-7.5,12.6-8.1c0.5-0.2,0.8-0.7,0.8-1.3v-8.7c0-0.3-0.2-0.7-0.5-0.9
                  c-0.8-0.5-1.3-1.5-1.3-2.5v-9c0-3.9,3.2-7,7-7h2c3.9,0,7,3.2,7,7v9c0,1-0.5,2-1.3,2.5c-0.3,0.2-0.5,0.5-0.5,0.9v8.7
                  c0,0.5,0.3,1.1,0.8,1.3c1.1,0.5,7.1,3.6,12.6,8.1c0.7,0.6,1.2,1.5,1.2,2.5v6.2c0,0.6,0.5,1.1,1.1,1.1c0.6,0,1.1-0.5,1.1-1.1v-6.2
                  c0-1.6-0.7-3.2-2-4.2c-5.2-4.3-10.9-7.3-12.6-8.2v-7.7c1.1-1,1.8-2.4,1.8-3.9v-9c0-5-4.1-9.2-9.2-9.2h-2c-5,0-9.2,4.1-9.2,9.2v8.9
                  C31.5,21.2,32.1,22.7,33.2,23.7L33.2,23.7z"/>
                <path fill="#222222" d="M45.7,41.2c-1.5,0-3,0.6-4,1.7L41.6,43l-0.2-0.2c-1.1-1.1-2.5-1.7-4-1.7c-1.5,0-3,0.6-4,1.7
                  c-1.1,1.1-1.7,2.5-1.7,4c0,1.5,0.6,3,1.7,4l7.4,7.4l0,0c0.2,0.2,0.5,0.3,0.7,0.3c0.3,0,0.5-0.1,0.7-0.3l7.4-7.4
                  c1.1-1.1,1.7-2.5,1.7-4s-0.6-3-1.7-4C48.7,41.8,47.2,41.2,45.7,41.2L45.7,41.2z M48.2,49.4l-6.7,6.7l-6.7-6.7c-0.7-0.7-1-1.6-1-2.5
                  c0-1,0.4-1.8,1-2.5c0.7-0.7,1.5-1,2.5-1s1.8,0.4,2.5,1l0.9,0.9c0.2,0.2,0.5,0.3,0.7,0.3s0.5-0.1,0.7-0.3l0.9-0.9
                  c0.7-0.7,1.6-1,2.5-1s1.8,0.4,2.5,1c0.7,0.7,1,1.6,1,2.5C49.3,47.8,48.9,48.7,48.2,49.4L48.2,49.4z"/>
              </g>
              </svg>
    </div>
    <div class="info_title">
      <h2>4. Placing your first SureBet</h2>
    </div>
    <div class="info_text_calculation">
      <p>
        If you followed the previous steps, you are now ready to place your first SureBet. Notice that in some bets, some bookmakers only allow that you bet in doubles or triples. It’s important to check that before placing the actual bet. You will come across
        many different formats of odds presentation, but we always use the European format (decimal). We suggest you do the same with the bookmakers you pick for your SureBets.
      </p>
    </div>
    <div class="info_logo clearfix">
      <img src="/img/Receive profits.svg" alt="Receive profits.svg">
    </div>
    <div class="info_title">
      <h2>5. Receive profits from your betting accounts</h2>
    </div>
    <div class="info_text">
      <p>
        Just as with depositing funds into your betting accounts, you have pretty much the same options for withdrawing your funds: credit card, bank transfer, and bank check. Note: Same as for depositing, you can also withdraw funds to your featured e-wallets
        like Neteller, Skrill, and Payoneer.
      </p>
    </div>
    <div class="info_logo">
      <img src="/img/Repeat.svg" alt="Repeat.svg">
    </div>
    <div class="info_title">
      <h2>6. Repeat</h2>
    </div>
    <div class="info_text_strategy">
      <p>
        At this stage, you must have already gotten a nice profit after completing your first successful SureBet. All that’s left for you to do is to REPEAT the process again and again and again.
      </p>
    </div>
  </div>
</div>
</main>