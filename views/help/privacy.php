<?php

use yii\helpers\Html;
use yii\helpers\Url;
use common\widgets\Alert;

/* @var $this yii\web\View */

$this->title = 'Terms & Privacy';
$this->params['breadcrumbs'][] = $this->title;
?>
  <main>
    <div class="container clearfix">
      <div class="main_menu clearfix width_container">
        <div class="container_list">
          <ul class="main_menu_list">
			<?= $this->render('_menu_help') ?>
          </ul>
        </div>
      </div>
      <div class="help_terms">
        <div class="terms_info">
          <div class="terms_info_title">
            <h2>Privacy Policy</h2>
            <p class="policy_number_indent">
              <span></span> <i>The following privacy policy shows the manner</i> in which Top Rated Odds (the company) collects, mantaines, uses, and discloses information collected from the company clients (the user) of www.trodds.com website (the site).
              This privacy policy applies to this site and any other products and services offered by the company.
            </p>
            <p class="policy_number_indent">
              <span>1.</span> <i>Collection of personal and non personal information</i>
            </p>
            <p>
              <span>A.</span>&nbsp; We may collect personal identification from users in various ways, including but not limited to, when user visit the website, registers, fill out a forms, and in connection with other services, activities, features
              or resources we offer to our users in the company website. We collect such information only if users voluntarily submits this type of information, users may be asked for for identification purposes some of their personal information (name,
              email), user can refuse to supply such personal information if chooses, expect it may them from engaging certain parts of the website, products, services, resources and features. WE DON’T COLLECT ANY PAYMENT INFORMATION FROM OUR USERS.
            </p>
            <p>
              <span>B.</span>&nbsp; The company may collect non personal information about users whenever the user visits our website, information such a browser name, operational system, type of computer and technical information of the user means connection
              to our webiste and other similar information
            </p>
            <p class="policy_number_indent">
              <span>2.</span> <i>Web browser cookies & usage of information</i>
            </p>
            <p>
              <span>A.</span>&nbsp; The company uses cookies, to understand and save user preferences for future visits and compile overall data about the site including site traffic and interaction to improve the site experience and tools for future
              visits. The company may contract with any 3rd party to help the website in better understanding of our visitors, these 3rd party services are not permitted to collect user personal data on the company's behalf expect to conduct and help
              the company by improving our business.
            </p>
            <p>
              <span>B.</span>&nbsp; The email address the user provides will be used only to improve user personalized experience, to respond to user queries in order to improve the company website customer support, to send user after opt-in daily/weekly/monthly
              updates regarding the company products, services and/or features in order to improve the quality of such services, features and/or products.
            </p>
            <p>
              <span class="policy_number_indent">3.</span> <i>Protection of information of information</i> The website adopts necessary techniques for collected information, storage, processing to and any security measures in order to protect the data
              from unwanted and unauthorized access, disclosure and/or access to the user personal information, email, password, and any other personal information stored on the company website.
            </p>
            <p class="policy_number_indent">
              <span>4.</span> <i>Sharing the user personal information</i> the company isn't and won’t sell, trade or rent user's personal information to any other 3rd party company, we may share overall demographic information not associated with an
              individual user regarding visitors and/or users with the company business partners, affiliates and/or other 3rd party partners.
            </p>
            <p class="policy_number_indent">
              <span>5.</span> <i>3rd party website.</i> User may find advertisements or other content that linked to 3rd party website on the company's website, includes but not limited to partners, suppliers, advertisers, sponsors and other 3rd parties.
              The company is not controls the content that is on these 3rd party websites in any way or manner and are not responsible for the 3rd party websites practises employed there. These site and services may have their own privacy policies, browsing
              and interaction on any other 3rd party websites, including websites that are linked to the company's site is subjected to the 3rd party website's own terms and policies
            </p>
            <p class="policy_number_indent">
              <span>6.</span> <i>Changes to this privacy policy & your acceptance to them</i> the company may update this privacy policy from time to time, and will publish this changes on the website, it the user solely responsibility to check for any
              updates regarding the privacy policy. By using the company's website the user signifies the agreement of the privacy policy and terms of service, if you don't agree to any part of the privacy policy please dont user the company website or
              any additional products, services and features. You continued usage of the website even following after some modification in the privacy policy and terms of user, is deemed as the user acceptance for them. If you have any questions regarding
              the usage of the website, terminology of the privacy policy and/or terms of use please don't hesitate to contact us at support@trodds.com
            </p>
          </div>
        </div>
      </div>
    </div>
  </main>