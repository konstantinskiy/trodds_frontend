<?php

use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */

$this->title = 'Help';
$this->params['breadcrumbs'][] = $this->title;
?>


<main>
<div class="container clearfix">
  <div class="main_menu clearfix width_container">
    <div class="container_list">
      <?= $this->render('_menu') ?>
    </div>
  </div>
  <div class="main_info width_container">
    <div class="info_logo">
      
      <!-- Generator: Adobe Illustrator 17.1.0, SVG Export Plug-In . SVG Version: 6.00 Build 0)  -->
      <!DOCTYPE svg PUBLIC "-//W3C//DTD SVG 1.1//EN" "http://www.w3.org/Graphics/SVG/1.1/DTD/svg11.dtd">
      <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 80 60" enable-background="new 0 0 80 60" xml:space="preserve">
              <g>
                <path fill="#222222" d="M59.4,19.4l7.4-7.4c0.4-0.4,0.6-1.1,0.3-1.6c-0.2-0.6-0.8-0.9-1.4-0.9l-5.1-0.1l-0.1-5.1
                  c0-0.6-0.4-1.1-0.9-1.4c-0.6-0.2-1.2-0.1-1.6,0.3l-7.4,7.4c-0.2,0.2-0.3,0.4-0.4,0.7l-0.1,0.5C46.3,9.5,42,8.2,37.3,8.2
                  c-13.6,0-24.6,11-24.6,24.6s11,24.6,24.6,24.6s24.6-11,24.6-24.6c0-4.7-1.3-9.1-3.6-12.8l0.5-0.1C59,19.7,59.2,19.6,59.4,19.4
                  L59.4,19.4z M58.8,32.7c0,11.9-9.7,21.6-21.6,21.6c-11.9,0-21.6-9.7-21.6-21.6c0-11.9,9.7-21.6,21.6-21.6c4.5,0,8.6,1.4,12.1,3.7
                  l-1.2,4.9l-1,1c-2.7-2.2-6.1-3.5-9.8-3.5c-8.5,0-15.5,7-15.5,15.5s6.9,15.5,15.5,15.5c8.5,0,15.5-6.9,15.5-15.5
                  c0-3.7-1.3-7.2-3.5-9.8l1-1l4.9-1.2C57.5,24.1,58.8,28.3,58.8,32.7L58.8,32.7z M35.6,34.4c0.3,0.3,0.7,0.4,1.1,0.4
                  c0.4,0,0.8-0.1,1.1-0.4l3.5-3.5c0.3,0.6,0.5,1.3,0.4,1.9c-0.4,2.4-2,4.4-4.4,4.4c-2.4,0-4.4-2-4.4-4.4c0-2.4,2-4.4,4.4-4.4
                  c0.7,0,1.3,0.1,1.9,0.4l-3.5,3.5C35.1,32.8,35.1,33.8,35.6,34.4L35.6,34.4z M41.3,26.5c-1.2-0.8-2.6-1.2-4.1-1.2
                  c-4.1,0-7.4,3.3-7.4,7.4s3.3,7.4,7.4,7.4s7.4-3.3,7.4-7.4c0-1.5-0.5-2.9-1.2-4.1l3.6-3.6c1.7,2.1,2.7,4.8,2.7,7.7
                  c0,6.9-5.6,12.5-12.5,12.5s-12.5-5.6-12.5-12.5s5.6-12.5,12.5-12.5c2.9,0,5.6,1,7.7,2.7L41.3,26.5z M57.6,16.9l-6.1,1.5l1.5-6.1
                  l4.6-4.6l0,3c0,0.8,0.7,1.5,1.5,1.5l3,0L57.6,16.9z"/>
              </g>
              <path fill="#FF000D" d="M57.6,17l-6.1,1.5l1.5-6.1l4.7-4.6l0,3c0,0.8,0.7,1.5,1.5,1.5l3,0L57.6,17z"/>
              <path fill="#FF000D" d="M35.6,34.4c0.3,0.3,0.7,0.4,1.1,0.4c0.4,0,0.8-0.1,1.1-0.4l3.5-3.5c0.3,0.6,0.5,1.3,0.4,1.9
                c-0.4,2.4-2,4.4-4.4,4.4c-2.4,0-4.4-2-4.4-4.4c0-2.4,2-4.4,4.4-4.4c0.7,0,1.3,0.1,1.9,0.4l-3.5,3.5C35,32.8,35,33.8,35.6,34.4
                L35.6,34.4z"/>
              </svg>

    </div>
    <div class="info_title">
      <h2>Who we are</h2>
    </div>
    <div class="info_text">
      <p>
        TRodds is proud to present possibly the best sports arbitrage platform available. We have carefully picked the most reliable, safest, and most profitable bookmakers currently in the market. Our premium edition gives you access to all possible surebets,
        to all bookmakers without any limitations, and to perhaps the most important tool, the surebet calculator. With zero delay in processing data, you cannot ask for more.
      </p>
    </div>
    <div class="info_logo">
      
      <!-- Generator: Adobe Illustrator 17.1.0, SVG Export Plug-In . SVG Version: 6.00 Build 0)  -->
      <!DOCTYPE svg PUBLIC "-//W3C//DTD SVG 1.1//EN" "http://www.w3.org/Graphics/SVG/1.1/DTD/svg11.dtd">
      <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 80 60" enable-background="new 0 0 80 60" xml:space="preserve">
              <path fill="#252525" d="M26.7,42.8c-1.8,0-3.3-1.5-3.3-3.3V28.9c0-1.8,1.5-3.3,3.3-3.3c1.8,0,3.3,1.5,3.3,3.3v10.6
                C30,41.3,28.5,42.8,26.7,42.8z"/>
              <path fill="#252525" d="M26.7,41.9c-1.3,0-2.4-1.1-2.4-2.4V28.9c0-1.3,1.1-2.4,2.4-2.4c1.3,0,2.4,1.1,2.4,2.4v10.6
                C29.1,40.8,28,41.9,26.7,41.9z"/>
              <path fill="#252525" d="M37.2,42.8c-1.8,0-3.3-1.5-3.3-3.3V24.7c0-1.8,1.5-3.3,3.3-3.3c1.8,0,3.3,1.5,3.3,3.3v14.8
                C40.6,41.3,39.1,42.8,37.2,42.8z"/>
              <path fill="#252525" d="M37.2,41.9c-1.3,0-2.4-1.1-2.4-2.4V24.7c0-1.3,1.1-2.4,2.4-2.4c1.3,0,2.4,1.1,2.4,2.4v14.8
                C39.7,40.8,38.6,41.9,37.2,41.9z"/>
              <path fill="#252525" d="M47.6,43.8c-2.3,0-4.2-1.9-4.2-4.2v-19c0-2.3,1.9-4.2,4.2-4.2s4.2,1.9,4.2,4.2v19
                C51.8,41.9,49.9,43.8,47.6,43.8z"/>
              <path fill="#EF2B2C" d="M47.7,40.3c-1.2,0-1.8-0.6-1.8-2V22.1c0-1.8,0.7-2.2,1.8-2.2c1.2,0,1.7,0.5,1.7,2.2v16.3
                C49.3,40.5,47.6,40.3,47.7,40.3z"/>
              <path fill="#252525" d="M65.1,34.6l7.2-8.6c0.2-0.3,0-0.7-0.3-0.7h-5.1l0-0.1C64.4,11.8,53.6,1.3,40.2,0C21.3-1.8,5.5,14,7.3,32.9
                C8.4,44.2,16,54.1,26.6,58.1c14.3,5.4,29.5-0.8,36.7-13.1c0.3-0.4,0.3-1,0.2-1.5c-0.1-0.5-0.5-0.9-0.9-1.2c-0.3-0.2-0.6-0.3-1-0.3
                c-0.7,0-1.3,0.4-1.7,1c-4.7,8.1-13.4,13.1-22.7,13.1c-15.4,0-27.8-13.4-26-29.2c1.1-10,8.1-18.7,17.7-21.9
                c15.2-5.1,30.5,4.6,33.8,19l0.2,1.1h-5.4c-0.4,0-0.6,0.4-0.3,0.7l7.2,8.6C64.6,34.8,65,34.8,65.1,34.6z"/>
              </svg>
    </div>
    <div class="info_title">
      <h2>Odds updates</h2>
    </div>
    <div class="info_text">
      <p>
        Since most of the Surebets last no more than several minutes, access to fast odds update is one of the key factors in being a successful surebettor. With our premium edition, you will benefit from our zero delay odds update feature to ensure your success.
      </p>
    </div>
    <div class="info_logo">
      
      <!-- Generator: Adobe Illustrator 17.1.0, SVG Export Plug-In . SVG Version: 6.00 Build 0)  -->
      <!DOCTYPE svg PUBLIC "-//W3C//DTD SVG 1.1//EN" "http://www.w3.org/Graphics/SVG/1.1/DTD/svg11.dtd">
      <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 80 60" enable-background="new 0 0 80 60" xml:space="preserve">
              <path fill="#222222" d="M40,0.5C23.7,0.5,10.5,13.7,10.5,30S23.7,59.5,40,59.5S69.5,46.3,69.5,30S56.3,0.5,40,0.5z M41.6,3.8
                c13.2,0.8,23.7,11.4,24.6,24.6h-6c-0.8-9.8-8.7-17.7-18.5-18.5V3.8L41.6,3.8z M57,30c0,9.3-7.6,17-17,17s-17-7.6-17-17s7.6-17,17-17
                S57,20.7,57,30z M38.4,3.8v6c-9.8,0.8-17.7,8.7-18.5,18.5h-6C14.6,15.2,25.2,4.6,38.4,3.8z M40,56.3c-13.9,0-25.4-10.9-26.2-24.6h6
                C20.7,42,29.4,50.2,40,50.2c5,0,9.6-1.8,13.1-4.8l4.3,4.3C52.7,53.8,46.7,56.3,40,56.3z M59.7,47.4l-4.3-4.3
                c2.7-3.1,4.4-7.1,4.8-11.4h6C65.8,37.7,63.4,43.1,59.7,47.4z"/>
              <path fill="#222222" d="M34.9,39.9c0.2,0.1,0.5,0.2,0.7,0.2c0.5,0,1.1-0.3,1.3-0.8l8.9-17.1c0.4-0.7,0.1-1.7-0.6-2
                c-0.7-0.4-1.7-0.1-2,0.6l-8.9,17.1C33.8,38.6,34.1,39.5,34.9,39.9z"/>
              <path fill="#222222" d="M34.3,30.2c1.2,0,2.4-0.6,3.3-1.7c0.8-1,1.2-2.2,1.2-3.6c0-1.3-0.4-2.6-1.2-3.6c-0.8-1.1-2-1.7-3.3-1.7
                s-2.4,0.6-3.3,1.7c-0.8,1-1.2,2.2-1.2,3.6s0.4,2.6,1.2,3.6C31.9,29.6,33.1,30.2,34.3,30.2z M34.3,22.8c0.7,0,1.4,0.9,1.4,2.2
                c0,1.3-0.8,2.2-1.4,2.2c-0.7,0-1.4-0.9-1.4-2.2C32.9,23.7,33.6,22.8,34.3,22.8z"/>
              <path fill="#222222" d="M42.4,31.4c-0.8,1-1.2,2.2-1.2,3.6c0,1.3,0.4,2.6,1.2,3.6c0.8,1.1,2,1.7,3.3,1.7c1.2,0,2.4-0.6,3.3-1.7
                c0.8-1,1.2-2.2,1.2-3.6c0-1.3-0.4-2.6-1.2-3.6c-0.8-1.1-2-1.7-3.3-1.7C44.4,29.8,43.2,30.4,42.4,31.4z M47.1,35
                c0,1.3-0.8,2.2-1.4,2.2s-1.4-0.9-1.4-2.2c0-1.3,0.8-2.2,1.4-2.2C46.4,32.8,47.1,33.7,47.1,35z"/>
              <path fill="#FF0000" d="M41.6,3.8c13.2,0.8,23.7,11.4,24.6,24.6h-6c-0.8-9.8-8.7-17.7-18.5-18.5V3.8L41.6,3.8z"/>
              <path fill="#FF0000" d="M40,56.3c-13.9,0-25.4-10.9-26.2-24.6h6C20.7,42,29.4,50.2,40,50.2c5,0,9.6-1.8,13.1-4.8l4.3,4.3
                C52.7,53.8,46.7,56.3,40,56.3z"/>
              </svg>
    </div>
    <div class="info_title">
      <h2>Surebets</h2>
    </div>
    <div class="info_text">
      <p>
        With our premium edition, you will gain access to all possible surebets with profits that can reach up to 10%-15% per surebet. You will also gain access to our surebet calculator which will show you which stakes you need to place on each outcome.
      </p>
    </div>
    <div class="info_logo">
      
      <!-- Generator: Adobe Illustrator 17.1.0, SVG Export Plug-In . SVG Version: 6.00 Build 0)  -->
      <!DOCTYPE svg PUBLIC "-//W3C//DTD SVG 1.1//EN" "http://www.w3.org/Graphics/SVG/1.1/DTD/svg11.dtd">
      <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 80 60" enable-background="new 0 0 80 60" xml:space="preserve">
              <path fill="#222222" d="M46.5,38l-3.3-2.2c-0.1-0.4-0.3-0.7-0.5-1.1l0.8-3.9c0.1-0.5-0.1-1.1-0.4-1.5l-2.5-2.5
                c-0.4-0.4-1-0.6-1.5-0.4l-3.9,0.8c-0.4-0.2-0.7-0.3-1.1-0.5l-2.2-3.3c-0.3-0.5-0.8-0.7-1.4-0.7h-3.5c-0.5,0-1.1,0.3-1.4,0.7
                l-2.2,3.3c-0.4,0.1-0.7,0.3-1.1,0.5l-3.9-0.8c-0.5-0.1-1.1,0.1-1.5,0.4l-2.5,2.5c-0.4,0.4-0.6,1-0.4,1.5l0.8,3.9
                c-0.2,0.4-0.3,0.7-0.5,1.1L11.1,38c-0.5,0.3-0.8,0.8-0.8,1.4v3.5c0,0.5,0.3,1.1,0.8,1.4l3.3,2.2c0.1,0.4,0.3,0.7,0.5,1.1l-0.8,3.9
                c-0.1,0.5,0.1,1.1,0.4,1.5l2.5,2.5c0.4,0.4,1,0.6,1.5,0.4l3.9-0.8c0.4,0.2,0.7,0.3,1.1,0.5l2.2,3.3c0.3,0.5,0.8,0.8,1.4,0.8h3.5
                c0.5,0,1.1-0.3,1.4-0.8l2.2-3.3c0.4-0.1,0.7-0.3,1.1-0.5l3.9,0.8c0.5,0.1,1.1-0.1,1.5-0.4l2.5-2.5c0.4-0.4,0.6-1,0.4-1.5l-0.8-3.9
                c0.2-0.4,0.3-0.7,0.5-1.1l3.3-2.2c0.5-0.3,0.7-0.8,0.7-1.4v-3.5C47.3,38.8,47,38.3,46.5,38z M44,42l-3,2c-0.3,0.2-0.5,0.5-0.7,0.9
                c-0.2,0.6-0.4,1.1-0.7,1.7c-0.2,0.3-0.2,0.7-0.1,1.1l0.7,3.6l-1.2,1.2l-3.6-0.7c-0.4-0.1-0.7,0-1.1,0.1c-0.6,0.3-1.2,0.5-1.7,0.7
                c-0.4,0.1-0.7,0.4-0.9,0.7l-2,3H28l-2-3c-0.2-0.3-0.5-0.5-0.9-0.7c-0.6-0.2-1.2-0.4-1.7-0.7c-0.3-0.2-0.7-0.2-1.1-0.1l-3.6,0.7
                l-1.2-1.2l0.8-3.6c0.1-0.4,0-0.8-0.1-1.1c-0.3-0.6-0.5-1.2-0.7-1.7c-0.1-0.4-0.4-0.7-0.7-0.9l-3-2v-1.7l3-2c0.3-0.2,0.5-0.5,0.7-0.9
                c0.2-0.6,0.4-1.1,0.7-1.7c0.2-0.3,0.2-0.7,0.1-1.1L17.5,31l1.2-1.2l3.6,0.7c0.4,0.1,0.8,0,1.1-0.1c0.6-0.3,1.1-0.5,1.7-0.7
                c0.4-0.1,0.7-0.4,0.9-0.7l2-3h1.7l2,3c0.2,0.3,0.5,0.5,0.9,0.7c0.6,0.2,1.2,0.4,1.7,0.7c0.3,0.2,0.7,0.2,1.1,0.1l3.6-0.7l1.2,1.2
                l-0.7,3.6c-0.1,0.4,0,0.7,0.1,1.1c0.3,0.6,0.5,1.2,0.7,1.7c0.1,0.4,0.4,0.7,0.7,0.9l3.1,2L44,42L44,42z"/>
              <path fill="#222222" d="M28.8,33.7c-4.1,0-7.4,3.3-7.4,7.4s3.3,7.4,7.4,7.4s7.4-3.3,7.4-7.4S32.9,33.7,28.8,33.7z M28.8,45.2
                c-2.2,0-4.1-1.8-4.1-4.1c0-2.2,1.8-4.1,4.1-4.1s4.1,1.8,4.1,4.1C32.9,43.3,31.1,45.2,28.8,45.2z"/>
              <path fill="#222222" d="M68.9,12.3l-2.5-1.6c-0.1-0.2-0.2-0.4-0.3-0.7l0.6-2.9c0.1-0.5-0.1-1.1-0.4-1.5l-1.9-1.9
                c-0.4-0.4-1-0.6-1.5-0.4l-2.9,0.6c-0.2-0.1-0.4-0.2-0.7-0.3L57.7,1c-0.3-0.5-0.8-0.8-1.4-0.8h-2.7c-0.5,0-1.1,0.3-1.4,0.8l-1.6,2.5
                c-0.2,0.1-0.4,0.2-0.7,0.3L47,3.2c-0.5-0.1-1.1,0.1-1.5,0.4l-1.9,1.9c-0.4,0.4-0.6,1-0.4,1.5l0.6,2.9c-0.1,0.2-0.2,0.4-0.3,0.7
                L41,12.2c-0.5,0.3-0.7,0.8-0.7,1.4v2.7c0,0.5,0.3,1.1,0.7,1.4l2.5,1.6c0.1,0.2,0.2,0.4,0.3,0.7l-0.6,2.9c-0.1,0.5,0.1,1.1,0.4,1.5
                l1.9,1.9c0.4,0.4,1,0.6,1.5,0.4l2.9-0.6c0.2,0.1,0.4,0.2,0.7,0.3l1.6,2.5c0.3,0.5,0.8,0.7,1.4,0.7h2.7c0.5,0,1.1-0.3,1.4-0.7
                l1.6-2.5c0.2-0.1,0.4-0.2,0.7-0.3l2.9,0.6c0.5,0.1,1.1-0.1,1.5-0.4l1.9-1.9c0.4-0.4,0.6-1,0.4-1.5L66.1,20c0.1-0.2,0.2-0.4,0.3-0.7
                l2.5-1.6c0.5-0.3,0.8-0.8,0.8-1.4v-2.7C69.6,13.1,69.3,12.6,68.9,12.3z M66.3,15.5l-2.2,1.4c-0.3,0.2-0.5,0.5-0.7,0.9
                c-0.1,0.4-0.3,0.8-0.5,1.3c-0.2,0.3-0.2,0.7-0.1,1.1l0.5,2.6l-0.7,0.7l-2.6-0.5c-0.4-0.1-0.7,0-1.1,0.1c-0.4,0.2-0.9,0.4-1.3,0.5
                c-0.4,0.1-0.7,0.4-0.9,0.7l-1.5,2.2h-0.9L53,24.2c-0.2-0.3-0.5-0.5-0.9-0.7c-0.4-0.1-0.8-0.3-1.3-0.5c-0.3-0.2-0.7-0.2-1.1-0.1
                l-2.6,0.5l-0.7-0.7l0.5-2.6c0.1-0.4,0-0.7-0.1-1.1c-0.2-0.4-0.4-0.9-0.5-1.3c-0.1-0.4-0.4-0.7-0.7-0.9l-2.2-1.5v-0.9l2.2-1.4
                c0.3-0.2,0.5-0.5,0.7-0.9c0.1-0.4,0.3-0.8,0.5-1.3c0.2-0.3,0.2-0.7,0.1-1.1l-0.5-2.6l0.7-0.7l2.6,0.5c0.4,0.1,0.7,0,1.1-0.1
                c0.4-0.2,0.9-0.4,1.3-0.5c0.4-0.1,0.7-0.4,0.9-0.7l1.4-2.2h0.9l1.4,2.2c0.2,0.3,0.5,0.5,0.9,0.7C58.1,6.6,58.5,6.8,59,7
                c0.3,0.2,0.7,0.2,1.1,0.1l2.6-0.5l0.7,0.7l-0.5,2.6c-0.1,0.4,0,0.8,0.2,1.1c0.2,0.4,0.4,0.8,0.5,1.3c0.1,0.4,0.4,0.7,0.7,0.9
                l2.2,1.4L66.3,15.5L66.3,15.5L66.3,15.5z"/>
              <g>
                <path fill="#FF0000" d="M64.1,13.1c-0.3-0.2-0.5-0.5-0.7-0.9c-0.1-0.4-0.3-0.9-0.5-1.3c-0.2-0.3-0.2-0.7-0.2-1.1l0.5-2.6l-0.7-0.7
                  L60,7.1c-0.4,0.1-0.8,0-1.1-0.1c-0.4-0.2-0.9-0.4-1.3-0.5c-0.3-0.1-0.6-0.4-0.9-0.7l-1.4-2.2h-0.9L53,5.8c-0.2,0.3-0.5,0.5-0.9,0.7
                  c-0.4,0.1-0.8,0.3-1.3,0.5c-0.3,0.2-0.7,0.2-1.1,0.1l-2.6-0.5l-0.7,0.7l0.5,2.6c0.1,0.4,0,0.7-0.1,1.1c-0.2,0.4-0.4,0.9-0.5,1.3
                  c-0.1,0.3-0.4,0.6-0.7,0.9l-2.2,1.4v0.9l2.2,1.5c0.3,0.2,0.5,0.5,0.7,0.9c0.1,0.4,0.3,0.8,0.5,1.3c0.2,0.3,0.2,0.7,0.1,1.1
                  l-0.5,2.6l0.7,0.7l2.6-0.5c0.4-0.1,0.7,0,1.1,0.1c0.4,0.2,0.9,0.4,1.3,0.5c0.3,0.1,0.6,0.4,0.9,0.7l1.4,2.2h0.9l1.5-2.2
                  c0.2-0.3,0.5-0.5,0.9-0.7c0.4-0.1,0.8-0.3,1.3-0.5c0.3-0.2,0.7-0.2,1.1-0.1l2.6,0.5l0.7-0.7l-0.5-2.6c-0.1-0.4,0-0.7,0.1-1.1
                  c0.2-0.4,0.4-0.9,0.5-1.3c0.1-0.3,0.4-0.6,0.7-0.9l2.2-1.4v0h0v-0.9L64.1,13.1z M54.9,21.1c-3.4,0-6.1-2.7-6.1-6.1
                  c0-3.4,2.7-6.1,6.1-6.1c3.4,0,6.1,2.7,6.1,6.1C61,18.4,58.3,21.1,54.9,21.1z"/>
                <path fill="#222222" d="M54.9,8.9c-3.4,0-6.1,2.7-6.1,6.1c0,3.4,2.7,6.1,6.1,6.1c3.4,0,6.1-2.7,6.1-6.1C61,11.6,58.3,8.9,54.9,8.9z
                   M54.9,17.8c-1.5,0-2.8-1.3-2.8-2.8c0-1.5,1.3-2.8,2.8-2.8c1.5,0,2.8,1.3,2.8,2.8C57.7,16.5,56.5,17.8,54.9,17.8z"/>
              </g>
              <path fill="#FF0000" d="M28.8,45.2c-2.2,0-4.1-1.8-4.1-4.1c0-2.2,1.8-4.1,4.1-4.1s4.1,1.8,4.1,4.1C32.9,43.3,31.1,45.2,28.8,45.2z"
                />
              </svg>
    </div>
    <div class="info_title">
      <h2>Surebet calculation</h2>
    </div>
    <div class="info_text_calculation">
      <p>
        A surebet, or an arbitrage bet, is a series of guaranteed bets where you are sure to win. Regardless of how the final outcome turns out to be, you cannot lose.
      </p>
      <p>Let’s take a closer look at the following example:</p>
      <p>A basketball event between LA Lakers VS Boston Celtics where 2 different bookmakers offer the following odds:</p>
      <p>Bookmaker1 LA Lakers 1.55 Boston Celtics 2.8</p>
      <p>Bookmaker2 LA Lakers 1.9 Boston Celtics 1.95</p>
      <p>In our example, the bookmakers gave such different odds for the same outcome that you can make a guaranteed 13.2% profit by betting on both outcomes. That’s what we call a Surebet in sports arbitrage. In other words, for every $100 bet, you
        will get a guaranteed return of $113.2. That’s a 13.2% profit. Not bad for a couple of minutes of work!
      </p>
    </div>
    <div class="info_logo">
      
      <!-- Generator: Adobe Illustrator 17.1.0, SVG Export Plug-In . SVG Version: 6.00 Build 0)  -->
      <!DOCTYPE svg PUBLIC "-//W3C//DTD SVG 1.1//EN" "http://www.w3.org/Graphics/SVG/1.1/DTD/svg11.dtd">
      <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 80 60" enable-background="new 0 0 80 60" xml:space="preserve">
              <g>
                <path fill="#222222" d="M56.4,0.9H22.4c-2,0-3.5,1.6-3.5,3.5v51.3c0,2,1.6,3.5,3.5,3.5h34.1c2,0,3.5-1.6,3.5-3.5V4.4
                  C60,2.5,58.4,0.9,56.4,0.9L56.4,0.9z M57,55.8c0,0.3-0.2,0.5-0.5,0.5H22.4c-0.3,0-0.5-0.2-0.5-0.5V4.4c0-0.3,0.2-0.5,0.5-0.5h34.1
                  c0.3,0,0.5,0.2,0.5,0.5V55.8z M57,55.8"/>
                <path fill="#222222" d="M52.4,7h-26c-0.8,0-1.5,0.7-1.5,1.5v11.1c0,0.8,0.7,1.5,1.5,1.5h26c0.8,0,1.5-0.7,1.5-1.5V8.5
                  C53.9,7.7,53.2,7,52.4,7L52.4,7z M50.9,18.1H28v-8h22.9V18.1z M50.9,18.1"/>
                <path fill="#222222" d="M32.3,24.4h-5.9c-0.8,0-1.5,0.7-1.5,1.5v5.9c0,0.8,0.7,1.5,1.5,1.5h5.9c0.8,0,1.5-0.7,1.5-1.5v-5.9
                  C33.9,25.1,33.2,24.4,32.3,24.4L32.3,24.4z M30.8,30.3H28v-2.9h2.9V30.3z M30.8,30.3"/>
                <path fill="#222222" d="M42.4,24.4h-5.9c-0.8,0-1.5,0.7-1.5,1.5v5.9c0,0.8,0.7,1.5,1.5,1.5h5.9c0.8,0,1.5-0.7,1.5-1.5v-5.9
                  C43.9,25.1,43.2,24.4,42.4,24.4L42.4,24.4z M40.9,30.3H38v-2.9h2.9V30.3z M40.9,30.3"/>
                <path fill="#222222" d="M52.4,24.4h-5.9c-0.8,0-1.5,0.7-1.5,1.5v5.9c0,0.8,0.7,1.5,1.5,1.5h5.9c0.8,0,1.5-0.7,1.5-1.5v-5.9
                  C53.9,25.1,53.2,24.4,52.4,24.4L52.4,24.4z M50.9,30.3H48v-2.9h2.9V30.3z M50.9,30.3"/>
                <path fill="#222222" d="M32.3,34.4h-5.9c-0.8,0-1.5,0.7-1.5,1.5v5.9c0,0.8,0.7,1.5,1.5,1.5h5.9c0.8,0,1.5-0.7,1.5-1.5v-5.9
                  C33.9,35,33.2,34.4,32.3,34.4L32.3,34.4z M30.8,40.3H28v-2.9h2.9V40.3z M30.8,40.3"/>
                <path fill="#222222" d="M42.4,34.4h-5.9c-0.8,0-1.5,0.7-1.5,1.5v5.9c0,0.8,0.7,1.5,1.5,1.5h5.9c0.8,0,1.5-0.7,1.5-1.5v-5.9
                  C43.9,35,43.2,34.4,42.4,34.4L42.4,34.4z M40.9,40.3H38v-2.9h2.9V40.3z M40.9,40.3"/>
                <path fill="#222222" d="M32.3,44.3h-5.9c-0.8,0-1.5,0.7-1.5,1.5v5.9c0,0.8,0.7,1.5,1.5,1.5h5.9c0.8,0,1.5-0.7,1.5-1.5v-5.9
                  C33.9,45,33.2,44.3,32.3,44.3L32.3,44.3z M30.8,50.2H28v-2.9h2.9V50.2z M30.8,50.2"/>
                <path fill="#222222" d="M42.4,44.3h-5.9c-0.8,0-1.5,0.7-1.5,1.5v5.9c0,0.8,0.7,1.5,1.5,1.5h5.9c0.8,0,1.5-0.7,1.5-1.5v-5.9
                  C43.9,45,43.2,44.3,42.4,44.3L42.4,44.3z M40.9,50.2H38v-2.9h2.9V50.2z M40.9,50.2"/>
                <path fill="#222222" d="M52.4,34.4h-5.9c-0.8,0-1.5,0.7-1.5,1.5v15.9c0,0.8,0.7,1.5,1.5,1.5h5.9c0.8,0,1.5-0.7,1.5-1.5V35.9
                  C53.9,35,53.2,34.4,52.4,34.4L52.4,34.4z M50.9,50.2H48V37.4h2.9V50.2z M50.9,50.2"/>
              </g>
              <rect x="28" y="10" fill="#FF0000" width="22.9" height="8"/>
              <rect x="48" y="37.4" fill="#FF0000" width="2.9" height="12.8"/>
              </svg>

    </div>
    <div class="info_title">
      <h2>Surebet calculator</h2>
    </div>
    <div class="info_text">
      <p>
        Probably without the surebet calculator, not a single surebet can be successful. It's definitely the key feature for being a successful surebettor. The calculator has a unique algorithm that shows you exactly how to divide your initial stake for each
        possible outcome of the surebet and gives you the same profit for each one. It doesn’t matter how the event will be decided.
      </p>
    </div>
    <div class="info_logo">
      
      <!-- Generator: Adobe Illustrator 17.1.0, SVG Export Plug-In . SVG Version: 6.00 Build 0)  -->
      <!DOCTYPE svg PUBLIC "-//W3C//DTD SVG 1.1//EN" "http://www.w3.org/Graphics/SVG/1.1/DTD/svg11.dtd">
      <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 80 60" enable-background="new 0 0 80 60" xml:space="preserve">
              <path fill="#222222" d="M66.6,20.8H53.3c-0.9,0-1.6,0.7-1.6,1.6V58c0,0.9,0.7,1.6,1.6,1.6h13.3c0.9,0,1.6-0.7,1.6-1.6V22.5
                C68.3,21.6,67.5,20.8,66.6,20.8z M65,56.3H55v-3.8h10L65,56.3L65,56.3z M65,49.2H55v-3.8h10L65,49.2L65,49.2z M65,42.1H55v-3.8h10
                L65,42.1L65,42.1z M65,35H55v-3.8h10L65,35L65,35z M65,27.9H55v-3.8h10L65,27.9L65,27.9z"/>
              <path fill="#222222" d="M46.2,35H32.9c-0.9,0-1.6,0.7-1.6,1.6V58c0,0.9,0.7,1.6,1.6,1.6h13.3c0.9,0,1.6-0.7,1.6-1.6V36.7
                C47.9,35.8,47.1,35,46.2,35z M44.6,56.3h-10v-3.8h10V56.3z M44.6,49.2h-10v-3.8h10V49.2z M44.6,42.1h-10v-3.8h10V42.1z"/>
              <path fill="#222222" d="M25.8,49.2H12.5c-0.9,0-1.6,0.7-1.6,1.6V58c0,0.9,0.7,1.6,1.6,1.6h13.3c0.9,0,1.6-0.7,1.6-1.6v-7.1
                C27.5,50,26.7,49.2,25.8,49.2z M24.2,56.3h-10v-3.8h10V56.3z"/>
              <path fill="#FF0000" d="M67.4,0.8h-8.8c-0.9,0-1.6,0.7-1.6,1.6s0.7,1.6,1.6,1.6h4.8l-7.3,7.3L51.8,7c-0.6-0.6-1.7-0.6-2.3,0
                L35.8,20.7l-4-4c-0.6-0.6-1.7-0.6-2.3,0L10.7,35.5c-0.6,0.6-0.6,1.7,0,2.3c0.6,0.6,1.9,0.4,2.3,0l17.7-17.7l4,4
                c0.3,0.3,0.7,0.5,1.2,0.5c0.4,0,0.8-0.2,1.2-0.5l13.7-13.7l4.3,4.3c0.6,0.6,1.7,0.6,2.3,0l8.4-8.4v4.8c0,0.9,0.7,1.6,1.6,1.6
                s1.6-0.7,1.6-1.6V2.4C69,1.5,68.3,0.8,67.4,0.8z"/>
              </svg>
    </div>
    <div class="info_title">
      <h2>Surebet strategy</h2>
    </div>
    <div class="info_text_strategy">
      <p>
        There is a difference between an average Surebettor who looks at sports arbitrage as a second income and a pro Surebettor who makes enough money without any day job. Both of them probably spend no more than a few hours. This difference is called the right
        Surebet strategy so pay close attention!
      </p>
      <p>Surebets come and go faster than a blink of an eye, so speed matters. You have to act very fast. During your practice, you will probably develop your own set of skills, but here are some key points. Use this set of simple advices and they will
        make you the best and separate you from the rest.</p>
      <ul>
        <p>•&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Keep everything in one place—account, payment, and bookmaker information.</p>
        <p>•&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Before starting your Surebetting session, make sure that most bookmakers are already open on your browser.</p>
        <p>•&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Before placing a bet, make sure that you have enough balance with that specific bookmaker. You don’t want to waste valuable time making a deposit during a Surebet situation or, even
          worse, finding out that you are already over the daily limit set by your credit card provider.</p>
        <p>•&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;You need to be sharp during a session so don’t push yourself! Mistakes start to happen more often when you are tired.</p>
      </ul>
    </div>
    <div class="info_logo">
      
      <!-- Generator: Adobe Illustrator 17.1.0, SVG Export Plug-In . SVG Version: 6.00 Build 0)  -->
      <!DOCTYPE svg PUBLIC "-//W3C//DTD SVG 1.1//EN" "http://www.w3.org/Graphics/SVG/1.1/DTD/svg11.dtd">
      <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 80 60" enable-background="new 0 0 80 60" xml:space="preserve">
              <path fill="#222222" d="M75.7,56.8l-5.2-12.2V2.4c0-1.1-0.9-2-2-2H11.4c-1.1,0-2,0.9-2,2v42.1L4.3,56.8c-0.2,0.6-0.2,1.3,0.2,1.9
                c0.4,0.5,1,0.9,1.7,0.9h67.8c0.7,0,1.3-0.3,1.7-0.9S76,57.4,75.7,56.8z M13.4,29.4h4c1.1,0,2-0.9,2-2c0-1.1-0.9-2-2-2h-4v-3.5h7.8
                c1.1,0,2-0.9,2-2c0-1.1-0.9-2-2-2h-7.8V4.4h53.1v38.6H13.4V29.4z M9.1,55.6l3.6-8.6h54.5l3.6,8.6H9.1z"/>
              <path fill="#FF0000" d="M44.7,49.3h-9.3c-1.1,0-2,0.9-2,2c0,1.1,0.9,2,2,2h9.3c1.1,0,2-0.9,2-2S45.8,49.3,44.7,49.3z"/>
              <path fill="#222222" d="M53.7,18.8l-16.9-5.6c-0.6-0.2-1.3-0.1-1.8,0.4c-0.5,0.5-0.6,1.2-0.4,1.8l5.6,17c0.2,0.7,0.9,1.2,1.7,1.2h0
                c0.8,0,1.4-0.5,1.7-1.2l1.8-5.8l5.2,5.2c0.6,0.6,1.9,0.6,2.5,0c0.7-0.7,0.7-1.8,0-2.5L47.8,24l5.8-1.8c0.7-0.2,1.2-0.9,1.2-1.7
                C54.9,19.8,54.4,19.1,53.7,18.8z M44,21.5c-0.6,0.2-1,0.6-1.2,1.2l-1.1,3.4L39,17.6l8.4,2.8L44,21.5z"/>
              <path fill="#FF0000" d="M44,21.5c-0.6,0.2-1,0.6-1.2,1.2L41.8,26L39,17.6l8.4,2.8L44,21.5z"/>
              </svg>
    </div>
    <div class="info_title">
      <h2>Order of placing bets</h2>
    </div>
    <div class="info_text">
      <p>
        When placing a Surebet, you need to place all your bets one after another. You can accomplish that by opening all the bookmakers’ pages, creating the betting slips, entering the exact stakes as shown in the Surebet calculator, and proceeding right before
        the confirmation bet pages before committing the first bet of the Surebet. Always confirm the lower stakes of the Surebet first.
      </p>
    </div>
    <div class="info_logo">
      
      <!-- Generator: Adobe Illustrator 17.1.0, SVG Export Plug-In . SVG Version: 6.00 Build 0)  -->
      <!DOCTYPE svg PUBLIC "-//W3C//DTD SVG 1.1//EN" "http://www.w3.org/Graphics/SVG/1.1/DTD/svg11.dtd">
      <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 80 60" enable-background="new 0 0 80 60" xml:space="preserve">
              <path fill="#222222" d="M67.8,0.6H28.2c-0.9,0-1.6,0.7-1.6,1.6v22c-0.4,1.4-0.7,3-0.7,4.5c0,3.7,1.3,7.1,3.4,9.7L27.8,40
                c-0.8-0.4-1.6-0.6-2.5-0.6c-1.5,0-2.8,0.6-3.9,1.6l-9.1,9.1c-1.1,1.1-1.7,2.6-1.6,4.1c0.1,1.5,0.8,3,2,4c0.9,0.8,2.2,1.2,3.4,1.2
                c1.5,0,3-0.6,4.1-1.7l6.4-6.4v6.5c0,0.9,0.7,1.6,1.6,1.6h28.9c0.4,0,0.8-0.2,1.2-0.5L69,48.3c0.3-0.3,0.5-0.7,0.5-1.2V2.3
                C69.5,1.4,68.7,0.6,67.8,0.6z M26.8,46.4l-9,9c-1,1-2.3,0.8-3.1,0.3c-1.4-1-1-2.5-0.2-3.3l9.1-9.1c0.8-0.8,2.3-0.9,3.1,0
                C27.8,44.2,27.7,45.6,26.8,46.4z M29.2,27.6h7.2c0.9,0,1.6-0.7,1.6-1.6s-0.7-1.6-1.6-1.6H30c1.8-4.5,6.2-7.8,11.3-7.8
                c6.7,0,12.2,5.5,12.2,12.2c0,6.7-5.5,12.2-12.2,12.2c-4.8,0-8.9-2.8-10.9-6.8h1.9c0.9,0,1.6-0.7,1.6-1.6c0-0.9-0.7-1.6-1.6-1.6h-2.9
                C29.2,30.2,29,28.4,29.2,27.6z M58.8,53.9v-5.1h5.1L58.8,53.9z M66.2,45.5C66.2,45.5,66.2,45.5,66.2,45.5l-9.1,0
                c-0.9,0-1.6,0.7-1.6,1.6v9c0,0,0,0,0,0.1H29.8v-8.3c0.6-0.9,1-2,1-3.1c0-0.9-0.2-1.8-0.6-2.5l1.5-1.5c2.7,2.1,6,3.4,9.7,3.4
                c3,0,5.7-0.8,8.1-2.3c0.1,0,12.3,0,12.3,0c0.9,0,1.6-0.7,1.6-1.6s-0.7-1.6-1.6-1.6h-8.5c1.7-2,2.8-4.4,3.3-7h5.2
                c0.9,0,1.6-0.7,1.6-1.6c0-0.9-0.7-1.6-1.6-1.6h-4.9c-0.1-2.5-0.7-4.9-1.9-7h6.8c0.9,0,1.6-0.7,1.6-1.6c0-0.9-0.7-1.6-1.6-1.6
                c0,0-9.2,0-9.2,0c-2.8-2.9-6.8-4.8-11.2-4.8c-4.6,0-8.7,2-11.5,5.2V3.9h36.4L66.2,45.5L66.2,45.5z"/>
              <path fill="#222222" d="M50.2,49H34.4c-0.9,0-1.6,0.7-1.6,1.6c0,0.9,0.7,1.6,1.6,1.6h15.8c0.9,0,1.6-0.7,1.6-1.6
                C51.8,49.8,51.1,49,50.2,49z"/>
              <path fill="#222222" d="M34.4,11.1h27.3c0.9,0,1.6-0.7,1.6-1.6c0-0.9-0.7-1.6-1.6-1.6H34.4c-0.9,0-1.6,0.7-1.6,1.6
                C32.8,10.3,33.5,11.1,34.4,11.1z"/>
              <path fill="#FF0000" d="M26.8,46.4l-9,9c-1,1-2.3,0.8-3.1,0.3c-1.4-1-1-2.5-0.2-3.3l9.1-9.1c0.8-0.8,2.3-0.9,3.1,0
                C27.7,44.2,27.7,45.6,26.8,46.4z"/>
              <polygon fill="#FF0000" points="58.8,53.9 58.8,48.8 63.9,48.8 "/>
              </svg>
    </div>
    <div class="info_title">
      <h2>Do your accounting</h2>
    </div>
    <div class="info_text">
      <p>
        At the end of the day, BIG profits is what defines you as a successful Surebettor. The only way to do this is to track your bets with extremely strict accounting. With limited accounts, it’s fairly easy. However, when you are in the Surebetting business,
        you need to keep tracking everything in your accounts, so write any betting movement you make.
      </p>
    </div>
  </div>
</div>
</main>