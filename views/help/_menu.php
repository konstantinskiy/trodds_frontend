<?php
// use yii\bootstrap\Nav;
use yii\widgets\Menu;
use yii\helpers\Url;

$menuItems = [];

$menuItems[] = ['label' => 'Getting started', 'url' => ['help/start']] ;
$menuItems[] = ['label' => 'About TRodds', 'url' => ['help/about']] ;
// $menuItems[] = ['label' => 'Payments', 'url' => ['help/payments']] ;
//$menuItems[] = ['label' => 'Terms, privacy Disclaimer', 'url' => ['help/terms']] ;
$menuItems[] = ['label' => 'Contact us', 'url' => ['help/contact']] ;
    
$checkUrl = 'help/'.Yii::$app->controller->action->id;

/*foreach($menuItems as &$menuItem) {
    if($menuItem['url'][0] == $checkUrl) {
        $menuItem['template'] = '<a class="main_menu_link active" href="{url}">{label}</a>';
    }
}*/

?>
<?php echo Menu::widget([
        'options' => [
            'class' => 'main_menu_list'
        ],
        'itemOptions' => [
            'class' => 'main_menu_item'
        ],
        'linkTemplate' => '<a class="main_menu_link" href="{url}">{label}</a>',
        'items' => $menuItems,
    ]);
?>