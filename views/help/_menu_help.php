<?php
// use yii\bootstrap\Nav;
use yii\widgets\Menu;
use yii\helpers\Url;

$menuItems = [];

$menuItems[] = ['label' => 'Terms of Use', 'url' => ['help/terms']] ;
$menuItems[] = ['label' => 'Privacy Policy', 'url' => ['help/privacy']] ;
    
$checkUrl = 'help/'.Yii::$app->controller->action->id;

?>
<?php echo Menu::widget([
        'options' => [
            'class' => 'main_menu_list'
        ],
        'itemOptions' => [
            'class' => 'main_menu_item'
        ],
        'linkTemplate' => '<a class="main_menu_link" href="{url}">{label}</a>',
        'items' => $menuItems,
    ]);
?>