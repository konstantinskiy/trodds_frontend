  <?php

use yii\helpers\Html;
use yii\helpers\Url;
use common\widgets\Alert;

/* @var $this yii\web\View */

$this->title = 'Terms & Privacy';
$this->params['breadcrumbs'][] = $this->title;
?>
  <main>
    <div class="container clearfix">
      <div class="main_menu clearfix width_container">
        <div class="container_list">
          <?= $this->render('_menu_help') ?>
        </div>
      </div>
      <div class="help_terms">
        <div class="terms_info">
          <div class="terms_info_title">
            <h2>1. General rules</h2>
            <p>
              <span>A.</span> The following terms of use concluding the use of an internet resource called Top Rated Odds (the provider) from one side, and a person that interested in receiving the service (the user) by registering and accessing trodds
              you agree to these terms of use, you should also see our privacy policy and the disclaimer.
            </p>
            <p>
              <span>B.</span> The user having agreed to this terms of use, has to follow these rules and confirms having reached 18 or being at the right legal age according to his country of residence.
            </p>
            <p>
              <span>C.</span> These terms of use describe conditions provided to the user by the provider and the obligations of the user following this rules.
            </p>
            <p>
              <span>D. </span> The use of the service provided by the provider is on a private basis only, the user obligated not to reveal any information received by the provider.
            </p>
            <p>
              <span>E. </span> Registering multiple accounts is not allowed, any kind of abuse will lead to permanent ban and termination of the service.
            </p>
            <p>
              <span>F. </span> Although the service is accessible worldwide, we make no representation that the materials on this site are appropriate or available in the user country of residence, accessing the provider service from locations that these
              type of services are illegal or prohibited is the user solely responsibility, those who chose to access the service from their location doing so on based on their initiative and responsible in compliance with their local law.
            </p>
            <h2>2. The service</h2>
            <p>
              <span>A.</span> The service automatically scans line from various bookmakers and presents the information after data analysis, this is not a financial service of any kind therefore we are not offering any managing packages by us or any other
              3rd party, the service is not a bookmaker or any other form of gambling service as well.
            </p>
            <p>
              <span>B.</span> The provider has the right to change the amount of information presented from time to time. Including lines, betting types, dates and bookmakers among others forms of information.
            </p>
            <p>
              <span>C.</span> The provider may change the number of bookmakers presented from time to time, and shall not hold responsibility for the reliability of any 3rd party data collected by the provider.
            </p>
            <p>
              <span>D. </span> The user utilizes the information provided by the provider at his own risk, no information shall be regarded as instruction or any other managing use by the provider for the user. Therefore the provider is not responsible
              for any consequences caused by the use of the service.
            </p>
            <p>
              <span>E. </span> The user must be fully aware of possible discrepancies between the information provided by the service and the real state of affairs, therefore the user cannot claim any possible losses caused by direct or indirect use of
              the information provided.
            </p>
            <h2>3. Obligations</h2>
            <p>
              <span>A.</span> The provider is not responsible for any kind of discrepancy caused by 3rd party providers, however the provider will make any kind of effort to secure the best quality data delivery to the user.
            </p>
            <p>
              <span>B.</span> The user is obligated to check before use the adequacy of the service.
            </p>
            <p>
              <span>C.</span> The provider will not consider any kind of claims for compensation as a result of possible losses, caused directly or indirectly by the service.
            </p>
            <p>
              <span>D. </span> The user utilizes the information provided by the provider at his own risk, no information shall be regarded as instruction or any other managing use by the provider for the user. Therefore the provider is not responsible
              for any consequences caused by the use of the service.
            </p>
            <p>
              <span>E. </span> In case the service is unavailable for some time, the provider has the right to prolong user’s subscription period.
            </p>
            <p>
              <span>F. </span> The user has no rights to use any automated means to receive the information from the provider.
            </p>
            <h2>4. Changes </h2>
            <p>
              <span>A.</span> The provider has the right to change this terms of use and the privacy policy.
            </p>
            <p>
              <span>B.</span> Using the terms of use and the privacy policy after alterations is considered as an agreement to the if modifications had been made.
            </p>
            <h2>5. Registration data, security and copyrights </h2>
            <p>
              <span>A.</span> In order to access the site provided services, you will require an account that will contain an username and password, you may obtain this information by subscribing to the service The subscription requests certain information
              and data, by registering you agree that all the information provided in the subscription process is true and aмccurate and the user will maintain and update if needed this information in order to keep it accurate and up-to- date.
            </p>
            <p>
              <span>B.</span> The information you obtain using the provider services, including your registration data, is subjected to the provider privacy policy, which is specifically incorporated by reference into this terms of service.
            </p>
            <p>
              <span>C.</span> This site may link the user to other 3rd party sites on the world wide web including reference to information, materials and / or services provided by other 3rd parties.these sites may contain materials and / or services
              that some people may find inappropriate or offensive. The user acknowledges that these other site or 3rd parties is not in the provider control, thus not responsible for the accuracy, copyright, decency, legality or any other aspect of the
              information in those 3rd party sites, the provider is also not responsible for any kind of errors in any reference to any 3rd party information, products or services, the inclusion of such link or reference does not imply endorsement, advertisement
              or association of any kind the provide with any other 3rd parties, or any warranty of some kind, direct or indirect.
            </p>
            <p>
              <span>D. </span> Intellectual property information is copyrighted trodds all rights reserved is solely to the provider. For the purpose of this terms of use content is defined as any information, data, textual property, communications, graphics,
              sound, softwares, videos, photos, music and an other materials and services the user may find while using the provider services.
            </p>
            <p>
              <span>E. </span> By accepting this terms of use, the user agrees that all content presented by the provider is subjected and protected by copyrights, trademarks, service marks or any other property rights and laws, and it’s the sole property
              of the provider.
            </p>
            <p>
              <span>F. </span> The user is solely responsible for maintaining in confidentiality of his account and subscription details including but not limited to account username and password and any statements and acts made on the user account through
              the use of username and password, therefore the user needs to ensure the safety of his subscription and account details, the provider personnel will never ask any user for his account password. The user has no right to transfer and/or share
              his personal account details with anyone and the provider reserves the right to terminate user account and / subscription such act will happen.
            </p>
            <p>
              <span>G. </span> The provider may make an email or any other messaging service, available to user as part of the provider service. Either directly or through a 3rd party provider, the provider may make an additional separate agreements stating
              the relationship between the user and the provider, except where noted or contradicts, includes these terms of use.
            </p>
            <p>
              <span>H. </span> The provider may impose automated monitoring techniques to protect our users from mass communication services and / or any other types of electronic systems that acts not according to our terms of use, however such monitoring
              services are not 100% perfect as a result the provider will not be responsible for any legitimate services that will be blocked or for any unsolicited communication that is not blocked.
            </p>
            <p>
              <span>I. </span> Typically mailboxes has limited storage capacity, if users exceeds the limit, the provider may imply automated devices that delete or block email messages that exceeds this limit, the provider will not be responsible for
              such blocked or deleted messages.
            </p>
            <h2>7. Entire agreement </h2>
            <p>
              <span>A.</span> These terms of use constitute the entire agreement between the provider and the user concerning the use of the provider direct and indirect services, this agreement will supersede any prior agreements and understanding between
              the parties. These terms of use may not be added, changed, modified in any form of way, any attempt doing so with any other document will be dismissed and voided. Unless otherwise was agreed in written form between the parties. Furthemore
              if there is any conflict between the site or associated, these terms of use shall prevail, unless otherwise stated.
            </p>
            <p>
              <span>B.</span> The user agrees that the provider have the right to terminate access for all or part of the service with or without prior notice in case including but not limited to breach or these terms of use, any fraudulent, abusive or
              illegal activity of any kind may be ground for termination of such kind and may be referred to the appropriate law enforcement authorities.
            </p>
            <p>
              <span>C.</span> Upon termination, regardless to the reasons, the provider reserves the right to immediately cease the user access to the service, as well as deactivate and / or delete the user account and any related information in the user
              account and / or deny such access to this information, furthermore the provider holds no liability that the user or any other 3rd party claims for damages arising as a result of the account termination out of any action taken by the provider
              in connection with account termination.
            </p>
            <p>
              <span>D. </span> The user may cancel his subscription at any given time, all payments made prior to the cancellation are not refundable, even on a relative basis. The provider premium features acquired by the user expire at the next billing
              cycle upon cancellation.
            </p>
            <p>
              <span>E. </span> Where the provider cannot or decides not to deliver it’s services or products, the provider liability is limited to refund the user for the specific product or service as a result of the provider fault to deliver the product
              and / or service to the user, the provider is not obligated to provide the user refunds for products or services as a result including but not limited to user account termination as a result of violation of any part of these terms of use.
            </p>
            <p>
              <span>F. </span> In case the service is not available to the user as a result software, hardware or network failure the user agrees that it’s not the provider fault and not entitled to any form of compensation, therefore the provider recommends
              to use most up-to-date software and hardware to ensure the perfect usage of the provider services and products.
            </p>
            <h2>8. Miscellaneous Notices</h2>
            <p>
              <span>A.</span> The user is prohibited to assign his rights and obligations to any 3rd party, any attempt to do so may lead to termination of the user subscription and / or account. The provider may assign his right and obligations according
              to these terms of use.
            </p>
            <p>
              <span>B.</span> All notices shall be made in written from to the following email address support@trodds.com, notices sent to the user may be added with user registration data.
            </p>
            <p>
              <span>C.</span> The user agrees not to sell, resell, duplicate and / or copy including but not limited to any private or commercial purposes part of the site, as well as whole, use of it and / or access to the service.
            </p>
            <p>
              <span>D. </span> If any part of this terms of use is seen as invalid or unenforceable, that part should be considered with the applicable law as nearly as possible, the original intentions of the parties, the remaining parts should be considered
              in the original manner with full intentions and will take full force and effect.
            </p>
            <p>
              <span>E. </span> Any failure by us to enforce any provision of this terms of use, terms of use related rights wont constitute a waiver of the rights to provision.
            </p>
          </div>
        </div>
      </div>
    </div>
  </main>