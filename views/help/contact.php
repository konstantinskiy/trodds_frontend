<?php

use yii\helpers\Html;
use yii\helpers\Url;

use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;

/* @var $this yii\web\View */

$this->title = 'Contact Us';
$this->params['breadcrumbs'][] = $this->title;
?>
  <main>

    <div class="main_menu clearfix width_container">
      <div class="container_list">
        <?= $this->render('_menu') ?>
      </div>
    </div>
    <div class="contact_main">
      <div class="contact_form_info row">
        <div class="contact_info col-10 col-md-4 col-sm-4 contact_info__help">
          <p>
            Feel free to send us your queries at any time and our support team will respond in the shortest time possible. Please fill the form or send us email to support@trodds.com.
          </p>
          <h2>Thank you for contacting us!</h2>
        </div>
        <div class="contact_form col-10 col-md-4 col-sm-4 help_contact_us">
          <p class="contact_form_title">Contact us</p>
            <?php $form = ActiveForm::begin([
                    'id' => 'contact-form',
                    'options' => [
                        'class' => 'form_contact'
                    ],
                    
                    'errorCssClass' => 'has-danger',
                    'fieldConfig' => [
                        'hintOptions' => [
                            'class' => 'hidden-xs-up form-control-feedback'
                        ],
                        'errorOptions' => [
                            'class' => 'hidden-xs-up form-control-feedback'
                        ]
                    ]
                ]); 
            ?>
            <p class="form_contact_name">Name</p>
            <?= $form->field($model, 'first_name')->textInput(['autofocus' => true, 'placeholder' => 'My name'])->label(false); ?>
            <p class="form_contact_email">Email<span id="true"></span><span id="false"></span></p>
            <?= $form->field($model, 'email')->textInput()->input('text', ['placeholder' => 'email@user.com', ''])->label(false); ?>
            <p class="form_contact_subject">Subject</p>
            <?= $form->field($model, 'subject')->textInput(['placeholder' => ''])->label(false); ?>
            <?= $form->field($model, 'verifyCode')->widget(Captcha::className(), [
                'options' => [
                    'placeholder' => 'Captcha code:',
                    'class' => 'form-control'
                ],
                'template' => '<p class="form_contact_capcha">Captcha code</p><div class="input-group">{input}<span class="input-group-addon" style="padding:0;">{image}</span></div>',
            ])->label(false) ?>
            <p class="form_contact_message">Your message</p>
            <p><?= $form->field($model, 'body', ['template' => '{input}{error}'])->textArea()?></p>
            <input type="submit" name="" value="Send">
          <?php ActiveForm::end(); ?>
        </div>
      </div>
    </div>
  </main>


