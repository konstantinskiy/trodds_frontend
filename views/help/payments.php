<?php

use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */

$this->title = 'Payments';
$this->params['breadcrumbs'][] = $this->title;
?>
<section class="section section-image section-faded" style="background-image:url(<?= Url::to('@web/img/banner2.jpg') ?>); background-position:center center;">
    <div class="container">
        <h2 class="text-xs-center"><?= $this->title ?></h2>
    </div>
</section>
<section class="section">
    <div class="container">
        <div id="tabs-container">
            <?= $this->render('_menu') ?>
            <div class="card tab-content" style="border-top:0;border-top-left-radius: 0;border-top-right-radius: 0;">
                <div id="tab-3" class="tab-pane card-block active">
                    <h3 class="mb-2">Payments</h3>
                    <p>Lorem ipsum dolor sit amet, consectetur tempor incididunt. Lorem ipsum dolor sit amet, consectetur
                        adipiscing elit, sed do eiusmod tempor incididunt.</p>
                    <p>Lorem ipsum dolor sit amet, consectetur tempor incididunt. Lorem ipsum dolor sit amet, consectetur
                        adipiscing elit, sed do eiusmod tempor incididunt.</p>
                    <div class="bookies mt-2">
                        <div class="row">
                            <div class="col-sm-6 col-lg-4">
                                <div class="card text-xs-center mb-2">
                                    <img class="card-img-top" src="/img/bookie-icon.png" alt="company">
                                    <div class="card-block">
                                        <h4 class="card-title">Company Name</h4>
                                        <p class="card-text">Lorem ipsum dolor sit amet, consectetur tempor incididunt. Lorem ipsum dolor
                                            sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt.</p>
                                        <a href="#" class="btn btn-success">Open Account</a>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6 col-lg-4">
                                <div class="card text-xs-center mb-2">
                                    <img class="card-img-top" src="/img/bookie-icon.png" alt="company">
                                    <div class="card-block">
                                        <h4 class="card-title">Company Name</h4>
                                        <p class="card-text">Lorem ipsum dolor sit amet, consectetur tempor incididunt. Lorem ipsum dolor
                                            sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt.</p>
                                        <a href="#" class="btn btn-success">Open Account</a>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6 col-lg-4">
                                <div class="card text-xs-center mb-2">
                                    <img class="card-img-top" src="/img/bookie-icon.png" alt="company">
                                    <div class="card-block">
                                        <h4 class="card-title">Company Name</h4>
                                        <p class="card-text">Lorem ipsum dolor sit amet, consectetur tempor incididunt. Lorem ipsum dolor
                                            sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt.</p>
                                        <a href="#" class="btn btn-success">Open Account</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>