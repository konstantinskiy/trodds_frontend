<?

use frontend\widgets\BookmakersWidget;
$this->title = 'Trodds';

if(!Yii::$app->user->isGuest){
  $this->params['page_class'] = '_main';
  $this->params['bg_class'] = '_home';
  $this->params['nav_class'] = '_home';
}else if(Yii::$app->user->isGuest){
  $this->params['page_class'] = '_main';
  $this->params['bg_class'] = '_main';
  $this->params['nav_class'] = '_main';
}

?>
  <main>
    
      <?if(Yii::$app->user->isGuest):?>
        <div class="section_presents">
            <div class="section_presents_container">
              <div class="ipad_container_main">
                <div class="home_user_table main_table_iphone" id="tableMain">
                </div>
              </div>
              <div class="mac_container_main">
                <div class="home_user_table main_table_mac" id="tableMain_iPhone">
                </div>
              </div>
            </div>
          </div>
          <div class="started_subs">
            <div class="started_subs_title">
              <p>Once you tried it you will never look back.</p>
              <h2>Become part of the success don’t waste a single moment!</h2>
              <?php echo $this->render('@frontend/views/site/_signupForm'); ?>
            </div>
          </div>
        <?=BookmakersWidget::widget()?>
          <div class="section_reasons">
            <div class="reasons_title">
              <h2>Top reasons why you should like TRodds</h2>
            </div>
            <div class="reasons_safe">
              <div class="safe_logo">
                
                <!-- Generator: Adobe Illustrator 17.1.0, SVG Export Plug-In . SVG Version: 6.00 Build 0)  -->
                <!DOCTYPE svg PUBLIC "-//W3C//DTD SVG 1.1//EN" "http://www.w3.org/Graphics/SVG/1.1/DTD/svg11.dtd">
                <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 100 100" enable-background="new 0 0 100 100" xml:space="preserve">
                  <path fill="#222222" d="M75.5,85.8l-5.6-5.6C72.5,75.3,74,70,74,64.6V64h3v-3h-3V47.5c0-0.8-0.7-1.5-1.5-1.5H62v-9
                      c0-0.8-0.7-1.5-1.5-1.5H56V31C56,18.6,45.9,8.5,33.5,8.5S11,18.6,11,31v4.5H6.5c-0.8,0-1.5,0.7-1.5,1.5v10.5V55v7.5v7.5v7.5v4.5
                      c0,4.1,3.4,7.5,7.5,7.5h24.7c3.4,3.3,7.4,6.3,12.1,8.8c0.2,0.1,0.5,0.2,0.7,0.2s0.5-0.1,0.7-0.2c7.6-4,13.5-9.4,17.5-15.4l5.1,5.1
                      H68v3h9c0.8,0,1.5-0.7,1.5-1.5v-9h-3L75.5,85.8L75.5,85.8z M71,64.6c0,4.6-1.2,9.2-3.4,13.4L65,75.4c1.6-3.6,2.6-7.4,2.9-11.4H71
                      V64.6z M65,61H50c-0.6,0-1.2,0.4-1.4,0.9c-0.2,0.6-0.1,1.2,0.3,1.6l12.4,12.4c-2.6,4.8-6.5,8.9-11.4,11.8c-9.3-5.4-15-15.3-15-26.1
                      V55h30L65,61L65,61z M53.6,64h11.3c-0.2,3.2-1,6.2-2.2,9.1L53.6,64z M14,31c0-10.7,8.7-19.5,19.5-19.5S53,20.3,53,31v4.5h-4.5V31
                      c0-8.3-6.7-15-15-15s-15,6.7-15,15v4.5H14V31z M45.5,35.5h-24V31c0-6.6,5.4-12,12-12s12,5.4,12,12V35.5z M8,64h9v4.5H8V64z M17,61
                      h-9v-4.5h9V61z M8,71.5h9v4.5H8V71.5z M17,53.5h-9V49h9V53.5z M12.5,86.4c-2.5,0-4.5-2-4.5-4.5v-3h10.5c0.8,0,1.5-0.7,1.5-1.5v-7.5
                      v-7.5V55v-7.5c0-0.8-0.7-1.5-1.5-1.5H8v-7.5H59V46H27.5c-0.8,0-1.5,0.7-1.5,1.5v17.1c0,7.8,3,15.4,8.5,21.9
                      C34.5,86.4,12.5,86.4,12.5,86.4z M50,95.2c-13.2-7.3-21-18.7-21-30.7V49h42v12h-3v-7.5c0-0.8-0.7-1.5-1.5-1.5h-33
                      c-0.8,0-1.5,0.7-1.5,1.5v8.2c0,12.1,6.6,23.3,17.3,29.1c0.2,0.1,0.5,0.2,0.7,0.2s0.5-0.1,0.7-0.2c5.5-3,9.9-7.4,12.8-12.6l2.5,2.5
                      C62.5,86.3,57,91.4,50,95.2z"/>
                  <rect x="80" y="61" fill="#222222" width="3" height="3"/>
                  <rect x="86" y="61" fill="#222222" width="3" height="3"/>
                  <rect x="91.9" y="61" fill="#222222" width="3" height="3"/>
                  <rect x="86" y="20.5" fill="#222222" width="3" height="3"/>
                  <rect x="91.9" y="20.5" fill="#222222" width="3" height="3"/>
                  <path fill="#222222" d="M91.9,35.5H74.6l3.4-3.4L75.9,30l-6,6c-0.6,0.6-0.6,1.5,0,2.1l6,6l2.1-2.1l-3.4-3.5h17.4V35.5z"/>
                  <path fill="#222222" d="M85.5,45.6l-2.1-2.1l-6,6c-0.6,0.6-0.6,1.5,0,2.1l6,6l2.1-2.1L82.1,52h12.9v-3H82.1L85.5,45.6z"/>
                  <path fill="#222222" d="M66.9,29.1L69,27l-3.4-3.5h17.4v-3H65.6l3.4-3.4L66.9,15l-6,6c-0.6,0.6-0.6,1.5,0,2.1L66.9,29.1z"/>
                  <path fill="#FF0000" d="M14,31c0-10.7,8.7-19.5,19.5-19.5S53,20.3,53,31v4.5h-4.5V31c0-8.3-6.7-15-15-15s-15,6.7-15,15v4.5H14V31z"
                      />
                  <rect x="8" y="49" fill="#FF0000" width="9" height="4.5"/>
                  <rect x="8" y="56.5" fill="#FF0000" width="9" height="4.5"/>
                  <rect x="8" y="64" fill="#FF0000" width="9" height="4.5"/>
                  <rect x="8" y="71.5" fill="#FF0000" width="9" height="4.5"/>
                  <path fill="#FF0000" d="M50,95.2c-13.2-7.3-21-18.7-21-30.7V49h42v12h-3v-7.5c0-0.8-0.7-1.5-1.5-1.5h-33c-0.8,0-1.5,0.7-1.5,1.5v8.2
                      c0,12.1,6.6,23.3,17.3,29.1c0.2,0.1,0.5,0.2,0.7,0.2s0.5-0.1,0.7-0.2c5.5-3,9.9-7.4,12.8-12.6l2.5,2.5C62.5,86.3,57,91.4,50,95.2z"
                      />
                  <path fill="#FF0000" d="M71,64.6c0,4.6-1.2,9.2-3.4,13.4L65,75.4c1.6-3.6,2.6-7.4,2.9-11.4H71V64.6z"/>
                  </svg>
              </div>
              <div class="safe_text">
                <h3>Safe Investment </h3>
                <p>
                  TRodds presents revolutionary scan system that turns sports betting into a risk free investment. The system scans all bookmakers’ lines and detects arbitrage situations (often called arbs or surebets). All you need is to place bets covering all outcomes
                  of the game according to the efficient SureBet Calculator. TRodds is 100% safe, secure, easy to use, and it’s on your side.
                </p>
              </div>
            </div>
          </div>
          <div class="section_reasons">
            <div class="reasons_safe reasons_star">
              <div class="safe_logo">
                
                <!-- Generator: Adobe Illustrator 17.1.0, SVG Export Plug-In . SVG Version: 6.00 Build 0)  -->
                <!DOCTYPE svg PUBLIC "-//W3C//DTD SVG 1.1//EN" "http://www.w3.org/Graphics/SVG/1.1/DTD/svg11.dtd">
                <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 396 396" enable-background="new 0 0 396 396" xml:space="preserve">
                        <g>
                          <path fill="#222222" d="M258,181.1c-2.3,2.2-3.3,5.4-2.8,8.5l10.7,62.1l-55.8-29.3c-2.8-1.5-6.1-1.5-8.9,0l-55.8,29.3l10.7-62.1
                            c0.5-3.1-0.5-6.3-2.8-8.5l-45.1-44l62.4-9.1c3.1-0.5,5.8-2.4,7.2-5.3l27.9-56.5l27.9,56.5c1.4,2.8,4.1,4.8,7.2,5.3l62.4,9.1
                            L258,181.1z M258,181.1"/>
                          <path fill="#222222" d="M132.6,276c-1.4,0-2.8-0.4-4-1.3c-2.1-1.5-3.1-4.1-2.7-6.6l13.3-77.8l-56.5-55.1c-1.9-1.8-2.5-4.5-1.7-6.9
                            c0.8-2.4,2.9-4.2,5.5-4.6l78.1-11.4l34.9-70.8c1.1-2.3,3.5-3.8,6.1-3.8c2.6,0,4.9,1.4,6.1,3.8l34.9,70.8l78.1,11.4
                            c2.5,0.4,4.7,2.2,5.5,4.6c0.8,2.5,0.1,5.1-1.7,6.9l-56.5,55.1l13.3,77.8c0.4,2.6-0.6,5.1-2.7,6.6c-1.2,0.8-2.5,1.3-4,1.3
                            c-1.1,0-2.2-0.3-3.1-0.8l-69.9-36.7l-69.9,36.7C134.8,275.8,133.7,276,132.6,276z M205.6,224.1c1.1,0,2.2,0.3,3.1,0.8l60.9,32
                            L258,189.1c-0.4-2.2,0.4-4.4,1.9-6l49.2-48l-68.1-9.9c-2.2-0.3-4.1-1.7-5.1-3.7l-30.4-61.7l-30.4,61.7c-1,2-2.9,3.4-5.1,3.7
                            l-68.1,9.9l49.2,48c1.6,1.6,2.3,3.8,1.9,6l-11.6,67.8l60.9-32C203.4,224.4,204.5,224.1,205.6,224.1z"/>
                          <path fill="#222222" d="M72.6,177.3"/>
                          <path fill="#222222" d="M72.6,348c-3.7,0-6.8-3-6.8-6.8V186.9c0-3.7,3-6.8,6.8-6.8c3.7,0,6.8,3,6.8,6.8v154.3
                            C79.4,344.9,76.3,348,72.6,348z"/>
                          <path fill="#222222" d="M340.8,177.3"/>
                          <path fill="#222222" d="M340.8,348c-3.7,0-6.8-3-6.8-6.8V186.9c0-3.7,3-6.8,6.8-6.8c3.7,0,6.8,3,6.8,6.8v154.3
                            C347.5,344.9,344.5,348,340.8,348z"/>
                          <path fill="#222222" d="M273.2,294.5"/>
                          <path fill="#222222" d="M273.2,348c-3.7,0-6.8-3-6.8-6.8v-37.1c0-3.7,3-6.8,6.8-6.8c3.7,0,6.8,3,6.8,6.8v37.1
                            C280,344.9,276.9,348,273.2,348z"/>
                          <path fill="#222222" d="M205.6,264.4"/>
                          <path fill="#222222" d="M205.6,348c-3.7,0-6.8-3-6.8-6.8V274c0-3.7,3-6.8,6.8-6.8c3.7,0,6.8,3,6.8,6.8v67.2
                            C212.4,344.9,209.3,348,205.6,348z"/>
                          <path fill="#222222" d="M138,294.5"/>
                          <path fill="#222222" d="M138,348c-3.7,0-6.8-3-6.8-6.8v-37.1c0-3.7,3-6.8,6.8-6.8c3.7,0,6.8,3,6.8,6.8v37.1
                            C144.8,344.9,141.8,348,138,348z"/>
                        </g>
                        <path fill="#FF0000" d="M261.8,181.7c-2.4,2.4-3.5,5.8-3,9.1l11.4,66.6L210.4,226c-3-1.6-6.6-1.6-9.6,0L141,257.5l11.4-66.6
                          c0.6-3.3-0.5-6.8-3-9.1L101,134.5l66.9-9.7c3.4-0.5,6.3-2.6,7.8-5.6l29.9-60.6l29.9,60.6c1.5,3,4.4,5.1,7.8,5.6l66.9,9.7
                          L261.8,181.7z"/>
                        <g>
                          <path fill="#222222" d="M88.2,326"/>
                        </g>
                        </svg>
              </div>
              <div class="safe_text">
                <h3>Reliable Bookmakers  </h3>
                <p>
                  If you want to get rich through arbitrage betting (arbing) you can only use bookmakers that actually let you withdraw the profits. In order to give you the best possible arbing experience, we partner only with the leading and most trusted bookies in the
                  industry. The system will detect automatically surebets only from those bookies who accept bettors from your country of residence.
                </p>
              </div>
            </div>
          </div>
          <div class="section_reasons">
            <div class="reasons_safe">
              <div class="safe_logo">
               
                <!-- Generator: Adobe Illustrator 17.1.0, SVG Export Plug-In . SVG Version: 6.00 Build 0)  -->
                <!DOCTYPE svg PUBLIC "-//W3C//DTD SVG 1.1//EN" "http://www.w3.org/Graphics/SVG/1.1/DTD/svg11.dtd">
                <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 100 100" enable-background="new 0 0 100 100" xml:space="preserve">
                        <path fill="#222222" d="M14.1,84.4c-6,0-10.9,4.9-10.9,10.9c0,0.9,0.7,1.6,1.6,1.6h18.8c0.9,0,1.6-0.7,1.6-1.6
                          C25,89.3,20.1,84.4,14.1,84.4z M6.4,93.8c0.6-3,3-5.4,6.1-6.1v2.9h3.1v-2.9c3,0.6,5.5,3,6.1,6.1H6.4z"/>
                        <path fill="#222222" d="M95.3,70.3h-2.7c-0.1-0.2-0.2-0.4-0.3-0.6l1.9-1.9c0.6-0.6,0.6-1.6,0-2.2l-6.6-6.6c-0.3-0.3-0.7-0.5-1.2-0.4
                          c2.5-1.3,4.2-3.9,4.2-6.9V10.9c0-4.3-3.5-7.8-7.8-7.8H10.9c-4.3,0-7.8,3.5-7.8,7.8v40.6c0,4.3,3.5,7.8,7.8,7.8h20.4l-5.9,10.9H25
                          h-2.3c-3,0-5.5,2.5-5.5,5.5s2.5,5.5,5.5,5.5h33.6c0,0.9,0.7,1.6,1.6,1.6h2.7c0.1,0.2,0.2,0.4,0.3,0.6l-1.9,1.9
                          c-0.6,0.6-0.6,1.6,0,2.2l6.6,6.6c0.6,0.6,1.6,0.6,2.2,0l1.9-1.9c0.2,0.1,0.4,0.2,0.6,0.3v2.7c0,0.9,0.7,1.6,1.6,1.6h9.4
                          c0.9,0,1.6-0.7,1.6-1.6v-2.7c0.2-0.1,0.4-0.2,0.6-0.3l1.9,1.9c0.6,0.6,1.6,0.6,2.2,0l6.6-6.6c0.6-0.6,0.6-1.6,0-2.2l-1.9-1.9
                          c0.1-0.2,0.2-0.4,0.3-0.6h2.7c0.9,0,1.6-0.7,1.6-1.6v-9.4C96.9,71,96.2,70.3,95.3,70.3z M82.8,59.4c0.9,0,1.8-0.2,2.6-0.5l-1.9,1.9
                          c-0.2-0.1-0.4-0.2-0.6-0.3L82.8,59.4L82.8,59.4z M10.9,6.3h71.9c2.6,0,4.7,2.1,4.7,4.7v32.8H6.3V10.9C6.3,8.4,8.4,6.3,10.9,6.3z
                           M6.3,51.6v-4.7h81.3v4.7c0,2.6-2.1,4.7-4.7,4.7h-1.6h-9.4H59.8H33.9h-23C8.4,56.3,6.3,54.1,6.3,51.6z M63.7,60.7l-0.9-1.3H65
                          L63.7,60.7z M70.3,59.4v1.2c-0.2,0.1-0.4,0.2-0.6,0.3l-1.4-1.4H70.3z M22.7,78.1c-1.3,0-2.3-1.1-2.3-2.3s1.1-2.3,2.3-2.3H25h1.3
                          h29.9v4.7H22.7z M29,70.3l5.9-10.9H59l2.4,3.6l-2.5,2.5c-0.6,0.6-0.6,1.6,0,2.2l1.9,1.9c-0.1,0.2-0.2,0.4-0.3,0.6h-2.7L29,70.3
                          L29,70.3z M93.8,79.7h-2.3c-0.7,0-1.3,0.4-1.5,1.1c-0.2,0.8-0.6,1.5-1,2.3c-0.3,0.6-0.2,1.3,0.3,1.8l1.6,1.6l-4.4,4.4l-1.6-1.6
                          c-0.5-0.5-1.2-0.6-1.8-0.3c-0.8,0.4-1.5,0.7-2.3,1c-0.6,0.2-1.1,0.8-1.1,1.5v2.3h-6.3v-2.3c0-0.7-0.4-1.3-1.1-1.5
                          c-0.8-0.2-1.5-0.6-2.3-1c-0.6-0.3-1.3-0.2-1.8,0.3l-1.6,1.6l-4.4-4.4l1.6-1.6c0.5-0.5,0.6-1.2,0.3-1.8c-0.4-0.8-0.7-1.5-1-2.3
                          c-0.2-0.6-0.8-1.1-1.5-1.1h-2.2v-6.3h2.3c0.7,0,1.3-0.4,1.5-1.1c0.2-0.8,0.6-1.5,1-2.3c0.3-0.6,0.2-1.3-0.3-1.8l-1.6-1.6l4.4-4.4
                          l1.6,1.6c0.5,0.5,1.2,0.6,1.8,0.3c0.8-0.4,1.5-0.7,2.3-1c0.6-0.2,1.1-0.8,1.1-1.5v-2.2h6.3v2.3c0,0.7,0.4,1.3,1.1,1.5
                          c0.8,0.2,1.5,0.6,2.3,1c0.6,0.3,1.3,0.2,1.8-0.3l1.6-1.6l4.4,4.4l-1.6,1.6c-0.5,0.5-0.6,1.2-0.3,1.8c0.4,0.8,0.7,1.5,1,2.3
                          c0.2,0.6,0.8,1.1,1.5,1.1h2.2V79.7z"/>
                        <path fill="#222222" d="M76.6,67.2c-5.2,0-9.4,4.2-9.4,9.4s4.2,9.4,9.4,9.4s9.4-4.2,9.4-9.4S81.7,67.2,76.6,67.2z M76.6,82.8
                          c-3.4,0-6.3-2.8-6.3-6.3s2.8-6.3,6.3-6.3s6.3,2.8,6.3,6.3S80,82.8,76.6,82.8z"/>
                        <rect x="64.1" y="34.4" fill="#222222" width="3.1" height="3.1"/>
                        <rect x="70.3" y="34.4" fill="#222222" width="3.1" height="3.1"/>
                        <rect x="76.6" y="34.4" fill="#222222" width="3.1" height="3.1"/>
                        <path fill="#FF0000" d="M6.3,51.6v-4.7h81.3v4.7c0,2.6-2.1,4.7-4.7,4.7h-1.6h-9.4H59.8H33.9h-23C8.4,56.3,6.3,54.1,6.3,51.6z"/>
                        <path fill="#FF0000" d="M22.7,78.1c-1.3,0-2.3-1.1-2.3-2.3s1.1-2.3,2.3-2.3H25h1.3h29.9v4.7H22.7z"/>
                        </svg>
              </div>
              <div class="safe_text">
                <h3>Usability and Cutting-Edge Technology</h3>
                <p>
                  There is no need to install any software - new surebets are updated automatically on our dynamic website. Once an arbing opportunity is detected, you immediately get all needed information to pick up the surebet: the bookmakers involved, the sport, the
                  event, the odds and the profit percentage. TRodds SureBet Calculator displays how much you should stake on each player/team in order to gain the target profit.
                </p>
              </div>
            </div>
          </div>
          <div class="bottom_title_main">
            <h2>
                    Don’t waste a single moment, sign up now for TRodds
                    and receive our special and limited time offer
                </h2>
            <a class="bottom_title_main_try" href="/signup">Try TRodds now</a>
          </div>
      <?else:?>

        <?php if(!Yii::$app->user->identity->isSubscriber): ?>
        <div class="home_limited">
          <h2>Limited account warning!</h2>
          <p>
            In order to enjoy from the SureBet Calculator, to see the full details about arbitrage situations and to take the advantage of our full featured
            <span>Premium Package</span> please upgrade your account just for <span>€9.99/month.</span>
          </p>
          <a href="/site/deposit">Upgrade</a>
        </div>
        <?php elseif(Yii::$app->user->identity->role == 20 && Yii::$app->user->identity->subscribed == 0): ?>

            <?php
                $today = time();
                if($today >= Yii::$app->user->identity->payed_day){
                    User::clearPayedPeriod();
                    $this->context->refresh();
                }else{
                    $remained = Yii::$app->user->identity->payed_day - $today;
                    $sec_remained = $remained % 60;
                    $min_remained = (($remained - $sec_remained) / 60) % 60;
                    $hours = (($remained - $sec_remained - 60 * $min_remained) / 3600) % 24;
                    $days = (($remained - $sec_remained - 60 * $min_remained - 3600 * $hours) / (3600 * 24));
                }
                $left_str = 'Left ';
                if($days > 1){
                    $left_str .= $days.' Days ';
                }elseif ($days == 1) {
                    $left_str .= $days.' Day ';
                }
                $left_str .= $hours.':';
                $left_str .= ($min_remained < 10 && $min_remained > 0)?'0':'';
                $left_str .= $min_remained.'.';
            ?>

            <?php $prefix = !empty(Yii::$app->params['paypal']) && !empty(Yii::$app->params['paypal']['sandbox']) ? 'sandbox.' : ''; ?>
            <?php $hosted_button_id = !empty(Yii::$app->params['paypal']) && !empty(Yii::$app->params['paypal']['hosted_button_id']) ? Yii::$app->params['paypal']['hosted_button_id'] : ''; ?>
            <form action="https://www.<?= $prefix ?>paypal.com/cgi-bin/webscr" method="post" target="_top">
                <input type="hidden" name="cmd" value="_s-xclick">
                <input type="hidden" name="hosted_button_id" value="<?= $hosted_button_id ?>">
                <input type="hidden" name="custom" value="<?= Yii::$app->user->id ?>">
                <div class="unsubscribed-block">
                    <i class="alarm_button">
                    <span>Unsubscribed Account! </br> <?=$left_str?>.</span> In order to continue you <b>Premium Package</b> subscription and to enjoy from the SureBet Calculator and the full details about arbitrage situations please upgrade your account just
                    for <b>€9.99/month.</b>
                    <a href="/site/deposit" class="upgrade-acc-btn">Upgrade</a>
                    </i>
                </div>
            </form>
        <?endif;?>
        
        <?if(!Yii::$app->user->identity->isSubscriber):?>
        <div class="home_table_bg">
          <div class="home_user_table_main col-md-12 col-sm-12 col-xs-12">
            <div class="home_table clearfix">
              <div class="table_info_pagination clearfix">
                <div class="home_user_pagination">
                  <ul class="home_pagination_list clearfix table-pagination">
                  </ul>
                </div>
              </div>
              <div class="home_user_table" id="tableMain">

              </div>
              <div class="table_info_pagination clearfix">
                <div class="home_user_pagination">
                  <ul class="home_pagination_list clearfix table-pagination">
                  </ul>
                </div>
              </div>
            </div>
          </div>
        </div>
      <?elseif(Yii::$app->user->identity->isSubscriber) : ?>

      <div class="home_user_table_container row">
        <div class="home_user_right col-md-7 col-sm-12 col-xs-12">
          <div class="home_table clearfix">

            <div class="table_info_pagination clearfix">
              <div class="home_user_pagination">
                <ul class="home_pagination_list clearfix table-pagination">
                </ul>
              </div>
              <p>SUREBETS</p>
            </div>
            <div class="home_user_table" id="tableMain">
            </div>
            <div class="table_info_pagination clearfix">
              <div class="home_user_pagination">
                <ul class="home_pagination_list clearfix table-pagination">
                </ul>
              </div>
            </div>
          </div>
        </div>
        <div class="home_user_left col-md-5 col-sm-12 col-xs-12">
          <div class="home_user_table_one" id="details">
            
          </div>
        </div>
      <?endif;?>
    <?endif;?>
  </main>