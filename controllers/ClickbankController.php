<?php
namespace frontend\controllers;

use Yii;
// use yii\base\InvalidParamException;
// use yii\web\BadRequestHttpException;
// use yii\web\Controller;
// use yii\filters\VerbFilter;
// use yii\filters\AccessControl;
// use common\models\LoginForm;
// use common\models\Bookmaker;
// use common\models\UserBookmakers;
// use frontend\models\PasswordResetRequestForm;
// use frontend\models\ResetPasswordForm;
use frontend\models\SignupForm;
use frontend\controllers\SiteController;

/**
 * Site controller
 */
class ClickbankController extends SiteController
{
    public function getViewPath()
    {
        return Yii::getAlias('@frontend/views/site');
    }

    /**
     * Signs user up.
     *
     * @return mixed
     */
    public function actionSignup()
    {
        $model = new SignupForm();
        if ($model->load(Yii::$app->request->post())) {
            if ($user = $model->signup()) {
                if (Yii::$app->getUser()->login($user)) {
                    // return $this->goHome();
                    return $this->redirect('http://1.trodds.pay.clickbank.net?vtid='.Yii::$app->user->id);
                }
            }
        }

        return $this->render('signup', [
            'model' => $model,
        ]);
    }

}
