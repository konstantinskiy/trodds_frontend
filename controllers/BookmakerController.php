<?php

namespace frontend\controllers;

use Yii;
use yii\data\ActiveDataProvider;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;
use yii\web\Controller;
use yii\web\NotFoundHttpException;

use common\models\Bookmaker;

/**
 * BookmakerController implements the CRUD actions for Bookmaker model.
 */
class BookmakerController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Bookmaker models.
     * @return mixed
     */
    public function actionIndex($tutotrial = false)
    {
        $query = Bookmaker::find();

        if (!Yii::$app->user->isGuest) {
            $userId = Yii::$app->user->identity->id;
            $query->joinWith('userBookmakers ub')->where(['ub.user_id' => $userId]);
        }

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'tutotrial' => $tutotrial,
        ]);
    }

    /**
     * Finds the Bookmaker model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return Bookmaker the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Bookmaker::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
