<?php
namespace frontend\controllers;

use common\models\User;
use Yii;
use yii\base\InvalidParamException;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\httpclient\Client;
use common\models\LoginForm;
use frontend\models\PasswordResetRequestForm;
use frontend\models\ResetPasswordForm;
use frontend\models\SignupForm;
use frontend\models\ContactForm;

/**
 * Subscription controller
 */
class SubscriptionController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['index', 'odds'],
                'rules' => [
                    [
                        'actions' => ['odds'],
                        'allow' => true,
                        'roles' => ['@', '?'],
                    ],
                    [
                        'actions' => ['index'],
                        'allow' => true,
                        'roles' => ['@', '?'],
                    ],
                ],
            ],
            // 'verbs' => [
            //     'class' => VerbFilter::className(),
            //     'actions' => [
            //         'logout' => ['post'],
            //     ],
            // ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return mixed
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

    public function actionOdds()
    {
        $request = Yii::$app->request;

        if ($request->isAjax) {
            $get = $request->get();

            $isPaid = !Yii::$app->user->isGuest && Yii::$app->user->identity->role == User::ROLE_SUBSCRIBER;

            $client = new Client();
            $client = $client->createRequest()
                ->setMethod('get')
                ->setUrl(Yii::$app->params['engineUrl'] . Yii::$app->user->id . '&paid=' . $isPaid);

            // if (!Yii::$app->user->isGuest) {
                $client->setData($request->get());
            // }

            $response = $client->send();

            if ($response->isOk) {
                echo $response->content;
            } 
        }
    }

}
