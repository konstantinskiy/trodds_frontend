<?php

namespace frontend\controllers;

use Yii;
use frontend\models\ChangePasswordForm;
use yii\filters\AccessControl;
use common\models\User;
use common\components\AccessRule;
use frontend\models\UserBookmaker;

class SettingsController extends \yii\web\Controller
{

    public $defaultAction = 'account';

    public function behaviors(){
        return [
            'access' => [
                'class' => AccessControl::className(),
                'ruleConfig' => [
                    'class' => AccessRule::className(),
                ],
                'rules' => [
                    [
                        'actions' => ['account', 'changepassword', 'bookies'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    /**
     * @param bool $tutotrial
     * @return string|\yii\web\Response
     */
    public function actionAccount($tutotrial = false)
    {
        $model = User::findOne(Yii::$app->user->identity->id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->refresh();
        } else {
            return $this->render('account', [
                'model' => $model,
                'tutotrial' => $tutotrial,
            ]);
        }
    }

    /**
     * @param bool $tutotrial
     * @return string
     */
    public function actionBookies($tutotrial = false)
    {
        $model = new UserBookmaker();
        if(($model->list = Yii::$app->request->post()) && !Yii::$app->user->isGuest){
            $userId = Yii::$app->user->identity->id;
            $model->updateList($userId);
        }
        return $this->render('bookies', ['tutotrial' => $tutotrial]);
    }

    /**
     * @return string|\yii\web\Response
     */
    public function actionChangepassword(){
        $model = new ChangePasswordForm;
        $modeluser = Yii::$app->user->identity;
      
        if($model->load(Yii::$app->request->post())){
            if($model->validate()){
                try{
                    $modeluser->setPassword($_POST['ChangePasswordForm']['newpass']);
                    if($modeluser->save()){
                        Yii::$app->getSession()->setFlash('success', 'Your password has been successfully changed.');
                        return $this->redirect(['changepassword']);
                    }else{
                        Yii::$app->getSession()->setFlash(
                            'error','Password not changed'
                        );
                        return $this->redirect(['changepassword']);
                    }
                }catch(Exception $e){
                    Yii::$app->getSession()->setFlash(
                        'error',"{$e->getMessage()}"
                    );
                    return $this->render('changepassword',[
                        'model'=>$model
                    ]);
                }
            }else{
                return $this->render('changepassword',[
                    'model'=>$model
                ]);
            }
        }else{
            return $this->render('changepassword',[
                'model'=>$model
            ]);
        }
    }
}
