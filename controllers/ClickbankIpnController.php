<?php

namespace frontend\controllers;

use Yii;
use common\models\User;

class ClickbankIpnController extends \yii\web\Controller
{

    public $enableCsrfValidation = false;

    public function actionIndex()
    {

        $request = Yii::$app->request;
        
        if ($request->isPost) {

            $file = 'new.txt';
            $content = file_get_contents($file);

            $content .= json_encode($_POST);

            file_put_contents($file, $content);

            // $type = $request->post('txn_type');
            // $payer_email = $request->post('payer_email');
            // $user_id = $request->post('custom');

            // if (($user = User::findOne($user_id)) !== null) {

            //     // Payed for Paypal subscription
            //     if($type == 'subscr_payment') {
            //         $user->role = User::ROLE_SUBSCRIBER;
            //         $user->paying_email = $payer_email;
            //         $user->save();
                    
            //         // Subscribe to Mailchimp's Paid list
            //         try {
            //             Yii::$app->mailchimp->post('/lists/c730e24c17/members', [
            //                 'status' => 'subscribed',
            //                 'email_address' => $user->email,
            //                 'merge_fields' => [
            //                     'FNAME' => $user->first_name,
            //                     'USER_ID' => $user->id
            //                 ]
            //             ]);
            //         } catch(\Exception  $e) {

            //         }

            //         // Unsubscribe from Mailchimp's regular list
            //         try {
            //             Yii::$app->mailchimp->delete('/lists/7f11b68199/members/'.md5($user->email));
            //         } catch(\Exception  $e) {

            //         }
            //     }

            //     // Unsubscribe Paypal Payment
            //     if($type == 'subscr_cancel' || $type == 'subscr_eot' || $type == 'subscr_failed') {
            //         $user->role = User::ROLE_USER;
            //         $user->save();

            //         // Unsubscribe from Mailchimp's Paid list
            //         try {
            //             Yii::$app->mailchimp->post('/lists/c730e24c17/members/'.md5($user->email));
            //         } catch(\Exception  $e) {

            //         }
            //     }

            // }

        }
    }

}
