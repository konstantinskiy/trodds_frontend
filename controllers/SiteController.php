<?php
namespace frontend\controllers;

use Yii;
use yii\base\InvalidParamException;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\web\Response;
use common\models\LoginForm;
use common\models\Bookmaker;
use common\models\UserBookmakers;
use common\models\AwayWin;
use common\models\Draw;
use common\models\HomeWin;
use frontend\models\PasswordResetRequestForm;
use frontend\models\ResetPasswordForm;
use frontend\models\SignupForm;
use frontend\models\ContactForm;
use frontend\models\EmailError;
use common\models\User;
use frontend\models\Confirm;
use frontend\components\AuthSocial;
use frontend\components\Mailer;

/**
 * Site controller
 */
class SiteController extends Controller
{

    public $defaultAction = 'index';

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout', 'signup'],
                'rules' => [
                    [
                        'actions' => ['signup', 'login'],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                    [
                        'actions' => ['logout', 'redirectbookmaker'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            // 'verbs' => [
            //     'class' => VerbFilter::className(),
            //     'actions' => [
            //         'logout' => ['post'],
            //     ],
            // ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
            'auth' => [
                'class' => 'yii\authclient\AuthAction',
                'successCallback' => [new AuthSocial(), 'onAuthSuccess'],
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return mixed
     */
    public function actionIndex()
    {
        if (!Yii::$app->user->isGuest) {
            $this->redirect(['subscription/index']);
        }
        return $this->render('index');
    }

    /**
     * Displays homepage.
     *
     * @return mixed
     */
    public function actionClickbank()
    {
        if (!Yii::$app->user->isGuest) {
            $this->redirect(['subscription/index']);
        }
        return $this->render('clickbank');
    }

    /**
     * Logs in a user.
     *
     * @return mixed
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            // return $this->goHome();
            return $this->redirect(['subscription/index']);
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            // return $this->goBack();
            return $this->redirect(['subscription/index']);
        } else {
            return $this->render('login', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Logs out the current user.
     *
     * @return mixed
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Displays contact page.
     *
     * @return mixed
     */
    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($model->sendEmail(Yii::$app->params['adminEmail'])) {
                Yii::$app->session->setFlash('success', 'Thank you for contacting us. We will respond to you as soon as possible.');
            } else {
                Yii::$app->session->setFlash('error', 'There was an error sending email.');
            }

            return $this->refresh();
        } else {
            return $this->render('contact', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Displays about page.
     *
     * @return mixed
     */
    public function actionAbout()
    {
        return $this->render('about');
    }

    /**
     * Displays start page.
     *
     * @return mixed
     */
    public function actionStart()
    {
        return $this->render('start');
    }


    /**
     * Signs user up.
     *
     * @return mixed
     */
    public function actionSignup()
    {
        $model = new SignupForm();

        if ($model->load(Yii::$app->request->post())) {
            $referrer = explode('/', Yii::$app->request->referrer);
            if (array_pop($referrer) == 'signup' && $user = $model->signup()) {
                $cookies = Yii::$app->response->cookies;
                $cookies->add(new \yii\web\Cookie([
                    'name' => 'id',
                    'value' => $user->id,
                ]));
                $cookies->add(new \yii\web\Cookie([
                    'name' => 'email',
                    'value' => $user->email,
                ]));

                $loginModel = new LoginForm();
                $loginModel->email = $user->email;
                $loginModel->password = $model->password;
                $loginModel->login();

                return $this->redirect(['/subscription/index']);
            }
        }

        return $this->render('signup', [
            'model' => $model,
        ]);
    }

    /**
     * @return mixed
     */
    public function actionConfirmEmail($token)
    {
        $model = User::findIdentityByAccessToken($token);

        if (!empty($model) && $model->confirmEmail() && Yii::$app->getUser()->login($model)) {
            Yii::$app->session->setFlash('success', 'Your email is confirmed. Thank you for signup on our web-site.');
        } else {
            Yii::$app->session->setFlash('error', 'Access token error. Please check your confirmation link.');
        }
        return $this->redirect(['subscription/index']);
    }

    /**
     * @return \yii\web\Response
     */
    public function actionRequestConfirmEmail()
    {
        if (!Yii::$app->user->isGuest) {
            $user = Yii::$app->user->identity;
            if (!empty($user)) {
                $user->generateAccessToken();
                if ($user->save()) {
                    $mailer = new Mailer();
                    $mailer->sendSignupEmail($user);
                    Yii::$app->session->setFlash('success', 'Check your email for further instructions to complete your account.<br />Follow the sent link to сonfirm your email.');
                }
            }
        }

        return $this->goBack();
    }

    /**
     * @return array
     */
    public function actionChangeEmail()
    {
        $success = false;
        $email = Yii::$app->request->post('email');

        if (!Yii::$app->user->isGuest && !empty($email)) {
            $user = Yii::$app->user->identity;
            $user->email = $email;
            if ($success = $user->save()) {
                Yii::$app->session->setFlash('success', 'Your email was successfully changed.');
            }
        }

        Yii::$app->response->format = Response::FORMAT_JSON;
        return [
            'success' => $success,
        ];
    }


    public function actionDeposit()
    {

        return $this->render('deposit');

    }

    /**
     * That function is return view with change mail
     *
     * @return string|\yii\web\Response
     */
    public function actionErrormail()
    {
        
        $cookies = Yii::$app->request->cookies;
        $id = $cookies->getValue('id', 0);
        $email = $cookies->getValue('email', '');
        
        $model = new EmailError();
        if($model->load(Yii::$app->request->post()) && $user = $model->changeEmail()){
            return $this->redirect(['/error-mail']);
        }
        
        if($id !== 0){
            return $this->render('errorMail', ['id' => $id, 'model' => $model, 'email' => $email]);
        }else{
            return $this->redirect(['/getting-started']);
        }
    }
    
    /**
     * Requests password reset.
     *
     * @return mixed
     */
    public function actionRequestPasswordReset()
    {
        $model = new PasswordResetRequestForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($model->sendEmail()) {
                Yii::$app->session->setFlash('success', 'Check your email for further instructions to change your password.<br />Follow the sent link to changing the password.');

                return $this->goHome();
            } else {
                Yii::$app->session->setFlash('error', 'Sorry, we are unable to reset password for email provided.');
            }
        }

        return $this->render('requestPasswordResetToken', [
            'model' => $model,
        ]);
    }

    /**
     * Resets password.
     *
     * @param string $token
     * @return mixed
     * @throws BadRequestHttpException
     */
    public function actionResetPassword($token)
    {
        try {
            $model = new ResetPasswordForm($token);
        } catch (InvalidParamException $e) {
            return $this->redirect('request-password-expired');
        }

        if ($model->load(Yii::$app->request->post()) && $model->validate() && $model->resetPassword()) {
            Yii::$app->session->setFlash('success', 'New password was saved.');

            return $this->goHome();
        }

        return $this->render('resetPassword', [
            'model' => $model,
        ]);
    }

    /**
     * @return string
     */
    public function actionRequestPasswordExpired()
    {
        return $this->render('requestPasswordExpired');
    }

    /**
     * @return \yii\web\Response
     */
    public function actionRedirectbookmaker()
    {
        $param = Yii::$app->request->queryParams;

        if (empty($param['bookmaker_id'])) {
            return $this->redirect(['bookmaker/index']);
        }

        $bookmaker_id = (int) $param['bookmaker_id'];
        $bet_id = !empty($param['bet_id']) ? (int) $param['bet_id'] : null;
        $bet_type = !empty($param['bet_type']) ? $param['bet_type'] : null;

        if ($userBookmaker = UserBookmakers::findOne(['user_id' => Yii::$app->user->id, 'bookmakers_id' => $bookmaker_id])) {
            $userBookmaker->times_clicked = $userBookmaker->times_clicked + 1;
        } else {
            $userBookmaker = new UserBookmakers();
            $userBookmaker->user_id = Yii::$app->user->id;
            $userBookmaker->bookmakers_id = $bookmaker_id;
            $userBookmaker->times_clicked = 1;
        }
        $userBookmaker->save();

        $bookmaker = Bookmaker::findOne(['id' => $bookmaker_id]);

        $findParams = ['bookmaker_id' => $bookmaker_id];
        if (!empty($bet_id)) {
            $findParams['bet_id'] = $bet_id;
        }

        if($bet_type == 'home') {
            $bet = HomeWin::findOne($findParams);
        } elseif($bet_type == 'away') {
            $bet = AwayWin::findOne($findParams);
        } elseif($bet_type == 'draw') {
            $bet = Draw::findOne($findParams);
        }

        if (isset($bet) && !empty($bet->url)) {
            return $this->redirect($bet->url);
        } elseif (!empty($bookmaker->deep_link_url)) {
            return $this->redirect($bookmaker->deep_link_url);
        } else {
            return $this->redirect($bookmaker->url);
        }
        
    }

    /**
     * @return string|\yii\web\Response
     */
    public function actionThankyou()
    {

        $cookies = Yii::$app->request->cookies;
        $id = $cookies->getValue('id', 0);
        $email = $cookies->getValue('email', '');
        
        $model = new EmailError();
        if($model->load(Yii::$app->request->post()) && $user = $model->changeEmail()){
            return $this->refresh();
        }
        
        if($id !== 0){
            return $this->render('errorMail', ['id' => $id, 'model' => $model, 'email' => $email]);
        }else{
            return $this->redirect(['/getting-started']);
        }
    }

    /**
     * @param null $code
     * @return string|\yii\web\Response
     */
    public function actionConfirm($code = null) {
        $model = new Confirm();
        if($model->load(Yii::$app->request->post()) && $model->confirm()){
            return $this->goHome();
        }
        if($code != null){
            return $this->render('confirm_link', [
                'model' => $model,
                'code' => $code,
            ]);
        }else{
            return $this->render('confirm', [
                'model' => $model
            ]);
        }
    }
    
}
