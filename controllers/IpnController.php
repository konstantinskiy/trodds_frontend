<?php

namespace frontend\controllers;

use Yii;
use common\models\User;

class IpnController extends \yii\web\Controller
{

    public $enableCsrfValidation = false;

    public function actionIndex()
    {

        $request = Yii::$app->request;
        
        if ($request->isPost) {

            $type = $request->post('txn_type');
            $payer_email = $request->post('payer_email');
            $user_id = $request->post('custom');

            if (($user = User::findOne($user_id)) !== null) {

                // Payed for Paypal subscription
                if($type == 'subscr_payment') {
                    $user->role = User::ROLE_SUBSCRIBER;
                    $user->paying_email = $payer_email;
                    
                    $user->subscribed = 1;
                    
                    $user->payed_day = intval(date('t')) * 24 * 3600 + time();
                    
                    $user->save();
                    
                    // Subscribe to Mailchimp's Paid list
                    try {
                        Yii::$app->mailchimp->post('/lists/c730e24c17/members', [
                            'status' => 'subscribed',
                            'email_address' => $user->email,
                            'merge_fields' => [
                                'FNAME' => $user->first_name,
                                'USER_ID' => $user->id
                            ]
                        ]);
                    } catch(\Exception  $e) {

                    }

                    // Unsubscribe from Mailchimp's regular list
                    try {
                        Yii::$app->mailchimp->delete('/lists/7f11b68199/members/'.md5($user->email));
                    } catch(\Exception  $e) {

                    }
                }

                // Unsubscribe Paypal Payment
                if($type == 'subscr_cancel' || $type == 'subscr_eot' || $type == 'subscr_failed') {
//                    $user->role = User::ROLE_USER;
                    
                    $user->subscribed = 0;
//                    $user->payed_day = 0;
                    
                    $user->save();

                    // Unsubscribe from Mailchimp's Paid list
                    try {
                        Yii::$app->mailchimp->post('/lists/c730e24c17/members/'.md5($user->email));
                    } catch(\Exception  $e) {

                    }
                }

            }

        }
    }

}
