<?php

namespace frontend\controllers;

use Yii;
use frontend\models\ContactForm;

use yii\filters\AccessControl;
use common\models\User;
use common\components\AccessRule;

class HelpController extends \yii\web\Controller
{

    public $defaultAction = 'start';

    public function behaviors(){
        return [
            'access' => [
                'class' => AccessControl::className(),
                'ruleConfig' => [
                    'class' => AccessRule::className(),
                ],
                'rules' => [
                    [
                        'actions' => ['about', 'contact', 'payments', 'start', 'terms', 'finishtutorial', 'privacy'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                    [
                        'actions' => ['about', 'contact', 'start', 'terms', 'privacy'],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                ],
            ],
        ];
    }

    public function actionAbout($tutotrial = false)
    {
        return $this->render('about',['tutotrial' => $tutotrial]);
    }

    /**
     * Displays contact page.
     *
     * @return mixed
     */
    public function actionContact($tutotrial = false)
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($model->sendEmail(Yii::$app->params['adminEmail'])) {
                Yii::$app->session->setFlash('success', 'Thank you for contacting us. We will respond to you as soon as possible.');
            } else {
                Yii::$app->session->setFlash('error', 'There was an error sending email.');
            }

            return $this->refresh();
        } else {
            return $this->render('contact', [
                'model' => $model,
                'tutotrial' => $tutotrial,
            ]);
        }
    }

    public function actionPayments()
    {
        return $this->render('payments');
    }

    public function actionStart($tutotrial = false)
    {
        return $this->render('start', ['tutotrial' => $tutotrial]);
    }

    public function actionTerms()
    {
        return $this->render('terms');
    }

    public function actionPrivacy()
    {
        return $this->render('privacy');
    }

    public function actionFinishtutorial()
    {
        $model = User::findOne(Yii::$app->user->identity->id);
        $model->times_logged = 1;
        $model->save();

        return $this->redirect(['subscription/index']);
    }

}
