<?php
$params = array_merge(
    require(__DIR__ . '/../../common/config/params.php'),
    require(__DIR__ . '/../../common/config/params-local.php'),
    require(__DIR__ . '/params.php'),
    require(__DIR__ . '/params-local.php')
);

$config = [
    'id' => 'app-frontend',
    'name'=>'Trodds.com',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'controllerNamespace' => 'frontend\controllers',
    'defaultRoute' => 'subscription/index',
    'modules' => [
        'debug' => [
            'class' => 'yii\debug\Module',
            'allowedIPs' => ['127.0.0.1', '::1']
        ]
    ],
    'components' => [
        'request' => [
            'csrfParam' => '_csrf-frontend',
        ],
        'user' => [
            'identityClass' => 'common\models\User',
            'enableAutoLogin' => true,
            'identityCookie' => ['name' => '_identity-frontend', 'httpOnly' => true],
        ],
        'session' => [
            // this is the name of the session cookie used for login on the frontend
            'name' => 'advanced-frontend',
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'assetManager' => [
            'bundles' => [
                'yii\bootstrap\BootstrapAsset' => [
                    'css' => [],
                ],
                'yii\bootstrap\BootstrapPluginAsset' => [
                    'js'=>[]
                ],
            ],
        ],
        'urlManager' => [
            'enablePrettyUrl' => true,
            'enableStrictParsing' => false,
            'showScriptName' => false,
            //'suffix' => '.html',
            'rules' => [
                'login' => 'site/login',
                'signup' => 'site/signup',
                'auth' => 'site/auth',
                'reset-password' => 'site/reset-password',
                'request-password-reset' => 'site/request-password-reset',
                'request-password-expired' => 'site/request-password-expired',
                'request-confirm-email' => 'site/request-confirm-email',
                'redirectbookmaker' => 'site/redirectbookmaker',
                'landing' => 'site/index',
                'thankyou' => 'site/thankyou',
                'getting-started' => 'site/start',
                'contact-us' => 'site/contact',
                'error-mail' => 'site/errormail',
                'confirm' => 'site/confirm',
                'confirm/<code:\w+>' => 'site/confirm',

                'terms' => 'help/terms',

                'clickbank' => 'clickbank/index',
                'clickbank/landing' => 'clickbank/index',
                'clickbank/why-us' => 'clickbank/whyus',
                'clickbank/getting-started' => 'clickbank/start',
                'clickbank/contact-us' => 'clickbank/contact',

                'bookies' => 'bookmaker/index',
                'settings/account' => 'settings/account',
                'settings/changepassword' => 'settings/changepassword',
                // '<controler>/<action>' => '<controler>/<action>',
                // '<action>' => 'subscription/<action>',
            ],
        ],
        'authClientCollection' => [
            'class' => 'yii\authclient\Collection',
            'clients' => [
                'google' => [
                    'class' => 'yii\authclient\clients\GoogleOAuth',
                ],
                'facebook' => [
                    'class' => 'yii\authclient\clients\Facebook',
                ],
                'twitter' => [
                    'class' => 'yii\authclient\clients\Twitter',
                ],
            ],
        ],
    ],
    'params' => $params,
];

if (YII_DEBUG) {
    $config['components']['log'] = [
        'traceLevel' => 3,
        'targets' => [
            [
                'class' => 'yii\log\FileTarget',
                'levels' => ['error', 'warning', 'info', 'trace', 'profile'],
            ],
        ],
    ];
}

return $config;