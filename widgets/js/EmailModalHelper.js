var EmailModalHelper = {
    map: {},

    _map: function() {
        var $this = this;
        $this.map = {
            emailModal: $('#change-email-modal'),
            emailForm: $('#change-email-form'),
            changeEmailLink: $('#change-user-email'),
            userEmail: $('#change-email-form #user-email'),
            changeError: $('#change-error-message'),
        };
    },

    init: function() {
        var $this = this;
        $this._map();
        $this.map.changeEmailLink.click(function() { return $this.openEmailModal(this); });
        this.map.emailModal.on("beforeSubmit", $this.map.emailForm, function () { return $this.changeEmailRequest(this); });
    },

    openEmailModal: function(link) {
        var $this = this;

        $this.map.emailModal.modal('show').focus();

        return false;
    },

    changeEmailRequest: function(link) {
        var $this = this;

        $.ajax({
            url: $this.map.emailForm.attr('action'),
            type: 'POST',
            data: {email: this.map.userEmail.val()},
            success: function (response) {
                if (response.success == true) {
                    window.location = window.location;
                } else {
                    $this.map.changeError.removeClass('hidden');
                }
            }
        });

        return false;
    }
};

$(function() {
    EmailModalHelper.init();
});