<?php

namespace frontend\widgets;

use Yii;
use yii\base\Widget;

class BookmakersWidget extends Widget{

	public function run(){
		return $this->render('_bookmakers');
	}

}