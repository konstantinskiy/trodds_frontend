<?php

namespace frontend\widgets;

use Yii;
use yii\helpers\Html;
use yii\bootstrap\Widget;
use common\models\User;

class ConfirmEmailChecker extends Widget
{
    public function run()
    {
        if (!Yii::$app->user->isGuest && Yii::$app->user->identity->status == User::STATUS_NOTACTIVE) {
            Yii::$app->getSession()->setFlash('error', [
                'Confirm your email address. A confirmation message was sent to ' . (!empty(Yii::$app->user->identity->email) ? '<b>' . Yii::$app->user->identity->email . '</b>': 'your email') . '.<br /><br />'
                . Html::a('Resend Confirmation', ['site/request-confirm-email'], ['class' => 'btn btn-danger', 'id' => 'resend-confirmation']) . ' '
                . Html::a('Update Email Address', ['user/view'], ['class' => 'btn btn-danger', 'id' => 'change-user-email'])
            ]);

            $view = Yii::$app->getView();
            ConfirmEmailAsset::register($view);

            return $this->render('changeEmailModal', ['model' => Yii::$app->user->identity]);
        }

        return null;
    }
}