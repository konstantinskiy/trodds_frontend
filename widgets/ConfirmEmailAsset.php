<?php
namespace frontend\widgets;

use yii\web\AssetBundle;

class ConfirmEmailAsset extends AssetBundle
{
    public $sourcePath = '@widgets';

    public $js = [
        'js/EmailModalHelper.js',
    ];
}
