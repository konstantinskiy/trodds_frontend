<div class="head_title<?if(isset($this->params['head_title']))echo $this->params['head_title'];?>">
  <?if($image != NULL):?>
    <div class="title_logo">
      <!--?xml version="1.0" encoding="utf-8"?-->
      <!-- Generator: Adobe Illustrator 17.1.0, SVG Export Plug-In . SVG Version: 6.00 Build 0)  -->
      <?=$image?>
    </div>
    <em>-</em>
  <?endif;?>
  
  <?if($content != NULL):?>
  	<h2><?=$content?></h2>
  <?else:?>
  	<h1><?=$this->title?></h1>
  <?endif;?>
</div>