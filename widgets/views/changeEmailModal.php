<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\Modal;
use yii\widgets\ActiveForm;
use yii\widgets\Pjax;
?>

<?php Modal::begin([
    'id' => 'change-email-modal',
    'header' => Html::tag('h3', 'Change your email'),
]); ?>

<?php Pjax::begin(['id' => 'change-email-block']); ?>

    <?php $form = ActiveForm::begin([
        'id' => 'change-email-form',
        'action' => ['site/change-email'],
        'options' => ['data-pjax' => true],
    ]); ?>

        <div class="alert alert-danger hidden" id="change-error-message">
            Error during saving user email address.
        </div>

        <?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>

        <div class="pull-right">
            <?= Html::a('Cancel', null, ['class' => 'btn btn-secondary', 'data-dismiss' => 'modal']) ?>
            <?= Html::submitButton('Update', ['class' => 'btn btn-primary']) ?>
        </div>

    <?php ActiveForm::end(); ?>

<?php Pjax::end(); ?>

<div class="clearfix"></div>

<?php Modal::end(); ?>