<?php

namespace frontend\assets;

use Yii;
use yii\web\AssetBundle;

/**
 * Main frontend application asset bundle.
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        //'css/tether.min.css',
        //'css/bootstrap.min.css',
        //'css/font-awesome.min.css',
        //'css/main.css',
        'css/style.css',
        'css/bootstrap-grid.css',
        'css/dev.css',
    ];
    public $js = [
        'js/scripts.js',
        //'js/tether.min.js',
        //'js/bootstrap.min.js',
        //'js/ie10-viewport-bug-workaround.js',
        'js/stickyfill.min.js',
        'js/handlebars-v4.0.5.js',
        'js/jquery.twbsPagination.min.js',
        //'js/jquery_cookie.js',
        //'js/ConfirmModalHelper.js',
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];

}
