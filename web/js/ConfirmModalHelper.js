var ConfirmModalHelper = {
    map: {},

    selectedHref: null,

    _map: function() {
        var $this = this;
        $this.map = {
            termsModal: $('#confirm-terms-modal'),
            authLink: $('.auth-client .auth-link'),
            agreeСheckbox: $('.modal-dialog  #agree-checkbox:checkbox')
        };
    },

    init: function() {
        var $this = this;
        $this._map();
        $this.map.authLink.click(function() { return $this.openTermsModal(this); });
        $this.map.agreeСheckbox.change(function() { return $this.agreeСheckboxChange(this); });
    },

    openTermsModal: function(link) {
        var $this = this;

        $this.selectedHref = $(link).attr('href');
        $this.map.termsModal.modal('show').focus();

        return false;
    },

    agreeСheckboxChange: function(checkbox) {
        var $this = this;

        if ($(checkbox).prop('checked')) {
            window.location.replace($this.selectedHref);
        }

        return false;
    }
};

$(function() {
    ConfirmModalHelper.init();
});