var isActive = false;

$('.js-menu').on('click', function() {

  if (isActive) {
    $(this).removeClass('active');
    $('body').removeClass('menu-open');
  } else {
    $(this).addClass('active');
    $('body').addClass('menu-open');
  }

  isActive = !isActive;

});

$(document).ready(function() {

    if($('.popup_video_iframe').length > 0){
        $('.popup_video_iframe')[0].src += "?autoplay=0";    
    }
  //При нажатии на ссылку с классом poplight и href атрибута тега <a> с #
  $('a.poplight[href*=\\#]').click(function() {

    var popID = $(this).attr('rel'); //получаем имя окна, важно не забывать при добавлении новых менять имя в атрибуте rel ссылки
    var popURL = $(this).attr('href'); //получаем размер из href атрибута ссылки

    if($(window).width() > 576){
        $(".play_secktop .popup_video_iframe").attr("src", $(".popup_video_iframe").attr("src").replace("autoplay=0", "autoplay=1"));
    }
    else{
        $(".play_mobile .popup_video_iframe").attr("src", $(".popup_video_iframe").attr("src").replace("autoplay=0", "autoplay=1"));
    }

    //запрос и переменные из href url
    var query = popURL.split('?');
    var dim = query[1].split('&');
    var popWidth = dim[0].split('=')[1]; //первое значение строки запроса
    var popHeight = dim[0].split('=')[2]; //второе значение строки запроса

    //Добавляем к окну кнопку закрытия
    $('#' + popID).fadeIn().css({
      'width': Number(popWidth),
      'height': Number(popHeight),
    }).prepend('<a href="#" title="Закрыть" class="close"></a>');

    //Определяем маржу(запас) для выравнивания по центру (по вертикали и горизонтали) - мы добавляем 80 к высоте / ширине с учетом отступов + ширина рамки определённые в css
    var popMargTop = ($('#' + popID).outerHeight()) / 2;
    var popMargLeft = ($('#' + popID).outerWidth()) / 2;

    //Устанавливаем величину отступа
    $('#' + popID).css({
      'margin-top': -popMargTop,
      'margin-left': -popMargLeft
    });

    //Добавляем полупрозрачный фон затемнения
    $('body').append('<div id="fade"></div>'); //div контейнер будет прописан перед тегом </body>.
    $('#fade').css({
      'filter': 'alpha(opacity=80)'
    }).fadeIn(); //полупрозрачность слоя, фильтр для тупого IE

    return false;
  });

  //Закрываем окно и фон затемнения
  $(document).on('click', 'a.close, #close, #fade', function() { //закрытие по клику вне окна, т.е. по фону...
    $('#fade , .popup_video , .popup_video_mobile').fadeOut(function() {
      $('#fade, a.close').remove(); //плавно исчезают
    });

    if($(window).width() > 576){
        $(".play_secktop .popup_video_iframe").attr("src", $(".popup_video_iframe").attr("src").replace("autoplay=1", "autoplay=0"));
    }
    else{
        $(".play_mobile .popup_video_iframe").attr("src", $(".popup_video_iframe").attr("src").replace("autoplay=1", "autoplay=0"));
    }

    return false;
  });

});